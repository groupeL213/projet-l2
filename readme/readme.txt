Dans ce dossier, vous trouverez le dump de la base de données (imagej_java.sql)
ainsi que la fiche de configuration (Fiche-de-configuration.pdf). La base de données
contient quelques données, qui sont issues de nos tests, et permettent une première
utilisation du logiciel.

Rappel :
La classe à exécuter est controller.Main.

Identifiants de connexion :
Utilisateur normal : UtilisateurUtilisateur / utilisateur
Administrateur : AdminAdmin / admin