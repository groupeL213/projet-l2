package view;

import controller.Algorithm;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Permet la création des Algorithmes. Appeler le constructuer pour créer une nouvelle fenêtre.
 * Appeler également showStage() pour montrer la fenêtre à l'écran.
 */
public class CreateNewAlgorithmView {

    private final Stage thisStage;

    @FXML private Button buttonConfirm, buttonCancel;
    @FXML private TextField nameAlgo, minArea, maxArea, minCirc, maxCirc;
    @FXML private ComboBox<Algorithm> baseAlgo;
    @FXML private Label textErrorOrConfirm;

    public CreateNewAlgorithmView() {
        thisStage = new Stage();

        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("fxml/CreateNewAlgorithm.fxml"));

            loader.setController(this);

            thisStage.setScene(new Scene(loader.load()));

            thisStage.setTitle("Création d'un algorithme");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Affiche le Stage créé dans le constructeur
     */
    public void showStage() {
        thisStage.showAndWait();
    }

    /**
     * Méthode qui permet d'initialiser les éléments graphiques.
     */
    @FXML
    private void initialize() {
        actualizeListAlgo();
        baseAlgo.getSelectionModel().select(0);
        buttonCancel.setOnAction(event -> actionOnCancel());
        buttonConfirm.setOnAction(event -> actionOnConfirm());
    }

    /**
     * Actualise la liste des Algorithmes de base.
     */
    private void actualizeListAlgo() {
        ArrayList<Algorithm> algoList = Algorithm.getAlgorithmList();
        ObservableList<Algorithm> observableList = FXCollections.observableArrayList(algoList);
        baseAlgo.setItems(observableList);
    }

    /**
     * Action effectuée lors du clic sur confirmer.
     */
    private void actionOnConfirm() {
        boolean correctValues;
        String name = this.nameAlgo.getText();
        if (!name.equals("")) {
            int valueMaxArea = verifyValueInt(maxArea);
            double valueMaxCirc = verifyValueDouble(maxCirc);
            int valueMinArea = verifyValueInt(minArea);
            double valueMinCirc = verifyValueDouble(minCirc);

            if (valueMinArea >= 0 && valueMinCirc >= 0 && valueMaxCirc >= 0 && valueMaxArea >= 0) {
                correctValues = true;
            } else {
                correctValues = false;
            }

            if (correctValues) {
                Algorithm algo = baseAlgo.getValue();
                boolean result = Algorithm.createNewAlgo(name, algo.getMethod(), valueMinArea, valueMaxArea, valueMinCirc, valueMaxCirc);
                if (result) {
                    showConfirm("L'algorithme "+ name + " a bien été créé.");
                } else {
                    showError("Une erreur est survenue lors de la création de l'algorithme.");
                }
            }
        } else {
            showError("Vous devez entrer un nom.");
            nameAlgo.requestFocus();
        }


    }

    /**
     * Action effectuée lors du clic sur Annuler.
     */
    private void actionOnCancel() {
        thisStage.close();
    }

    /**
     * Affiche une erreur.
     * @param text texte de l'erreur
     */
    private void showError(String text) {
        textErrorOrConfirm.setText(text);
        textErrorOrConfirm.setTextFill(Color.DARKRED);
    }

    /**
     * Affiche une confirmation.
     * @param text texte de la confirmation
     */
    private void showConfirm(String text) {
        textErrorOrConfirm.setText(text);
        textErrorOrConfirm.setTextFill(Color.DARKGREEN);
    }

    /**
     * Vérifie que les valeurs entrées sont correctes. Elles doivent être des entiers positifs.
     * @param textField champ de texte à vérifier
     * @return la valeur convertie ; -1 si n'est pas convertible
     */
    private int verifyValueInt(TextField textField) {
        try {
            return Integer.parseInt(textField.getText());
        } catch(Exception e) {
            showError("Attention : les valeurs entrées pour l'aire doivent être des entiers positifs.");
            textField.setText("");
            return -1;
        }
    }

    private double verifyValueDouble(TextField textField) {
        try {
            double value = Double.parseDouble(textField.getText());
            if (value < 0 || value > 1) return -1;
            else return value;
        } catch(Exception e) {
            showError("Attention : les valeurs entrées pour la circularité doivent être " +
                    "des nombres flottants entre 0 et 1.");
            textField.setText("");
            return -1;
        }
    }


}
