package view;

import controller.*;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import model.ImageRequests;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.sql.SQLException;


/**
 * Contrôleur de vue de l'écran de visualisation de l'image. Permet de visualiser les différentes
 * informations pour une image précise. Pour afficher cet écran, il faut appeler son constructeur.
 * Quelques méthodes doivent être appelées en supplément :
 *    - giveImage(ImageFromDB) pour donner l'image à afficher,
 *    - giveAssay(Assay) pour donner l'essai dont fait partie l'image,
 *    - giveVisualizationAssay(VisualizationAssayView) pour donner la fenêtre de visualisation de l'essai parent
 *    (permet d'actualiser la liste des images en cas de modification ou suppression).
 * showStage() permet d'afficher la fenêtre à l'écran.
 */
public class VisualizationImageView {

    private final Stage thisStage;
    private ImageFromDB imageData;
    private Assay connectedAssay;
    private VisualizationAssayView visualizationAssayActor;


    @FXML private ImageView selectedImageView;
    @FXML private TextField nameImage, addedDate, numberOfCells;
    @FXML private Button buttonModifyName;
    @FXML private Hyperlink openAnalyzed, deleteImage;

    ImageJ2 ij;


    /**
     * Constructeur de la fenêtre.
     */
    public VisualizationImageView() {

        thisStage = new Stage();

        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("fxml/VisualizationImage.fxml"));

            loader.setController(this);

            thisStage.setScene(new Scene(loader.load()));

            thisStage.setTitle("Visualisation d'une image");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Affiche le Stage créé dans le constructeur
     */
    public void showStage() {
        thisStage.showAndWait();
    }

    /**
     * Initialise la fenêtre et ses objets graphiques, ainsi que les écouteurs.
     */
    @FXML
    private void initialize() {
        openAnalyzed.setOnAction(this::openAnalyzed);
        deleteImage.setOnAction(this::deleteImage);
        buttonModifyName.setOnAction(event -> actionOnModifyName());
    }

    /**
     * Supprime une image.
     * @param event événement déclencheur
     */
    private void deleteImage(ActionEvent event) {

        boolean delete = InformationWindow.create("Etes-vous sûr de vouloir supprimer cette " +
                "image ? Cette action est irréversible.", InformationWindow.CONFIRMATION);

        if (delete) {
            try {
                ImageRequests.requestForDeletingImage(imageData.getId());
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
            ij.closeImage();
            thisStage.close();
            if (visualizationAssayActor != null) {
                visualizationAssayActor.actualizeImagesList();
            }
        }

    }

    /**
     * Ouvre la fenêtre de l'analyse de l'image.
     * @param event événement déclencheur.
     */
    private void openAnalyzed(ActionEvent event) {
        ij.showConvertedImage();
    }

    /**
     * Action effectuée lorsqu'on veut changer le nom de l'image.
     */
    private void actionOnModifyName() {
        System.out.println(nameImage.getStyle());
        if (buttonModifyName.getText().equals("Modifier")) {
            buttonModifyName.setText("Confirmer");
            nameImage.setEditable(true);
            nameImage.getStyleClass().remove("invisibleTextField");
            nameImage.getStyleClass().add("visibleTextField");
            nameImage.requestFocus();
        } else {
            if (nameImage.getText().equals("")) {
                nameImage.setText("Entrez un nom");
                nameImage.requestFocus();
            } else {
                buttonModifyName.setText("Modifier");
                nameImage.setEditable(false);
                nameImage.getStyleClass().remove("visibleTextField");
                nameImage.getStyleClass().add("invisibleTextField");
                boolean modify = imageData.modifyName(nameImage.getText());
                if (!modify) {
                    InformationWindow.create("Impossible de changer le nom de l'image.", InformationWindow.WARNING);
                    nameImage.setText(imageData.getName());
                } else {
                    visualizationAssayActor.actualizeImagesList();
                }
            }
        }
    }


    /**
     * Permet de donner l'image à afficher à cette instance. Nécessaire au bon fonctionnement de cette fenêtre.
     * @param rowData les données de l'image
     */
    public void giveImage(ImageFromDB rowData) {
        this.imageData = rowData;
        this.selectedImageView.setImage(rowData.getImage());
        nameImage.setText(imageData.getName());
        numberOfCells.setText(numberOfCells.getText() + " " + imageData.getNumberOfCells());
        addedDate.setText(addedDate.getText() + " " + imageData.getAddDate().toFrenchForm());

        BufferedImage image = javafx.embed.swing.SwingFXUtils.fromFXImage(imageData.getImage(), null);
        ij = new ImageJ2(image,imageData.getName(),connectedAssay.getAlgo(),imageData.isBlackBackground());
    }

    /**
     * Donne l'essai associé à l'image à afficher.
     * @param assay l'essai
     */
    public void giveAssay(Assay assay){
        this.connectedAssay = assay;
    }

    /**
     * Donne la visualisation de l'essai dont est issue l'image. Permet d'actualiser la liste des images
     * de l'essai en cas de suppression.
     * @param visu instance de la visualisation d'essai
     */
    public void giveVisualizationAssay(VisualizationAssayView visu) {
        this.visualizationAssayActor = visu;
    }
}
