package view;

import controller.Campaign;
import controller.Team;
import controller.User;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import model.UserRequests;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Contrôleur de vue de la visualisation des équipes. Pour afficher une telle fenêtre, vous devez d'abord
 * appeler le constructeur. Ensuite vous devez appeler :
 *    - giveTeam(Team) pour donner l'instance de l'équipe à afficher
 *    - giveConnectedUser(User) pour donner l'utilisateur connecté
 *    - giveAdminView(AdminWindowView) (facultatif) pour pouvoir actualiser l'instance de AdminWindowView en cas
 *    de suppression ou modification
 *    - showStage() pour afficher l'écran
 */
public class VisualizationTeamView {

    private final Stage thisStage;

    private Team teamToShow;
    private User connectedUser;
    private AdminWindowView adminView;

    @FXML private TableView<User> tableMembers;
    @FXML private TableColumn<User, String> columnNameMember, columnFirstnameMember, columnLoginMember;
    @FXML private TableView<Campaign> tableCampaigns;
    @FXML private TableColumn<Campaign, String> columnNameCampaign, columnNumberAssays, columnNumberImages;
    @FXML private TextField nameTeam, creationDateTeam;
    @FXML private Button buttonDeleteTeam;

    /**
     * Constructeur de la fenêtre
     */
    public VisualizationTeamView() {

        thisStage = new Stage();

        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("fxml/VisualizationTeam.fxml"));

            loader.setController(this);

            thisStage.setScene(new Scene(loader.load()));

            thisStage.setTitle("Visualiser une équipe");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Affiche le Stage créé dans le constructeur
     */
    public void showStage() {
        thisStage.showAndWait();
    }

    /**
     * Initialise la fenêtre et ses objets graphiques, place les écouteurs.
     */
    @FXML
    private void initialize() {
        tableMembers.setRowFactory(tv -> {
            TableRow<User> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
                    User rowData = row.getItem();
                    VisualizationUserView vizu = new VisualizationUserView();
                    vizu.giveUsers(rowData, connectedUser);
                    vizu.showStage();
                }
            });
            return row ;
        });

        tableCampaigns.setRowFactory(tv -> {
            TableRow<Campaign> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
                    Campaign rowData = row.getItem();
                    VisualizationCampaignView vizu = new VisualizationCampaignView();
                    vizu.giveCampaign(rowData);
                    vizu.giveConnectedUser(connectedUser);
                    vizu.showStage();
                }
            });
            return row ;
        });

        buttonDeleteTeam.setOnAction(event -> actionOnDelete());
    }

    /**
     * Permet de donner l'instance de l'équipe à afficher à cette instance.
     * Nécessaire au bon fonctionnement de cette classe.
     * @param t équipe à afficher
     */
    public void giveTeam(Team t) {
        teamToShow = t;
        nameTeam.setText(teamToShow.getName());
        creationDateTeam.setText(teamToShow.getCreationDate().toFrenchForm());
        actualizeMembersList();
        actualizeCampaignsList();
    }

    /**
     * Permet de transmettre l'utilisateur connecté à cette instance.
     * @param user utilisateur connecté
     */
    public void giveConnectedUser(User user) {
        this.connectedUser = user;
    }

    /**
     * Permet de transmettre l'instance de la fenêtre administrateur à cette instance.
     * Permet d'actualiser les tableaux lors d'une suppression.
     * @param view instance de la fenêtre administrateur
     */
    public void giveAdminView(AdminWindowView view) {
        this.adminView = view;
    }

    /**
     * Actualise la liste des membres. Nécessite que giveTeam() ait été appelée auparavant.
     */
    private void actualizeMembersList() {
        if (teamToShow != null) {
            ObservableList<User> dataUser = FXCollections.observableArrayList();

            try {
                ArrayList<Integer> userList = UserRequests.teamUsersList(teamToShow.getId());

                for(int id : userList) {
                    User u = new User(id);
                    dataUser.add(u);
                }

                columnNameMember.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getName()));
                columnFirstnameMember.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getFirstname()));
                columnLoginMember.setCellValueFactory(cellData -> new SimpleStringProperty(String.valueOf(cellData.getValue().getLogin())));

                tableMembers.setItems(dataUser);
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
    }

    /**
     * Actualise la liste des campagnes associées à l'équipe.
     */
    private void actualizeCampaignsList() {
        if (teamToShow != null) {
            ArrayList<Campaign> campaignsList = Campaign.getCampaignsForATeam(teamToShow);
            ObservableList<Campaign> dataUser = FXCollections.observableArrayList(campaignsList);

            columnNameCampaign.setCellValueFactory(cellData ->
                    new SimpleStringProperty(cellData.getValue().getName()));
            columnNumberAssays.setCellValueFactory(cellData ->
                    new SimpleStringProperty(String.valueOf(cellData.getValue().getNumberOfAssays())));
            columnNumberImages.setCellValueFactory(cellData ->
                    new SimpleStringProperty(String.valueOf(cellData.getValue().getNumberOfImages())));

            tableCampaigns.setItems(dataUser);
        }
    }

    /**
     * Action effectuée lors du clic sur Supprimer. Demande une confirmation.
     */
    private void actionOnDelete() {
        boolean confirm = InformationWindow.create("Confimer la suppression de cette équipe ?", InformationWindow.CONFIRMATION);
        if (confirm) {
            boolean delete = teamToShow.deleteThisTeam();
            if (delete) {
                actualizeAdminView();
                thisStage.close();
            } else {
                InformationWindow.create("Impossible de supprimer l'équipe car des campagnes sont encore reliées " +
                        "à cette équipe. Supprimez d'abord les campagnes associées, puis réessayez.",
                        InformationWindow.WARNING);
            }
        }
    }

    /**
     * Actualise la liste des équipes dans la fenêtre administrateur.
     */
    private void actualizeAdminView() {
        if (this.adminView != null) {
            adminView.actualizeTeamsList();
        }
    }
}
