package view;

import controller.Assay;
import controller.Campaign;
import controller.Date;
import controller.User;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import model.AssayRequests;
import model.CampaignRequests;

import javax.swing.*;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;

/**
 * Contrôleur de vue de l'écran de visualisation de la campagne. Permet de visualiser les différentes informations
 * sur une campagne précise.
 */
public class VisualizationCampaignView {

    private final Stage thisStage;

    private Campaign campaignToShow;
    private User connnectedUser;

    private HomeView homeview;

    private String TEXT_NUMBER_OF_IMAGES, TEXT_NUMBER_OF_CELLS, TEXT_AVERAGE_CELLS,
                    TEXT_NAME_OF_CAMPAIGN, TEXT_DESCRIPTION_OF_CAMPAIGN, TEXT_CREATION_DATE, TEXT_MODIFICATION_DATE;

    @FXML private Label creationDateCampaign, lastModificationCampaign,
            statsNumberOfImages, statsNumberOfCells, statsAverageNumberOfCells, creatorName, teamOfCampaign;
    @FXML private TextArea descriptionCampaign;
    @FXML private TitledPane campaignName;

    @FXML private TableView<Assay> tableAssays;
    @FXML private TableColumn<Assay, String> columnNameAssay, columnCreationDateAssay, columnNumberImagesAssay, columnAlgorithmAssay;

    @FXML private Hyperlink linkAddAssay;

    @FXML private Button buttonDeleteCampaign, buttonModifyCampaign, CVSExport;

    /**
     * Permet de créer une nouvelle fenêtre de cette vue.
     */
    public VisualizationCampaignView() {

        thisStage = new Stage();

        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("fxml/VisualizationCampaign.fxml"));

            loader.setController(this);

            thisStage.setScene(new Scene(loader.load()));

            thisStage.setTitle("Visualisation de la campagne");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Affiche le Stage créé dans le constructeur
     */
    public void showStage() {
        thisStage.showAndWait();
    }

    /**
     * Initialise la fenêtre et ses objets graphiques, place les écouteurs.
     */
    @FXML
    private void initialize() {
        TEXT_NUMBER_OF_IMAGES = this.statsNumberOfImages.getText();
        TEXT_NUMBER_OF_CELLS = this.statsNumberOfCells.getText();
        TEXT_AVERAGE_CELLS = this.statsAverageNumberOfCells.getText();
        TEXT_NAME_OF_CAMPAIGN = this.campaignName.getText();
        TEXT_DESCRIPTION_OF_CAMPAIGN = this.descriptionCampaign.getText();
        TEXT_CREATION_DATE = this.creationDateCampaign.getText();
        TEXT_MODIFICATION_DATE = this.lastModificationCampaign.getText();

        tableAssays.setRowFactory(tv -> {
            TableRow<Assay> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
                    Assay rowData = row.getItem();
                    VisualizationAssayView vizu = new VisualizationAssayView();
                    vizu.giveCampaign(campaignToShow);
                    vizu.giveAssay(rowData);
                    vizu.giveConnectedUser(connnectedUser);
                    if (this.homeview != null) vizu.giveHomeView(this.homeview);
                    vizu.giveCampaignView(this);
                    vizu.showStage();
                }
            });
            return row ;
        });
        Comparator<String> columnComparatorDate =
                (String v1, String v2) -> (Date.convertStringSlashsToDate(v1))
                        .compareTo(Date.convertStringSlashsToDate(v2));
        columnCreationDateAssay.setComparator(columnComparatorDate);

        linkAddAssay.setOnAction(this::actionAddAssay);
        buttonModifyCampaign.setOnAction(event -> actionOnClicModify());
        buttonDeleteCampaign.setOnAction(event -> actionDeleteCampaign());
        CVSExport.setOnAction(event -> actionCVSExport());

    }

    /**
     * Exporte la campagne actuelle
     */
    private void actionCVSExport() {
        try {
            this.campaignToShow.export(tableAssays.getItems());
            InformationWindow.create("Export réussi. Retrouvez votre fichier CVS " +
                    "dans le dossier export du logiciel.", InformationWindow.INFORMATION);
        } catch (IOException | SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Permet de supprimer la campagne
     */
    private void actionDeleteCampaign() {

        boolean confirm = InformationWindow.create("Etes-vous sûr de vouloir supprimer cette campagne ? " +
                "Les essais, images et analyses associés seront également supprimés." +
                "\nCette action est irréversible.",InformationWindow.CONFIRMATION);

        if(confirm){
            if(campaignToShow.deleteThisCampaign()){
                thisStage.close();
            }
            actualizeListInHomeView();
        }

    }

    /**
     * Action effectuée lors du clic sur Modifier.
     */
    private void actionOnClicModify() {
        ModifyCampaignOrAssayView view = new ModifyCampaignOrAssayView();
        view.giveCampaign(campaignToShow);
        view.giveVisualization(this);
        view.showStage();
        actualizeListInHomeView();
    }

    /**
     * Action sur écouteur : ajouter un nouvel essai.
     * @param event événement déclencheur
     */
    private void actionAddAssay(ActionEvent event) {
        if (this.connnectedUser != null) {
            CreateNewAssayView create = new CreateNewAssayView();
            create.giveConnectedUser(this.connnectedUser);
            create.giveSelectedCampaign(this.campaignToShow);
            create.giveVisualizationCampaignView(this);
            if (this.homeview != null) create.giveHomeView(homeview);
            create.showStage();
        } else {
            if (event.getSource().getClass() == Hyperlink.class) {
                ((Hyperlink) event.getSource()).setText("Action indisponible");
            }
        }
    }

    /**
     * Affiche les différentes informations de la campagnes dans les différents éléments graphiques.
     */
    private void showInfosCampaign() {
        if (campaignToShow != null) {
            refreshNameAndDescription();
            creationDateCampaign.setText(TEXT_CREATION_DATE + " " + campaignToShow.getCreationDate().toFrenchForm());
            creatorName.setText(campaignToShow.getCreator().getFirstname() +" "+ campaignToShow.getCreator().getName());
            teamOfCampaign.setText("Equipe : "+campaignToShow.getTeam().getName());
        }

    }

    /**
     * Actualise les statistiques de la campagne.
     */
    private void actualizeStats() {
        if (campaignToShow != null) {
            statsNumberOfImages.setText(TEXT_NUMBER_OF_IMAGES
                    + " " + campaignToShow.getNumberOfImages());
            statsNumberOfCells.setText(TEXT_NUMBER_OF_CELLS
                    + " " + campaignToShow.getNumberOfCells());
            statsAverageNumberOfCells.setText(TEXT_AVERAGE_CELLS
                    + " " + campaignToShow.getAverageNumberOfCellsPerImage());
        }
    }

    /**
     * Permet de donner la campagne à afficher. Cette méthode doit être appelée pour le bon fonctionnement du
     * programme.
     * @param c la campagne à afficher
     */
    public void giveCampaign(Campaign c) {
        this.campaignToShow = c;
        showInfosCampaign();
        actualizeAssaysList();
    }

    /**
     * Permet de donner l'utilisateur qui s'est connecté. Cette méthode doit être appelée pour le bon
     * fonctionnement du programme.
     * @param user l'utilisateur connecté
     */
    public void giveConnectedUser(User user) {
        this.connnectedUser = user;
        if(connnectedUser.isAdmin()) {
            linkAddAssay.setDisable(true);
            tableAssays.setDisable(true);
            buttonModifyCampaign.setDisable(true);
            buttonDeleteCampaign.setDisable(true);
        }
    }

    /**
     * Permet de transmettre à cette vue la page d'accueil.
     * @param view la vue de la page d'accueil
     */
    public void giveHomeView(HomeView view) {
        this.homeview = view;
    }

    /**
     * Actualise la liste des essais dans le tableau correspondant.
     */
    public void actualizeAssaysList() {
        if (campaignToShow != null) {

            ObservableList<Assay> data = FXCollections.observableArrayList();

            try {
                ArrayList<Integer> assayIDList =
                        AssayRequests.requestForIdAssaysInCampaign(campaignToShow.getId());

                for(Integer id : assayIDList) {
                    Assay c = new Assay(id);
                    data.add(c);
                }

                columnNameAssay.setCellValueFactory(cellData
                        -> new SimpleStringProperty(cellData.getValue().getName()));
                columnCreationDateAssay.setCellValueFactory(cellData
                        -> new SimpleStringProperty(cellData.getValue().getCreationDate().toSimpleForm()));
                columnNumberImagesAssay.setCellValueFactory(cellData
                        -> new SimpleStringProperty(String.valueOf(cellData.getValue().getNumberOfImages())));
                columnAlgorithmAssay.setCellValueFactory(cellData
                        -> new SimpleStringProperty(cellData.getValue().getAlgo().getName()));

                tableAssays.setItems(data);

            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        actualizeStats();
    }

    /**
     * Actualise la liste des essais et des campagnes sur la page d'accueil.
     */
    public void actualizeListInHomeView() {
        if (this.homeview != null) {
            this.homeview.actualizeCampaignsList();
            this.homeview.actualizeAssaysList();
        }
    }

    /**
     * Rafraîchit le nom et la description de la campagne. Utilisé après la modification de celle-ci.
     */
    public void refreshNameAndDescription() {
        if (campaignToShow != null) {
            campaignName.setText(TEXT_NAME_OF_CAMPAIGN + campaignToShow.getName());

            if (campaignToShow.getDescription().equals("")) {
                descriptionCampaign.setText(TEXT_DESCRIPTION_OF_CAMPAIGN + "(aucune description)" );
            } else {
                descriptionCampaign.setText(TEXT_DESCRIPTION_OF_CAMPAIGN + "" + campaignToShow.getDescription());
            }


            lastModificationCampaign.setText(TEXT_MODIFICATION_DATE + " "
                    + campaignToShow.getLastModificationDate().toFrenchForm()
                    + " (il y a " +campaignToShow.getLastModificationDate().howManyTimeAgo(new Date())+")");
        }
    }

}
