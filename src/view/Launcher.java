package view;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

/**
 * Classe qui ouvre une instance de ConnectionView. C'est la classe qui est appelée par controller.Main, étant
 * le point de départ de l'application. La classe Launcher permet de lancer l'interface graphique en elle-même.
 */
public class Launcher extends Application {

    public static final Image logoApplication = new Image(Launcher.class.getResourceAsStream("img/favicon.png"));

    /**
     * Démarre l'application.
     * @param primaryStage primaryStage
     * @throws Exception Une erreur est survenue lors du chargement du fichier FXML.
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        try {

            FXMLLoader loader = new FXMLLoader(getClass().getResource("fxml/Connection.fxml"));

            Scene scene = new Scene(loader.load());
            primaryStage.setTitle("Connexion");

            ConnectionView controller = loader.getController();

            controller.setStage(primaryStage);

            primaryStage.setScene(scene);
            primaryStage.getIcons().add(logoApplication);
            primaryStage.show();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
}
