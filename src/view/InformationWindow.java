package view;

import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.DialogPane;
import javafx.stage.Stage;

/**
 * Permet d'ouvrir une fenêtre de confirmation, information ou avertissement de type Alert.
 */
public class InformationWindow {

    /**
     * Type de fenêtre CONFIRMATION. Demande la confirmation à l'utilisateur, avec 2 boutons : Oui, Non.
     * Utiliser ce type fera renvoyer true si l'utilisateur a cliqué oui, false sinon.
     */
    public final static int CONFIRMATION = 1;
    /**
     * Type de fenêtre INFORMATION. Ouvre une fenêtre donnant une information à l'utilisateur, sans type de retour.
     * Bouton "ok".
     */
    public final static int INFORMATION = 2;
    /**
     * Type de fenêtre WARNING. Ouvre une fenêtre d'avertissement à l'utilisateur, sans type de retour.
     * Bouton "ok".
     */
    public final static int WARNING = 3;

    /**
     * Crée la fenêtre selon le type demandé avec le texte précisé.
     * @param text texte à afficher
     * @param type type de fenêtre (confirmation, information, warning). Utiliser les constantes de la classe.
     * @return par défaut return false ; true si le type est "confirmation" et que l'utilisateur a cliqué "oui"
     */
    public static boolean create(String text, int type) {

        Alert alert = null;
        ButtonType result = null;
        
        switch (type) {
            case CONFIRMATION: {
                alert = new Alert(Alert.AlertType.CONFIRMATION, text, ButtonType.YES, ButtonType.NO);
                alert.setHeaderText("Confirmation :");
                result = alert.showAndWait().orElse(ButtonType.NO);
            }
            break;

            case INFORMATION: {
                alert = new Alert(Alert.AlertType.INFORMATION, text, ButtonType.OK);
                alert.setHeaderText("Information :");
                result = alert.showAndWait().orElse(ButtonType.NO);
            }
            break;

            case WARNING: {
                alert = new Alert(Alert.AlertType.WARNING, text, ButtonType.OK);
                alert.setHeaderText("Attention :");
                result = alert.showAndWait().orElse(ButtonType.NO);
            }
            break;
        }//switch

        Stage st = new Stage();
        st.setScene(new Scene(alert.getDialogPane()));
        st.setAlwaysOnTop(true);

        if (ButtonType.YES.equals(result))
            return true;
        return false;
    }//create

}

