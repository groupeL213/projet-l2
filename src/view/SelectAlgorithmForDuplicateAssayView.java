package view;

import controller.Algorithm;
import controller.Assay;
import controller.User;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import model.AlgorithmRequests;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Contrôleur de vue pour la sélection du nouvel algorithme pour un essai dupliqué.
 */
public class SelectAlgorithmForDuplicateAssayView {


    private final Stage thisStage;
    private Assay assayToDuplicate;
    private User connectedUser;

    @FXML private
    Button buttonConfirm, buttonCancel;
    @FXML private
    Text textErrorOrConfirm;
    @FXML private
    ComboBox<Algorithm> comboboxAlgorithm;

    /**
     * Constructeur de la fenêtre
     */
    public SelectAlgorithmForDuplicateAssayView() {

        thisStage = new Stage();

        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("fxml/SelectAlgorithmForDuplicateAssay.fxml"));

            loader.setController(this);

            thisStage.setScene(new Scene(loader.load()));

            thisStage.setTitle("Sélection de l'algorithme");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Affiche le Stage créé dans le constructeur
     */
    public void showStage() {
        thisStage.showAndWait();
    }

    /**
     * Initialise la fenêtre et ses objets graphiques, ainsi que les écouteurs.
     */
    @FXML
    private void initialize() {
        actualizeAlgorithmList();
        buttonCancel.setOnAction(event -> thisStage.close());
        buttonConfirm.setOnAction(event -> actionDuplicateAssay());

    }

    /**
     * Donne l'essai à dupliquer. Nécessaire au bon fonctionnement du programme.
     * @param assay essai à dupliquer
     */
    public void giveAssayToDuplicate(Assay assay) {
        assayToDuplicate = assay;
    }

    /**
     * Donne l'utilisateur connecté. Nécessaire au bon fonctionnement du programme.
     * @param user utilisateur connecté
     */
    public void giveConnectedUser(User user) {
        connectedUser = user;
    }



    /**
     * Actualise la liste de sélection des algorithmes (ComboBox).
     */
    public void actualizeAlgorithmList() {
        try {
            ArrayList<Integer> algoListID = AlgorithmRequests.requestForListIdAlgo();
            ArrayList<Algorithm> algoList = new ArrayList<>();
            for (Integer algoID : algoListID) {
                algoList.add(new Algorithm(algoID));
            }
            ObservableList<Algorithm> observableList = FXCollections.observableArrayList(algoList);
            comboboxAlgorithm.setItems(observableList);


        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }


    }

    /**
     * Affiche une erreur sur la fenêtre.
     * @param text texte de l'erreur
     */
    private void showError(String text) {
        textErrorOrConfirm.setText(text);
        textErrorOrConfirm.setFill(Color.RED);
    }

    /**
     * Action effectuée lors de la validation. Duplique l'essai.
     */
    private void actionDuplicateAssay() {
        if (comboboxAlgorithm.getValue() != null && assayToDuplicate != null && connectedUser != null) {
            Algorithm algoSelected = comboboxAlgorithm.getValue();
            Assay duplicated = assayToDuplicate.duplicateAssay(connectedUser, algoSelected);
            if (duplicated != null) {
                thisStage.close();
                VisualizationAssayView visu = new VisualizationAssayView();
                visu.giveAssay(duplicated);
                visu.giveConnectedUser(connectedUser);
                visu.showStage();
            } else {
                showError("Une erreur est survenue lors de la duplication.");
            }

        } else {
            showError("Une erreur est survenue : paramètres manquants.");
        }

    }


}
