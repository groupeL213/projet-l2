package view;

import controller.*;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import javafx.scene.text.Text;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;

/**
 * Contrôleur-vue de l'écran d'accueil de l'utilisateur normal. Affiche la liste des campagnes, des essais,
 * des images, quelques statistiques, et offre la possibilité d'en ajouter d'autres.
 * Pour créer cette fenêtre, vous devez appeler son constructeur. Pour le bon fonctionnement de cet écran,
 * vous devez aussi appeler :
 *   - giveConnectedUser(User) pour donner l'utilisateur connecté,
 *   - giveConnectionView(ConnectionView) pour donner l'instance d'écran de connexion,
 *   - showStage() pour montrer la fenêtre à l'écran.
 */
public class HomeView {

    private final Stage thisStage;

    @FXML private Hyperlink createNewCampaignLink, createNewAssayLink;
    @FXML private TableView<Campaign> tableCampaigns;
    @FXML private TableView<Assay> tableAssays;
    @FXML private Text textConnectedAs;

    @FXML private MenuItem disconnectOption, exitOption, createNewCampaignMenu, createNewAssayMenu,
            consultAccountMenu, menuAbout;

    @FXML private
    TableColumn<Campaign, String> columnNameCampaign, columnCreationDateCampaign,
            columnNumberAssaysCampaign, columnNumberImagesCampaign;

    @FXML private TableColumn<Assay, String> columnNameAssay, columnCreationDateAssay,
            columnCampaignOfAssay, columnNumberImagesAssay;

    @FXML private Button refreshLists;
    @FXML private TextField statsNumberOfImages, statsNumberOfCells, statsAverageNumberOfCells;

    private String TEXT_STATS_IMAGES, TEXT_STATS_CELLS, TEXT_STATS_AVERAGE_CELLS;
    private User connectedUser;
    private ConnectionView connectionView;


    /**
     * Constructeur de la fenêtre
     */
    public HomeView() {
        thisStage = new Stage();

        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("fxml/Home.fxml"));

            loader.setController(this);

            thisStage.setScene(new Scene(loader.load()));
            thisStage.getIcons().add(Launcher.logoApplication);

            thisStage.setTitle("Gestionnaire de campagnes de mesure");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Affiche le Stage créé dans le constructeur
     */
    public void showStage() {
        thisStage.showAndWait();
    }

    /**
     * Initialise la fenêtre et ses objets graphiques, met en place les écouteurs
     */
    @FXML
    private void initialize() {
        createNewCampaignLink.setOnAction(event -> actionCreateNewCampaignLink());
        createNewCampaignMenu.setOnAction(event -> actionCreateNewCampaignLink());
        createNewAssayLink.setOnAction(event -> actionCreateNewAssayLink());
        createNewAssayMenu.setOnAction(event -> actionCreateNewAssayLink());

        consultAccountMenu.setOnAction(event -> actionAccountView());
        menuAbout.setOnAction(event -> actionAboutView() );

        refreshLists.setOnAction(event -> { actionOnRefreshLists(); actualizeStatistics(); });

        disconnectOption.setOnAction(event -> actionDisconnect());
        exitOption.setOnAction(event -> {
            thisStage.close();
            if (Main.automaticReconnection) connectionView.reconnexion.stop();
            Platform.exit();
        });


        tableCampaigns.setRowFactory(tv -> {
            TableRow<Campaign> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
                    Campaign rowData = row.getItem();
                    VisualizationCampaignView vizu = new VisualizationCampaignView();
                    vizu.giveCampaign(rowData);
                    vizu.giveConnectedUser(connectedUser);
                    vizu.giveHomeView(this);
                    vizu.showStage();
                }
            });
            return row ;
        });
        Comparator<String> columnComparatorDate =
                (String v1, String v2) -> (Date.convertStringSlashsToDate(v1))
                        .compareTo(Date.convertStringSlashsToDate(v2));

        columnCreationDateCampaign.setComparator(columnComparatorDate);
        columnCreationDateAssay.setComparator(columnComparatorDate);

        tableAssays.setRowFactory(tv -> {
            TableRow<Assay> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
                    Assay rowData = row.getItem();
                    VisualizationAssayView vizu = new VisualizationAssayView();
                    vizu.giveAssay(rowData);
                    vizu.giveConnectedUser(connectedUser);
                    vizu.giveHomeView(this);
                    vizu.showStage();
                }
            });
            return row ;
        });

        TEXT_STATS_IMAGES = statsNumberOfImages.getText();
        TEXT_STATS_CELLS = statsNumberOfCells.getText();
        TEXT_STATS_AVERAGE_CELLS = statsAverageNumberOfCells.getText();

    }

    /**
     * Action effectuée lorsqu'on accès au menu "A propos".
     */
    private void actionAboutView() {
        AboutView about = new AboutView();
        about.showStage();
    }

    /**
     * Action effectuée lorsque l'on souhaite visualiser son compte.
     */
    private void actionAccountView() {
        VisualizationUserView visu = new VisualizationUserView();
        visu.giveUsers(connectedUser, connectedUser);
        visu.showStage();
    }


    /**
     * Action effectuée lors du clic sur Rafraîchir les listes. Rafraîchit les listes de campagnes
     * et d'essais. Cette méthode crée un Thread pour ne pas bloquer l'interface utilisateur.
     */
    private void actionOnRefreshLists() {
        Thread t = new Thread() {
            @Override
            public void run() {
                thisStage.getScene().setCursor(Cursor.WAIT);
                actualizeCampaignsList();
                actualizeAssaysList();
                thisStage.getScene().setCursor(null);
            }
        };
        t.start();
    }

    /**
     * Action effectuée lorsque l'on souhaite créer une nouvelle campagne.
     */
    private void actionCreateNewCampaignLink() {
        CreateNewCampaignView newCampaign = new CreateNewCampaignView();
        newCampaign.giveConnectedUser(connectedUser);
        newCampaign.giveHomeView(this);
        newCampaign.showStage();
    }

    /**
     * Action effectuée lorsque l'on souhaite créer un nouvel essai.
     */
    private void actionCreateNewAssayLink() {
        CreateNewAssayView newAssay = new CreateNewAssayView();
        newAssay.giveConnectedUser(connectedUser);
        newAssay.giveHomeView(this);
        newAssay.showStage();
    }

    /**
     * Action effectuée quand on veut se déconnecter.
     */
    private void actionDisconnect() {
        connectionView.giveHomeView(thisStage);
        connectionView.reset();
        connectionView.showStage();
    }

    /**
     * Permet de donner l'utilisateur connecté à cette instance. Nécessaire au bon fonctionnement du programme.
     * @param user utilisateur connecté
     */
    public void giveConnectedUser(User user) {
        connectedUser = user;
        System.out.println("Utilisateur reçu : "+user.toString());
        actualizeStatistics();
        textConnectedAs.setText(textConnectedAs.getText() + " " + connectedUser.getName() + " "
                + connectedUser.getFirstname() + " (" + connectedUser.getLogin() + ")");
    }

    /**
     * Permet de donner l'instance de ConnectionView à cette instance.
     * @param connect instance de ConnectionView
     */
    public void giveConnectionView(ConnectionView connect) {
        this.connectionView = connect;
        connectionView.hideStage();
    }

    /**
     * Actualise la liste des campagnes. Nécessite que giveConnectedUser() ait été appelée auparavant.
     */
    public void actualizeCampaignsList() {
        long start = System.currentTimeMillis();
        if (connectedUser != null) {
            ArrayList<Campaign> campignList = Campaign.getCampaignsSeenByUser(connectedUser);
            ObservableList<Campaign> data = FXCollections.observableArrayList(campignList);

            columnNameCampaign.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getName()));
            columnCreationDateCampaign.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getCreationDate().toSimpleForm()));
            columnNumberAssaysCampaign.setCellValueFactory(cellData -> new SimpleStringProperty(String.valueOf(cellData.getValue().getNumberOfAssays())));
            columnNumberImagesCampaign.setCellValueFactory(cellData -> new SimpleStringProperty(String.valueOf(cellData.getValue().getNumberOfImages())));

            tableCampaigns.setItems(data);
        }
        long end = System.currentTimeMillis();
        System.out.println("Durée d'actualizeCampagignsList() : " + (end-start) + " ms");
    }

    /**
     * Actualise la liste des essais. Nécessite que giveConnectedUser() ait été appelée auparavant.
     */
    public void actualizeAssaysList() {
        long start = System.currentTimeMillis();
        if (connectedUser != null) {

            ArrayList<Assay> assayList = Assay.getAssaysSeenByUser(connectedUser);
            ObservableList<Assay> data = FXCollections.observableArrayList(assayList);

            columnNameAssay.setCellValueFactory(cellData
                    -> new SimpleStringProperty(cellData.getValue().getName()));
            columnCreationDateAssay.setCellValueFactory(cellData
                    -> new SimpleStringProperty(cellData.getValue().getCreationDate().toSimpleForm()));
            columnNumberImagesAssay.setCellValueFactory(cellData
                    -> new SimpleStringProperty(String.valueOf(cellData.getValue().getNumberOfImages())));
            columnCampaignOfAssay.setCellValueFactory(cellData
                    -> {
                try {
                    return new SimpleStringProperty((new Campaign(cellData.getValue().getIdCampaign())).getName());
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
                return new SimpleStringProperty("");
            });
            tableAssays.setItems(data);
        }
        long end = System.currentTimeMillis();
        System.out.println("Durée d'actualizeAssaysList() : " + (end-start) + " ms");
    }


    /**
     * Actualise les statistiques.
     */
    private void actualizeStatistics() {
        statsNumberOfImages.setText(TEXT_STATS_IMAGES + " " + connectedUser.getNumberOfImportedImages());
        statsNumberOfCells.setText(TEXT_STATS_CELLS + " " + connectedUser.getNumberOfCells());
        statsAverageNumberOfCells.setText(TEXT_STATS_AVERAGE_CELLS + " " + connectedUser.getAverageCells());
    }


}
