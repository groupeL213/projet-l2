package view;

import controller.Main;
import controller.User;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import model.Connection_BD;

import java.sql.SQLException;


/**
 * Controller-View de la vue connectionView. Définit les actionListener de la vue.
 * Permet la connexion des utilisateurs au gestionnaire de campagnes de mesure.
 * Cette fenêtre ne peut pas être ouverte autrement qu'en passant par Launcher. Pour y accéder ultérieurement,
 * il faut que la fenêtre enfant ait accès à l'unique instance de cette classe. Ensuite, la classe voulant
 * ouvrir une nouvelle ConnectionView doit appeler reset() de ladite classe, puis showStage(). Si la classe
 * enfant est HomeView, elle peut aussi appeler giveHomeView(HomeView).
 */
public class ConnectionView {

    @FXML public Button connectionButton;
    @FXML public Text connectionError;
    @FXML public TextField connectionLogin;
    @FXML public TextField connectionPassword;


    @FXML private Label notifyConnectionDB;

    private Stage thisStage;
    private User connectedUser;

    private Stage homeView;

    public Thread reconnexion;


    /**
     * Initialise la fenêtre et ses objets graphiques, place les écouteurs.
     */
    @FXML
    private void initialize() {
        connectionButton.setOnAction(event -> actionButtonConnexion());
        connectionPassword.setOnKeyPressed(k -> {
            if (k.getCode().equals(KeyCode.ENTER)) {
                actionButtonConnexion();
            }
        });

        Thread threadToNotify = new Thread() {
            public void run() {
                notifyConnectionDB.setText("Connexion à la base de données en cours...");
                if (Connection_BD.isConnected) {
                    notifyConnectionToDB(Connection_BD.delayToConnectToDB);
                } else {
                    try {
                        while (!Connection_BD.isConnected)  {
                            synchronized (Main.objectToWait) {
                                Main.objectToWait.wait();
                            }
                        }
                        Platform.runLater(new Runnable() {
                            @Override
                            public void run() {
                                notifyConnectionToDB(Connection_BD.delayToConnectToDB);
                            }
                        });
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        threadToNotify.start();

        reconnexion = new Thread() {
            public void run() {
                try {
                    sleep(10000);
                    while(true) {
                        Connection_BD.verifyConnection();
                        sleep(Main.delayToCheckConnectionToDB*1000);
                    }
                } catch (InterruptedException | SQLException e) {
                    e.printStackTrace();
                }

            }
        };
        if (Main.automaticReconnection)
            reconnexion.start();

    }

    /**
     * ActionListener pour le bouton Connexion.
     */
    private void actionButtonConnexion() {
        showConfirm("Connexion en cours");

        if (!connectionLogin.getText().equals("") && !connectionPassword.getText().equals("") ) {
            User u = User.verifyConnection(connectionLogin.getText(), connectionPassword.getText());
            if (u == null) {
                System.out.println("Erreur, login ou mdp incorrect");
                showError("Identifiant ou mot de passe incorrect");
            } else {
                connectedUser = u.copy();
                System.out.println("Connexion en cours...");
                ConnectionView thisConnection = this;
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        if (connectedUser.isAdmin()) {
                            AdminWindowView admin = new AdminWindowView();
                            admin.giveConnectedUser(connectedUser);
                            admin.giveConnectionView(thisConnection);
                            admin.showStage();

                        } else {
                            HomeView home = new HomeView();
                            System.out.println("Utilisateur envoyé : "+connectedUser.toString());
                            home.giveConnectedUser(connectedUser);
                            home.actualizeCampaignsList();
                            home.actualizeAssaysList();
                            home.giveConnectionView(thisConnection);
                            home.showStage();
                        }
                    }
                });
            }
        } else {
            showError("Les deux champs doivent être remplis");
        }
    }

    /**
     * Affiche l'erreur.
     * @param error texte de l'erreur
     */
    private void showError(String error) {
        connectionError.setText(error);
        connectionError.setFill(Color.DARKRED);
    }

    /**
     * Affiche la confirmation.
     * @param text texte de la confirmation
     */
    private void showConfirm(String text) {
        System.out.println("Methode");
        connectionError.setText(text);
        connectionError.setFill(Color.DARKGREEN);
    }

    /**
     * Change le Label qui notifie la connexion à la base de données.
     * @param duration durée d'établissement de la connexion
     */
    private void notifyConnectionToDB(long duration) {
        notifyConnectionDB.setText("Connexion à la base de données établie ("+duration+" ms)");
    }

    /**
     * Affiche le stage de l'instance et ferme celui de HomeView.
     */
    public void showStage() {
        thisStage.show();
        homeView.close();
    }

    /**
     * Cache (ferme) le stage de l'instance.
     */
    public void hideStage() {
        thisStage.hide();
    }

    /**
     * Permet de donner le stage à afficher à cette instance.
     * @param stage instance de Stage à afficher
     */
    public void setStage(Stage stage) {
        thisStage = stage;
    }


    /**
     * Permet de donner l'instance de HomeView à l'origine de cette instance. Facultatif.
     * @param homeStage l'instance de HomeView
     */
    public void giveHomeView(Stage homeStage) {
        homeView = homeStage;
    }

    /**
     * Remet à zéro la fenêtre pour la ré-utiliser lors de la déconnexion
     */
    public void reset() {
        this.initialize();
        connectionLogin.setText("");
        connectionPassword.setText("");
        showError("");
    }

}
