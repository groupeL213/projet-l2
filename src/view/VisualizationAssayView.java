package view;

import controller.*;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import model.ImageRequests;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;

/**
 * Contrôleur-vue de l'écran de visualisation d'un essai. Affiche les différentes informations sur l'essai.
 */
public class VisualizationAssayView {

    private final Stage thisStage;

    private Assay assayToShow;
    private Campaign campaignOfThisAssay;
    private User connectedUser;
    private String TEXT_NUMBER_OF_IMAGES, TEXT_NUMBER_OF_CELLS, TEXT_AVERAGE_CELLS,
    TEXT_NAME_ASSAY, TEXT_DESCRIPTION_ASSAY, TEXT_CREATION_ASSAY, TEXT_MODIFICATION_ASSAY;

    private HomeView homeview;
    private VisualizationCampaignView campaignView;

    @FXML private Button buttonDeleteAssay, buttonDuplicateAssay, buttonModifyAssay;

    @FXML private TitledPane nameAssay;
    @FXML private TextField creationDateAssay, campaignOfAssay, lastModificationDate;
    @FXML private TextField numberOfImages, numberOfCells, averageNumberOfCells, algorithmAssay;
    @FXML private TextArea descriptionAssay;

    @FXML private TableView<ImageFromDB> tableImages;
    @FXML private TableColumn<ImageFromDB, String> columnNameImage, columnDateImage, columnNumberOfCells;

    @FXML private Hyperlink linkAddImage;

    /**
     * Constructeur de la fenêtre.
     */
    public VisualizationAssayView() {

        thisStage = new Stage();

        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("fxml/VisualizationAssay.fxml"));

            loader.setController(this);

            thisStage.setScene(new Scene(loader.load()));

            thisStage.setTitle("Visualisation de l'essai");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Affiche le Stage créé dans le constructeur
     */
    public void showStage() {
        thisStage.showAndWait();
    }

    /**
     * Initialise la fenêtre et ses objets graphiques, place les écouteurs.
     */
    @FXML
    private void initialize() {
        linkAddImage.setOnAction(this::actionAddImage);
        buttonDuplicateAssay.setOnAction(event -> actionDuplicateAssay());
        buttonDeleteAssay.setOnAction(event -> actionDeleteAssay());
        buttonModifyAssay.setOnAction(event -> actionOnModify());

        TEXT_NUMBER_OF_IMAGES = this.numberOfImages.getText();
        TEXT_NUMBER_OF_CELLS = this.numberOfCells.getText();
        TEXT_AVERAGE_CELLS = this.averageNumberOfCells.getText();
        TEXT_NAME_ASSAY = this.nameAssay.getText();
        TEXT_DESCRIPTION_ASSAY = this.descriptionAssay.getText();
        TEXT_CREATION_ASSAY = this.creationDateAssay.getText();
        TEXT_MODIFICATION_ASSAY = this.lastModificationDate.getText();

        tableImages.setRowFactory(tv -> {
            TableRow<ImageFromDB> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
                    ImageFromDB rowData = row.getItem();
                    VisualizationImageView vizu = new VisualizationImageView();
                    vizu.giveAssay(this.assayToShow);
                    vizu.giveImage(rowData);
                    vizu.giveVisualizationAssay(this);
                    vizu.showStage();
                }
            });
            return row ;
        });
        Comparator<String> columnComparatorDate =
                (String v1, String v2) -> (Date.convertStringSlashsToDate(v1))
                        .compareTo(Date.convertStringSlashsToDate(v2));
        columnDateImage.setComparator(columnComparatorDate);

    }

    /**
     * Supprime un essai, ainsi que toute les images et amas associés
     */
    private void actionDeleteAssay() {

        boolean confirm = InformationWindow.create("Etes-vous sûr de vouloir supprimer cet essai ? " +
                "Les images et analyses associées à cet essai seront également supprimées." +
                "\nCette action est irréversible.",InformationWindow.CONFIRMATION);

        if(confirm) {
            if (assayToShow.deleteThisAssay()) {
                thisStage.close();
            }
            actualizeListInHomeView();
            if (this.campaignView != null) campaignView.actualizeAssaysList();

        }//if
    }

    /**
     * Action effectuée lors du clic sur Modifier.
     */
    private void actionOnModify() {
        ModifyCampaignOrAssayView modify = new ModifyCampaignOrAssayView();
        modify.giveVisualization(this);
        modify.giveAssay(assayToShow);
        modify.showStage();
    }

    /**
     * Montre les informations de l'essai sur la fenêtre.
     */
    private void showInfosAssay() {
        if (assayToShow != null) {

            algorithmAssay.setText(algorithmAssay.getText() + " " + assayToShow.getAlgo().getName());
            creationDateAssay.setText(TEXT_CREATION_ASSAY + " " + assayToShow.getCreationDate().toFrenchForm());


            if (this.campaignOfThisAssay == null) {
                try {
                    this.campaignOfThisAssay = new Campaign(assayToShow.getIdCampaign());
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
            }
            campaignOfAssay.setText(campaignOfAssay.getText()+ " " + campaignOfThisAssay.getName());

            refreshNameAndDescription();

        }
    }

    /**
     * Actualise les statistiques de l'essai.
     */
    public void actualizeStats() {
        if (assayToShow != null) {
            this.numberOfImages.setText(TEXT_NUMBER_OF_IMAGES
                    + " " + assayToShow.getNumberOfImages());
            this.numberOfCells.setText(TEXT_NUMBER_OF_CELLS
                    + " " + assayToShow.getNumberOfCells());
            this.averageNumberOfCells.setText(TEXT_AVERAGE_CELLS
                    + " " + assayToShow.getAverageNumberOfCells());
        }
    }

    /**
     * Action d'ajout d'une image sur cet essai.
     * @param event événement à l'orgine de l'action
     */
    private void actionAddImage(ActionEvent event) {
        if (connectedUser != null) {
            ImportNewImageView image = new ImportNewImageView();
            image.giveConnectedUser(connectedUser);
            image.giveCurrentAssay(assayToShow);
            image.giveVisualizationAssay(this);

            if (campaignOfThisAssay == null) {
                try {
                    image.giveCurrentCampaign(new Campaign(assayToShow.getIdCampaign()));
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
            }
            else image.giveCurrentCampaign(campaignOfThisAssay);

            image.showStage();


        } else {
            if (event.getSource().getClass() == Hyperlink.class) {
                ((Hyperlink) event.getSource()).setText("Action indisponible");
            }
        }
    }

    /**
     * Action de duplication de cet essai.
     */
    private void actionDuplicateAssay() {
        SelectAlgorithmForDuplicateAssayView select = new SelectAlgorithmForDuplicateAssayView();
        select.giveAssayToDuplicate(assayToShow);
        select.giveConnectedUser(connectedUser);
        select.showStage();
    }

    /**
     * Actualise la liste des essais et des campagnes sur la page d'accueil.
     */
    private void actualizeListInHomeView() {
        if (this.homeview != null) {
            homeview.actualizeAssaysList();
            homeview.actualizeCampaignsList();
        }
    }

    /**
     * Actualise la liste des images.
     */
    public void actualizeImagesList() {
        if (assayToShow != null) {

            ObservableList<ImageFromDB> data = FXCollections.observableArrayList();

            try {
                ArrayList<Integer> imageIDList =
                        ImageRequests.requestForImagesInTheAssay(assayToShow.getId());

                for(Integer id : imageIDList) {
                    ImageFromDB image;
                    try{
                        image = ImageRequests.requestInfosImage(id);
                        data.add(image);
                    } catch(NullPointerException e){

                        boolean confirmDelete = InformationWindow.create("Une entrée de la BD semble avoir un problème." +
                                "\n Voulez vous supprimez cette donnée ? Dans le cas contraire," +
                                "\n elle ne sera juste pas prise en compte pour cette fois.",InformationWindow.WARNING);

                        if(confirmDelete)
                            ImageRequests.requestForDeletingImage(id);

                    }//catch
                }//for

                columnNameImage.setCellValueFactory(cellData
                        -> new SimpleStringProperty(cellData.getValue().getName()));
                columnDateImage.setCellValueFactory(cellData
                        -> new SimpleStringProperty(cellData.getValue().getAddDate().toSimpleForm()));
                columnNumberOfCells.setCellValueFactory(cellData
                        -> new SimpleStringProperty(String.valueOf(cellData.getValue().getNumberOfCells())));


                tableImages.setItems(data);

            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        actualizeStats();


    }

    /**
     * Rafraîchit le nom et la description de l'essai. Utilisé après la modification de celui-ci.
     */
    public void refreshNameAndDescription() {
        if (assayToShow != null) {
            this.nameAssay.setText(TEXT_NAME_ASSAY + assayToShow.getName());
            if (assayToShow.getDescription().equals("")) {
                this.descriptionAssay.setText(TEXT_DESCRIPTION_ASSAY+"(aucune description)");
            } else {
                this.descriptionAssay.setText(TEXT_DESCRIPTION_ASSAY+assayToShow.getDescription());
            }
            this.lastModificationDate.setText(TEXT_MODIFICATION_ASSAY+" "
                    +assayToShow.getLastModificationDate().toFrenchForm()
                    + " (il y a "+assayToShow.getLastModificationDate().howManyTimeAgo(new Date())+")");
            actualizeListInHomeView();
        }
    }


    /**
     * Permet d'obtenir l'essai dont on doit afficher les informations.
     * Cette méthode est requise pour le bon fonctionnement de cet écran, sans quoi la page sera vide.
     * @param assay L'essai à paramétrer.
     */
    public void giveAssay(Assay assay) {
        this.assayToShow = assay;
        showInfosAssay();
        actualizeImagesList();
    }
    /**
     * Permet d'obtenir la campagne de l'essai à afficher.
     * @param campaign campagne de l'essai
     */
    public void giveCampaign(Campaign campaign) {
        this.campaignOfThisAssay = campaign;
    }

    /**
     * Permet de donner l'utilisateur connecté à cette classe.
     * @param user l'utilisateur connecté
     */
    public void giveConnectedUser(User user) {
        this.connectedUser = user;
    }

    /**
     * Permet de transmettre à cette vue la page d'accueil.
     * @param view la vue de la page d'accueil
     */
    public void giveHomeView(HomeView view) {
        this.homeview = view;
    }

    /**
     * Permet de transmettre à cette vue la page de visualisation de campagne.
     * @param view la vue de la page d'accueil
     */
    public void giveCampaignView(VisualizationCampaignView view) {
        this.campaignView = view;
    }



}
