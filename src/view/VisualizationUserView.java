package view;

import controller.Team;
import controller.User;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import model.TeamRequests;
import model.UserRequests;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Contrôleur de vue de l'écran de visualisation de l'utilisateur. Permet de visualiser les différentes
 * informations pour un utilisateur précis.
 */
public class VisualizationUserView {

    private final Stage thisStage;

    private User userToShow, connectedUser;
    private AdminWindowView adminView;

    private final Image iconModify = new Image(getClass().getResourceAsStream("img/modify-icon.png"));
    private final Image iconConfirm = new Image(getClass().getResourceAsStream("img/ok-icon.png"));

    @FXML ImageView imageViewName, imageViewFirstname;

    @FXML TextField nameUser, firstnameUser, loginUser, newPassword, newPasswordRepeated;
    @FXML Label isAdmin;
    @FXML VBox paneListTeams;

    private final ArrayList<CheckBox> checkBoxTeamsList;

    @FXML Button buttonSaveChanges, buttonDeleteUser, modifyNameButton, modifyFirstnameButton;
    @FXML Text textErrorOrConfirm;

    /**
     * Constructeur de la fenêtre.
     */
    public VisualizationUserView() {

        thisStage = new Stage();

        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("fxml/VisualizationUser.fxml"));

            loader.setController(this);

            thisStage.setScene(new Scene(loader.load()));

            thisStage.setTitle("Visualiser un compte utilisateur");

        } catch (IOException e) {
            e.printStackTrace();
        }

        checkBoxTeamsList = new ArrayList<>();
    }

    /**
     * Affiche le Stage créé dans le constructeur
     */
    public void showStage() {
        thisStage.showAndWait();
    }

    /**
     * Initialise la fenêtre et ses objets graphiques, ainsi que les écouteurs.
     */
    @FXML
    private void initialize() {
        buttonSaveChanges.setOnAction(event -> actionClicButtonSaveChanges());
        buttonDeleteUser.setOnAction(event -> actionOnClicDelete());
        modifyNameButton.setOnAction(this::actionOnClicModifyName);
        modifyFirstnameButton.setOnAction(this::actionOnClicModifyName);
    }

    /**
     * Permet de donner l'utilisateur à afficher. Nécessaire au bon fonctionnement de cette classe.
     * @param userToShow l'utilisateur à montrer
     * @param connectedUser l'utilisateur connecté
     */
    public void giveUsers(User userToShow, User connectedUser) {
        this.userToShow = userToShow;
        this.connectedUser = connectedUser;
        nameUser.setText(userToShow.getName());
        firstnameUser.setText(userToShow.getFirstname());
        loginUser.setText(userToShow.getLogin());
        isAdmin.setText(userToShow.isAdmin() ? "oui" : "non");
        if (userToShow.isAdmin() || !connectedUser.isAdmin())
            paneListTeams.setDisable(true);
        if (!connectedUser.isAdmin() || userToShow.getId() == connectedUser.getId()) {
            buttonDeleteUser.setDisable(true);
        }
        actualizeTeamsList();
    }

    public void giveAdminView(AdminWindowView view) {
        this.adminView = view;
    }


    /**
     * Actualise la liste des équipes pour l'utilisateur. Cette méthode affiche la liste de toutes les équipes
     * sous forme de CheckBox et coche celles dont fait partie l'utilisateur affiché.
     */
    public void actualizeTeamsList() {
        try {
            ArrayList<Integer> teamsList = TeamRequests.teamsList();
            ArrayList<Integer> teamsListOfTheUser = UserRequests.requestForTeamsOfUser(userToShow.getId());
            for (int id : teamsList) {
                Team team = new Team(id);
                CheckBox c = new CheckBox(team.getName());
                c.setUserData(team);
                this.checkBoxTeamsList.add(c);
                if (teamsListOfTheUser.contains(team.getId())) {
                    c.setSelected(true);
                }
            }
            paneListTeams.getChildren().addAll(this.checkBoxTeamsList);

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    /**
     * Action effectuée lors du clic sur le bouton sauvegarder. Cette méthode met à jour la base de données
     * pour le mot de passe et pour les équipes. Elle affiche également une erreur si l'accès à la base
     * de données est impossible ou que les deux mots de passe ne correspondent pas.
     */
    private void actionClicButtonSaveChanges() {

        if (newPassword.getText().equals(newPasswordRepeated.getText())) {

            boolean result = false;
            String error = "Une erreur est survenue lors de la sauvegarde.";

            if (!newPassword.getText().equals("")) {
                ConfirmPasswordView confirmPrevious = new ConfirmPasswordView();
                confirmPrevious.giveUsers(this.connectedUser, this.userToShow);
                confirmPrevious.showStage();
                if (confirmPrevious.getCorrectPassword()) {
                    result = userToShow.saveChanges(newPassword.getText(), checkBoxTeamsList);
                } else {
                    error = "L'ancien mot de passe tapé est incorrect.";
                }
            } else {
                result = userToShow.saveChanges(newPassword.getText(), checkBoxTeamsList);
            }

            if (result) {
                eraseError();
                showConfirm("Les changements ont bien été sauvegardés.", 3);
                newPassword.setText("");
                newPasswordRepeated.setText("");
            } else {
                showError(error, 0);

            }
        } else {
            showError("Les deux mots de passe ne correspondent pas.", 0);
        }
    }

    /**
     * Action effectuée lors du clic sur Supprimer. Action disponible uniquement pour les administrateurs.
     * Action indisponible si l'administrateur essaye de supprimer son propre compte.
     */
    private void actionOnClicDelete() {
        boolean confirm = InformationWindow.create("Confirmer la suppression de cet utilisateur ?" +
                " Aucune donnée personnelle ne sera conservée. Cette action est irréversible.",
                InformationWindow.CONFIRMATION);
        if (confirm) {
            boolean delete = userToShow.deleteThisUser();
            if (delete) {
                thisStage.close();
                actualizeAdminView();
            } else {
                InformationWindow.create("Une erreur est survenue lors de la suppression.",
                        InformationWindow.WARNING);
            }
        }
    }

    /**
     * Action effectuée lors du clic sur les boutons Modifier Nom ou Prénom.
     * @param event événement déclencheur
     */
    private void actionOnClicModifyName(ActionEvent event) {
        Button actorButton = (Button)event.getSource();
        TextField textToGet;

        textToGet = (actorButton.getId().equals("modifyNameButton")) ? nameUser : firstnameUser;

        textToGet.getStyleClass().remove("invisibleTextField");
        textToGet.getStyleClass().add("visibleTextField");
        textToGet.setEditable(true);
        actorButton.setOnAction(this::actionOnValideModifyName);
        modifyIconButton(actorButton, iconConfirm);
    }

    /**
     * Action effectuée lors du clic sur les boutons Valider de modification du Nom ou Prénom.
     * @param event événement déclencheur
     */
    private void actionOnValideModifyName(ActionEvent event) {
        Button actorButton = (Button)event.getSource();
        TextField textToGet;
        int TYPE_MODIF;
        String valueToChange;

        if (actorButton.getId().equals("modifyNameButton")) {
            textToGet = nameUser;
            TYPE_MODIF = User.TYPE_MODIF_NAME;
            actorButton = modifyNameButton;
            valueToChange = userToShow.getName();
        } else {
            textToGet = firstnameUser;
            TYPE_MODIF = User.TYPE_MODIF_FIRSTNAME;
            actorButton = modifyFirstnameButton;
            valueToChange = userToShow.getFirstname();
        }

        if (textToGet.getText().equals("") || textToGet.getText().equals("Entrez un nom")) {
            textToGet.setText("Entrez un nom");
            textToGet.requestFocus();
        } else {
            textToGet.getStyleClass().remove("visibleTextField");
            textToGet.getStyleClass().add("invisibleTextField");
            textToGet.setEditable(false);
            actorButton.setOnAction(this::actionOnClicModifyName);
            modifyIconButton(actorButton, iconModify);
            if (!textToGet.getText().equals(valueToChange)) {
                boolean confirm = userToShow.modifyName(textToGet.getText(), TYPE_MODIF);
                actualizeAdminView();
                if (confirm)
                    showConfirm("Changements sauvegardés.",3);
                else
                    showError("Erreur lors de la sauvegarde des changements.", 0);
            }
        }
    }

    /**
     * Modifie l'image dans un bouton.
     * @param button bouton concerné
     * @param image image à mettre dans le bouton
     */
    private void modifyIconButton(Button button, Image image) {
        ((ImageView)button.getGraphic()).setImage(image);
    }

    /**
     * Affiche le texte demandé comme une confirmation pendant une durée déterminée.
     * @param text texte à afficher
     * @param seconds le nombre de secondes pendant lequel le texte doit être affiché, s'il vaut zéro alors
     *                le texte sera affiché indéfiniment
     */
    private void showConfirm(String text, long seconds) {
        showTextErrorOrConfirm(text, seconds);
        textErrorOrConfirm.setFill(Color.DARKGREEN);
    }

    /**
     * Affiche le texte demandé comme une erreur pendant une durée déterminée.
     * @param text texte à afficher
     * @param seconds le nombre de secondes pendant lequel le texte doit être affiché, s'il vaut zéro alors
     *                le texte sera affiché indéfiniment
     */
    private void showError(String text, long seconds) {
        showTextErrorOrConfirm(text, seconds);
        textErrorOrConfirm.setFill(Color.DARKRED);
    }

    /**
     * Affiche le texte dans le champs d'erreur/confirmation pendant une durée déterminée.
     * @param text texte à afficher
     * @param seconds le nombre de secondes pendant lequel le texte doit être affiché, s'il vaut zéro alors
     *                le texte sera affiché indéfiniment
     */
    private void showTextErrorOrConfirm(String text, long seconds) {
        if (seconds > 0) {
            Thread t = new Thread() {
                @Override
                public void run() {
                    textErrorOrConfirm.setText(text);
                    try {
                        sleep(seconds*1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    eraseError();
                }
            };
            t.start();
        } else {
            textErrorOrConfirm.setText(text);
        }
    }

    /**
     * Efface le champs de texte représentant une erreur (ou une confirmation).
     */
    private void eraseError() {
        textErrorOrConfirm.setText("");
    }

    /**
     * Actualise la liste des utilisateurs dans la fenêtre administrateur.
     */
    private void actualizeAdminView() {
        if (this.adminView != null) {
            adminView.actualizeUsersLists();
        }
    }

}
