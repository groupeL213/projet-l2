package view;

import controller.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Contrôleur de vue de la fenêtre de création d'un essai. Pour ouvrir une fenêtre de ce type,
 * il faut appeler son constructeur. Ensuite, doivent être appelées :
 *    - giveConnectedUser(User) pour donner l'instance de l'utilisateur connecté (obligatoire)
 *    - giveSelectedCampaign(Campaign) pour donner l'instance de la campagne qui doit être préselectionnée (factulatif,
 *    si elle n'est pas appelée, affichage par défaut)
 *    - giveVisualizationCampaign(VisualizationCampaignView) pour donner l'instance de visualisation de la campagne
 *    attachée à cet essai (facultatif, permet d'actualiser la liste des essais dans la visualisation de campagne
 *    en cas de modification ou suppression)
 *    - giveHomeView(HomeView) (très fortement conseillé, permet d'actualiser la liste des campagnes et des essais
 *    lors de la confirmation)
 */
public class CreateNewAssayView {
    private final Stage thisStage;
    private User connectedUser;
    private Campaign selectedCampaign;
    private VisualizationCampaignView actorVisualization;
    private HomeView homeview;

    @FXML private Button cancelButton, confirmButton;
    @FXML private TextField assayName;
    @FXML private TextArea assayDescription;
    @FXML private ComboBox<Campaign> selectionCampaign;
    @FXML private ComboBox<Algorithm> selectionAlgorithm;
    @FXML private Text textErrorOrConfirm;

    /**
     * Constructeur de la fenêtre.
     */
    public CreateNewAssayView() {

        thisStage = new Stage();

        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("fxml/CreateNewAssay.fxml"));

            loader.setController(this);

            thisStage.setScene(new Scene(loader.load()));

            thisStage.setTitle("Création d'un essai");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Affiche le Stage créé dans le constructeur
     */
    public void showStage() {
        actualize();
        thisStage.showAndWait();
    }

    /**
     * Méthode qui permet d'initialiser les éléments graphiques.
     */
    @FXML
    private void initialize() {
        cancelButton.setOnAction(event -> actionButtonCancel());
        confirmButton.setOnAction(event -> actionButtonConfirm());
    }

    /**
     * Action effectuée lors du clic sur le bouton de confirmation.
     */
    private void actionButtonConfirm() {
        if (assayName.getText().equals("")) {
            showError("Vous devez indiquer le nom de l'essai.");
        } else {
            if (selectionAlgorithm.getValue() == null) {
                showError("Vous devez sélectionner un algorithme de comptage.");
            } else {
                Assay assay = Assay.createNewAssay(assayName.getText(),
                        assayDescription.getText(),
                        selectionCampaign.getValue(),
                        selectionAlgorithm.getValue().getId(), connectedUser);
                showConfirm("L'essai "+assay.getName()+" a bien été créé.");
                confirmButton.setDisable(true);
                if (this.actorVisualization != null) {
                    this.actorVisualization.actualizeAssaysList();
                    this.actorVisualization.refreshNameAndDescription();
                }

                Thread threadActualize = new Thread() {
                    @Override
                    public void run() {
                        actualizeListHomeView();
                    }
                };
                threadActualize.start();
            }

        }
    }

    /**
     * Action effectuée lors du clic sur le bouton Annuler.
     */
    private void actionButtonCancel() {
        thisStage.close();
    }

    /**
     * Affiche une erreur.
     * @param error erreur à afficher
     */
    private void showError(String error) {
        textErrorOrConfirm.setText(error);
        textErrorOrConfirm.setFill(Color.DARKRED);
    }

    /**
     * Affiche une confirmation.
     * @param confirm confirmation à afficher
     */
    private void showConfirm(String confirm) {
        textErrorOrConfirm.setText(confirm);
        textErrorOrConfirm.setFill(Color.DARKGREEN);
    }

    /**
     * Trouve l'index de la campagne qui doit être pré-sélectionnée.
     * @param campaignToFind la campagne à trouver ; vaut null si aucune campagne ne doit être préselectionnée
     * @param listOfSelection la liste des campagnes qui seront affichées, et dans laquelle ont doit trouver l'index
     * @return l'index de listOfSelection auquel on trouve campaignToFind
     */
    private int findIndexOfSelectedCampaign(Campaign campaignToFind, ArrayList<Campaign> listOfSelection) {
        int index = 0;
        if (campaignToFind != null) {
            for (Campaign c : listOfSelection) {
                if (c.getId() == campaignToFind.getId()) {
                    break;
                } else {
                    index++;
                }
            }
        }
        System.out.println("Find index : "+index);
        return index;
    }

    /**
     * Actualise la liste des campagnes et des algorithmes.
     */
    private void actualize() {
        actualizeAlgorithmList();
        try {
            actualizeCampaignList();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Actualise la liste des campagnes disponibles pour l'utilisateur.
     * @throws Exception Erreur provoquée lorsque l'utilisateur connecté n'a pas été donné.
     */
    private void actualizeCampaignList() throws Exception {
        if (connectedUser == null) {
            throw new Exception("Error: you must give the connectedUser using " +
                    "giveConnectedUser() before call this method.");
        }
        else {
            ArrayList<Campaign> campaignList = Campaign.getCampaignsSeenByUser(connectedUser);
            ObservableList<Campaign> observableList = FXCollections.observableArrayList(campaignList);
            selectionCampaign.setItems(observableList);
            selectionCampaign.getSelectionModel().select(findIndexOfSelectedCampaign(this.selectedCampaign, campaignList));
        }
    }

    /**
     * Actualise la liste des Algorithmes disponibles.
     */
    private void actualizeAlgorithmList() {
        ArrayList<Algorithm> algoList = Algorithm.getAlgorithmList();
        ObservableList<Algorithm> observableList = FXCollections.observableArrayList(algoList);
        selectionAlgorithm.setItems(observableList);
    }

    /**
     * Actualise la liste des essais et des campagnes sur la page d'accueil.
     */
    private void actualizeListHomeView() {
        if (this.homeview != null) {
            homeview.actualizeAssaysList();
            homeview.actualizeCampaignsList();
        }
    }

    /**
     * Permet d'initialiser l'utilisateur connecté. Permet de donner l'objet User à cette classe.
     * @param connectedUser utilisateur connecté
     */
    public void giveConnectedUser(User connectedUser) {
        this.connectedUser = connectedUser;
    }

    /**
     * Permet d'initialiser la campagne à présélectionner. Si cette méthode n'est pas appelée,
     * alors aucune campagne ne sera pré-sélectionnée.
     * @param c campagne à préselectionner
     */
    public void giveSelectedCampaign(Campaign c) {
        this.selectedCampaign = c;
    }

    /**
     * Permet de donner l'instance de VisualizationCampaignView à l'origine de cette fenêtre.
     * Permet d'actualiser la liste des essais après la création.
     * @param visualization l'objet VisualizationCampaignView à l'origine de cette classe
     */
    public void giveVisualizationCampaignView(VisualizationCampaignView visualization) {
        this.actorVisualization = visualization;
    }

    /**
     * Permet de transmettre à cette vue la page d'accueil.
     * @param view la vue de la page d'accueil
     */
    public void giveHomeView(HomeView view) {
        this.homeview = view;
    }

}
