package view;

import controller.Team;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Contrôleur de vue de la création des équipes. Pour créer une fenêtre de ce type,
 * il faut appeler son constructeur. Ensuite, doit être appelée giveAdminView(AdminWindowView)
 * pour pouvoir actualiser la liste des équipes après confirmation. Ensuite, appeler showStage()
 * pour afficher l'écran.
 */
public class CreateNewTeamView {

    private final Stage thisStage;

    @FXML private TextField textfieldNameTeam;
    @FXML private Button buttonConfirm, buttonCancel;

    @FXML private Label textError, textConfirm;
    private AdminWindowView adminView;

    /**
     * Constructeur de la fenêtre
     */
    public CreateNewTeamView() {

        thisStage = new Stage();

        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("fxml/CreateNewTeam.fxml"));

            loader.setController(this);

            thisStage.setScene(new Scene(loader.load()));

            thisStage.setTitle("Créer une nouvelle équipe");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Affiche le Stage créé dans le constructeur
     */
    public void showStage() {
        thisStage.showAndWait();
    }

    /**
     * Initialise la fenêtre et ses objets graphiques. Place les écouteurs.
     */
    @FXML
    private void initialize() {
        buttonCancel.setOnAction(event -> actionOnCancel());
        buttonConfirm.setOnAction(event -> actionOnConfirm());
    }

    /**
     * Action effectuée lors du clic sur le bouton Confirmer.
     */
    private void actionOnConfirm() {
        String nameTeam = textfieldNameTeam.getText();
        Team newTeam = Team.createNewTeam(nameTeam);
        if (newTeam == null) {
            showError("Une erreur est survenue lors de la création de l'équipe.");
        } else {
            showConfirmation("L'équipe "+nameTeam+" a bien été créée.");
            showError("");
            buttonConfirm.setDisable(true);
            buttonCancel.setText("Fermer");
            actualizeAdminView();
        }
    }

    /**
     * Action effectuée lors du clic sur le bouton Annuler.
     */
    private void actionOnCancel() {
        thisStage.close();
    }

    /**
     * Affiche une confirmation.
     * @param text texte de la confirmation
     */
    private void showConfirmation(String text) {
        textConfirm.setText(text);
    }

    /**
     * Affiche une erreur.
     * @param text texte de l'erreur
     */
    private void showError(String text) {
        textError.setText(text);
    }

    /**
     * Actualise la liste des équipes dans la fenêtre administrateur.
     */
    private void actualizeAdminView() {
        if (adminView != null) {
            adminView.actualizeTeamsList();
        }
    }

    /**
     * Donne l'instance de AdminWindowView à cette instance.
     * @param view instance de AdminWindowView
     */
    public void giveAdminView(AdminWindowView view) {
        this.adminView = view;
    }



}
