package view;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Hyperlink;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * Contrôleur de vue de la page à propos. Pour ouvrir cette fenêtre, vous devez appeler le constructeur,
 * puis appeler showStage() pour montrer la fenêtre à l'écran.
 */
public class AboutView {
    private final Stage thisStage;

    @FXML private Hyperlink linkToImageJWebSite;

    public AboutView() {
        thisStage = new Stage();

        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("fxml/About.fxml"));

            loader.setController(this);

            thisStage.setScene(new Scene(loader.load()));

            thisStage.setTitle("A propos");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Affiche le Stage créé dans le constructeur
     */
    public void showStage() {
        thisStage.showAndWait();
    }

    /**
     * Initialise la fenêtre et ses objets graphiques, place les écouteurs.
     */
    @FXML
    private void initialize() {
        linkToImageJWebSite.setOnAction(event -> openWebpage("https://imagej.nih.gov/ij/") );

    }

    /**
     * Méthode qui permet d'ouvrir une page web.
     * @param url adresse url de la page web
     */
    private static void openWebpage(String url) {
        try {
            java.awt.Desktop.getDesktop().browse(new URI(url));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }
}
