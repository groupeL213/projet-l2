package view;

import controller.Assay;
import controller.Campaign;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Contrôleur de vue de la modification d'une campagne. Pour ouvrir une telle fenêtre,
 * vous devez commnencer par appeler son constructeur. Ensuite, doivent être appelées :
 *   - EN CAS DE MODICIATION D'UN ESSAI : giveAssay(Assay)
 *   - EN CAS DE MODIFICATION D'UNE CAMPAGNE : giveCampaign(Campaign)
 *   - giveVisualization(VisualizationCampaignView || VisualizationAssayView)
 *   - showStage() pour afficher l'écran
 */
public class ModifyCampaignOrAssayView {


    private final Stage thisStage;
    private Campaign campaignToModify;
    private Assay assayToModify;
    private VisualizationCampaignView visualizationCampaign;
    private VisualizationAssayView visualizationAssay;

    @FXML private TextField textName;
    @FXML private TextArea textDescription;
    @FXML private Button buttonCancel, buttonConfirm;
    @FXML private Text textError;
    @FXML private Label titlePage;



    /**
     * Constructeur de la fenêtre.
     */
    public ModifyCampaignOrAssayView() {

        thisStage = new Stage();

        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("fxml/ModifyCampaignOrAssay.fxml"));

            loader.setController(this);

            thisStage.setScene(new Scene(loader.load()));

            thisStage.setTitle("Modifier la campagne");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Affiche le Stage créé dans le constructeur
     */
    public void showStage() {
        thisStage.showAndWait();
    }

    /**
     * Initialise la fenêtre et ses objets graphiques, met en place les écouteurs
     */
    @FXML
    private void initialize() {
        buttonCancel.setOnAction(event -> actionOnCancel());
        buttonConfirm.setOnAction(event -> actionOnConfirm());


    }

    /**
     * Permet de donner la campagne à modifier à cette instance.
     * Cette méthode doit être appelée si la modification concerne une campagne.
     * Sinon, giveAssay() doit être appelée.
     * @param c campagne à modifier
     */
    public void giveCampaign(Campaign c) {
        this.campaignToModify = c;

        if (campaignToModify != null) {
            titlePage.setText("Modification de la campagne");
            textName.setText(campaignToModify.getName());
            textDescription.setText(campaignToModify.getDescription());
        }
    }

    /**
     * Permet de donner l'essai à modifier à cette instance.
     * Cette méthode doit être appelée si la modification concerne un essai.
     * Sinon, giveCampagne() doit être appelée.
     * @param a essai à modifier
     */
    public void giveAssay(Assay a) {
        this.assayToModify = a;

        if (assayToModify != null) {
            titlePage.setText("Modification de l'essai");
            textName.setText(assayToModify.getName());
            textDescription.setText(assayToModify.getDescription());
        }
    }

    /**
     * Permet de donner la fenêtre de visualisation de campagne à l'origine de cette fenêtre.
     * Permettra d'actualiser les informations sur cette fenêtre de visualisation.
     * @param view instance de la visualisation de la campagne qu'on modifie
     */
    public void giveVisualization(VisualizationCampaignView view) {
        if (this.visualizationAssay == null) this.visualizationCampaign = view;
    }

    /**
     * Permet de donner la fenêtre de visualisation de l'essai à l'origine de cette fenêtre.
     * Permettra d'actualiser les informations sur cette fenêtre de visualisation.
     * @param view instance de la visualisation de l'essai qu'on modifie
     */
    public void giveVisualization(VisualizationAssayView view) {
        if (this.visualizationCampaign == null) this.visualizationAssay = view;
    }

    /**
     * Action effectuée lors du clic sur Confirmer.
     */
    private void actionOnConfirm() {
        if (textName.getText().equals("")) {
            showError("Vous devez entrer un nom.");
        } else {
            boolean modif=false;
            if (campaignToModify != null) {
                modif = campaignToModify.modify(textName.getText(), textDescription.getText());
            } else if (assayToModify != null) {
                modif = assayToModify.modify(textName.getText(), textDescription.getText());
            }
            refreshParent();
            actionAfterModification(modif);
        }
    }

    /**
     * Action effectuée lors du clic sur Annuler.
     */
    private void actionOnCancel() {
        thisStage.close();
    }

    /**
     * Affiche une erreur.
     * @param text texte de l'erreur
     */
    private void showError(String text) {
        this.textError.setText(text);
    }

    /**
     * Rafraîchit la page parente (visualisation essai s'il s'agit d'un essai, campagne sinon).
     */
    private void refreshParent() {
        if (visualizationCampaign != null) visualizationCampaign.refreshNameAndDescription();
        if (visualizationAssay != null) visualizationAssay.refreshNameAndDescription();
    }

    /**
     * Action effectuée après la modification de l'essai/campagne.
     * @param modif si la modification est un succès
     */
    private void actionAfterModification(boolean modif) {
        if (modif) thisStage.close();
        else showError("Une erreur est survenue lors de la modification.");
    }



}
