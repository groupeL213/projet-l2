package view;

import controller.*;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import model.ImageRequests;

import javax.imageio.ImageIO;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.SQLException;

/**
 * Contrôleur de vue de l'écran d'importation d'une nouvelle image. Pour créer cet écran, vous devez
 * appeler son constructeur, et appeler ces méthodes :
 *    - giveConnectedUser(User) Obligatoire
 *    - giveCurrentAssay(Assay) Obligatoire
 *    - giveCurrentCampaign(Campaign) Facultatif : si cette méthode n'est pas appelée, la campagne
 *    dans laquelle on ajoute l'image ne sera pas nommée.
 *    - giveVisualizationAssay(VisualizationAssay) Facultatif : si cette méthode n'est pas appelée,
 *    l'écran de visualisation de l'essai lié à la nouvelle image ne sera pas rafraîchie.
 */
public class ImportNewImageView {

    private final Stage thisStage;
    private User connectedUser;
    private Assay currentAssay;
    private Campaign currentCampaign;
    private VisualizationAssayView visualizationActor;

    @FXML private Button importButton;
    @FXML private Hyperlink cancelLink, confirmLink;
    @FXML private TextField imageName;
    @FXML private ImageView selectedImageView;
    @FXML private Text textError;
    @FXML private Label textAssay, textCampaign;

    String chemin = null;

    int fondNoirInt = 0;
    boolean fondNoirBool = false;

    BufferedImage img = null;
    Image imgFX = null;


    /**
     * Constructeur de la fenêtre.
     */
    public ImportNewImageView() {

        thisStage = new Stage();

        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("fxml/ImportNewImage.fxml"));

            loader.setController(this);

            thisStage.setScene(new Scene(loader.load()));

            thisStage.setTitle("Importer une nouvelle image");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Affiche le Stage créé dans le constructeur
     */
    public void showStage() {
        thisStage.showAndWait();
    }

    /**
     * Initialise la fenêtre et ses objets graphiques, met en place les écouteurs.
     */
    @FXML
    private void initialize() {
        importButton.setOnAction(event -> actionImportImage());
        cancelLink.setOnAction(event -> actionLinkCancel());
        confirmLink.setOnAction(event -> actionLinkConfirm());

    }

    /**
     * Action effectuée lors du clic sur le bouton importer image.
     */
    private void actionImportImage() {
        Frame parent = new Frame();
        FileDialog fd = new FileDialog((Frame)parent, "Ouvrir");
        fd.show();
        if(fd.getFile() == null)
            return;
        chemin = fd.getDirectory()+fd.getFile();
        parent.dispose();

        File file = new File(chemin);
        try {
            img = ImageIO.read(file);
            imgFX = new Image(new FileInputStream(chemin), 350,250,true,false);

        } catch (IOException e) {
            e.printStackTrace();
        }
        selectedImageView.setImage(imgFX);

        importButton.setText("Changer d'image");

        boolean blackBackground = InformationWindow.create("L'image a t-elle un fond noir ?",
                InformationWindow.CONFIRMATION);

        if(blackBackground){
            fondNoirInt = 1;
            fondNoirBool = true;
        }

    }

    /**
     * Action effectuée lors du clic sur le lien de confirmation.
     */
    private void actionLinkConfirm() {
        if (imageName.getText().equals("")) {
            showError("Vous devez indiquer le nom de l'image.");
            return;
        }
        else if (img == null){
            showError("Vous n'avez pas importé d'image.");
            return;
        }
        else {
            eraseError();
        }

        String date = (new Date()).toString();
        int idImage =-1;

        try {

            ImageJ2 ij = new ImageJ2(img, imageName.getText(), currentAssay.getAlgo(), fondNoirBool);
            int nbrCells = ij.runAnalyze();

            ij.showConvertedImage();

            boolean write = InformationWindow.create("Confirmer le résultat et l'écriture dans la base de données ?" +
                    "\n "+nbrCells+" cellules ont été trouvées", InformationWindow.CONFIRMATION);

           if(write) {
                FileInputStream fis = new FileInputStream(chemin);
                idImage = ImageRequests.requestForNewImage(imageName.getText(), fis, date, fondNoirInt,
                        currentAssay.getId(), connectedUser.getId());
                fis.close();
                currentAssay.modifyLastModification(new Date());

                ij.writePiles(idImage);
            }//if

            ij.closeImage();
        } catch (SQLException | IOException e) {
            e.printStackTrace();
        }

        if (visualizationActor != null) {
            visualizationActor.actualizeImagesList();
            visualizationActor.refreshNameAndDescription();
            visualizationActor.actualizeStats();
        }
        resetVariables();
    }

    /**
     * Remet à zéro les variables de l'instance.
     */
    private void resetVariables() {
        importButton.setText("Importer");
        img = null;
        imgFX = null;
        chemin = "";
        imageName.setText("");
        fondNoirInt = 0;
        fondNoirBool = false;
        selectedImageView.setImage(null);
    }

    /**
     * Action effectuée lors du clic sur le lien Annuler.
     */
    private void actionLinkCancel() {
        thisStage.close();
    }

    /**
     * Affiche une erreur.
     * @param error texte de l'erreur
     */
    private void showError(String error) {
        textError.setText(error);
    }

    /**
     * Efface l'erreur.
     */
    private void eraseError() {
        textError.setText("");
    }

    /**
     * Permet de donner l'utilisateur connecté à cette instance.
     * @param connectedUser utilisateur connecté
     */
    public void giveConnectedUser(User connectedUser) {
        this.connectedUser = connectedUser;
    }

    /**
     * Permet de donner l'essai de l'image à cette instance.
     * @param assay essai de l'image
     */
    public void giveCurrentAssay(Assay assay) {
        this.currentAssay = assay;
        if (currentAssay != null) textAssay.setText(textAssay.getText() + currentAssay.getName());
    }

    /**
     * Permet de donner la campagne de l'image à cette instance.
     * @param campaign campagne de l'image
     */
    public void giveCurrentCampaign(Campaign campaign) {
        this.currentCampaign = campaign;
        if (currentCampaign != null) textCampaign.setText(textCampaign.getText() + currentCampaign.getName());
    }

    /**
     * Permet de donner la visualisation de l'essai à l'origine de cette instance.
     * Permet d'actualiser la liste des images après importation.
     * @param visu instance de la visualisation de l'essai
     */
    public void giveVisualizationAssay(VisualizationAssayView visu) {
        this.visualizationActor = visu;
    }
}
