package view;

import controller.Campaign;
import controller.Team;
import controller.User;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import model.UserRequests;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Contrôleur de vue de la création d'une campagne. Pour créer une fenêtre de ce type, il faut appeler son
 * constructeur. Ensuite, doivent être appelées :
 *    - giveConnectedUser(User) pour donner l'utilisateur connecté (obligatoire)
 *    - giveHomeView(HomeView) pour donner l'instance de HomeView à l'origine de cette fenêtre
 *    - showStage() pour montrer l'écran
 */
public class CreateNewCampaignView {

    private final Stage thisStage;
    private User connectedUser;
    private HomeView homeview;

    @FXML private Button cancelButton;
    @FXML private Button confirmButton;
    @FXML private TextField newCampaignName;
    @FXML private TextArea newCampaignDescription;
    @FXML private Text textErrorOrConfirm;
    @FXML private ComboBox<Team> comboBoxTeam;

    /**
     * Constructeur de la fenêtre
     */
    public CreateNewCampaignView() {

        thisStage = new Stage();

        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("fxml/CreateNewCampaign.fxml"));

            loader.setController(this);

            thisStage.setScene(new Scene(loader.load()));

            thisStage.setTitle("Création d'une campagne");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Affiche le Stage créé dans le constructeur
     */
    public void showStage() {
        thisStage.showAndWait();
    }

    /**
     * Initialise la fenêtre et ses objets graphiques, place les écouteurs.
     */
    @FXML
    private void initialize() {
        cancelButton.setOnAction(event -> actionButtonCancel());
        confirmButton.setOnAction(event -> actionButtonConfirm());

    }

    /**
     * Action effectuée lors du clic sur le bouton Confirmation.
     */
    private void actionButtonConfirm() {
        if (newCampaignName.getText().equals("")) {
            showError("Vous devez indiquer le nom de la campagne.");
        } else {
            Campaign campaign = Campaign.createNewCampaign(newCampaignName.getText(),
                                                           newCampaignDescription.getText(),
                                                           comboBoxTeam.getValue(), connectedUser);
            showConfirm("La campagne "+campaign.getName()+" a bien été créée.");
            confirmButton.setDisable(true);
            cancelButton.setText("Fermer");
            Thread threadActualize = new Thread() {
                @Override
                public void run() {
                    actualizeListHomeView();
                }
            };
            threadActualize.start();
        }
    }

    /**
     * Action effectuée lors du clic sur le bouton Annuler.
     */
    private void actionButtonCancel() {
        thisStage.close();
    }

    /**
     * Affiche une erreur.
     * @param error texte de l'erreur
     */
    private void showError(String error) {
        textErrorOrConfirm.setText(error);
        textErrorOrConfirm.setFill(Color.DARKRED);
    }

    /**
     * Affiche une confirmation.
     * @param confirm texte de la confirmation
     */
    private void showConfirm(String confirm) {
        textErrorOrConfirm.setText(confirm);
        textErrorOrConfirm.setFill(Color.DARKGREEN);
    }

    /**
     * Actualise la liste des équipes.
     * @throws Exception erreur déclenchée lorsque l'utilisateur connecté n'a pas été initialisé
     */
    private void actualizeTeamList() throws Exception {
        if (connectedUser == null) {
            throw new Exception("Error: you must give the connectedUser using " +
                    "giveConnectedUser() before call this method.");
        }
        else {
            ArrayList<Integer> teamIDList = UserRequests.requestForTeamsOfUser(connectedUser.getId());

            ArrayList<Team> teamList = new ArrayList<>();
            for (Integer teamID : teamIDList) {
                teamList.add(new Team(teamID));
            }
            ObservableList<Team> observableList = FXCollections.observableArrayList(teamList);
            comboBoxTeam.setItems(observableList);
            comboBoxTeam.getSelectionModel().select(0);


        }
    }

    /**
     * Actualise la liste des essais et des campagnes sur la page d'accueil.
     */
    private void actualizeListHomeView() {
        if (this.homeview != null) {
            homeview.actualizeAssaysList();
            homeview.actualizeCampaignsList();
        }
    }

    /**
     * Permet de donner l'utilisateur connecté à cette instance.
     * @param connectedUser utilisateur connecté
     */
    public void giveConnectedUser(User connectedUser) {
        this.connectedUser = connectedUser;
        try {
            actualizeTeamList();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Permet de transmettre à cette vue la page d'accueil.
     * @param view la vue de la page d'accueil
     */
    public void giveHomeView(HomeView view) {
        this.homeview = view;
    }

}
