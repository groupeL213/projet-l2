package view;

import controller.Team;
import controller.User;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import model.TeamRequests;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Controller-View de la vue CreateNewAccountView. Définit les actionListener de la vue.
 * Pour ouvrir une fenêtre de ce type, il faut appeler le constructeur. Ensuite, doivent être appelées :
 *    - giveAdminView(AdminWindowView) pour donner l'instance de la fenêtre d'accueil administrateur
 *    - showStage() pour afficher la fenêtre à l'écran
 */
public class CreateNewAccountView {

    @FXML private TextField newAccountLastName;
    @FXML private TextField newAccountFirstName;
    @FXML private TextField newAccountPassword;
    @FXML private TextField newAccountPasswordRepeated;
    @FXML private Text textErrorOrConfirm, titlePage;
    @FXML private Button newAccountCancelButton;
    @FXML private Button newAccountConfirmButton;
    @FXML private VBox vboxTeamsList;
    @FXML private ScrollPane scrollPaneTeams;
    @FXML private Label infoNoTeamsForAdmin;


    private final ArrayList<CheckBox> checkBoxTeamsList;

    /**
     * True si la création de compte concerne un administrateur, false sinon.
     */
    private boolean creationOfAdminAccount;

    private final Stage thisStage;
    private AdminWindowView adminView;

    /**
     * Constructeur de la fenêtre.
     */
    public CreateNewAccountView() {

        thisStage = new Stage();

        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("fxml/CreateNewAccount.fxml"));

            loader.setController(this);

            thisStage.setScene(new Scene(loader.load()));

            thisStage.setTitle("Ajouter un nouvel utilisateur");

        } catch (IOException e) {
            e.printStackTrace();
        }

        this.checkBoxTeamsList = new ArrayList<>();
        actualizeTeamsCheckBoxList();
    }

    /**
     * Affiche le Stage créé dans le constructeur
     */
    public void showStage() {
        thisStage.showAndWait();
    }

    /**
     * Initialise la fenêtre et ses objets graphiques, place les écouteurs.
     */
    @FXML
    private void initialize() {
        newAccountConfirmButton.setOnAction(this::actionButtonConfirm);
        newAccountCancelButton.setOnAction(this::actionButtonCancel);
    }

    /**
     * Controller-view du bouton de confirmation.
     * Vérifie que les champs ne sont pas vides, puis essaye de créer l'utilisateur.
     * @param event L'événement du clic sur le bouton Confirmation.
     */
    private void actionButtonConfirm(ActionEvent event) {
        if (newAccountLastName.getText().equals("") || newAccountFirstName.getText().equals("") ||
                newAccountPassword.getText().equals("") || newAccountPasswordRepeated.getText().equals("")) {
            showError("Tous les champs doivent être remplis.");
        }
        else {

            if (newAccountPassword.getText().equals(newAccountPasswordRepeated.getText())) {

                User u = User.createNewUser(newAccountLastName.getText(), newAccountFirstName.getText(),
                        newAccountPassword.getText(), creationOfAdminAccount);

                if (u == null) {
                    System.out.println("erreur");
                    showError("Une erreur est survenue lors de la création de cet utilisateur.");
                } else {
                    System.out.println("utilisateur créé");
                    showConfirmation(u.getLogin());
                    newAccountConfirmButton.setDisable(true);
                    u.addToTeamList(extractTeamChecked(this.checkBoxTeamsList));
                    actualizeUserListAdminView();
                }
            }
            else {
                showError("Les mots de passe ne correspondent pas.");
            }
        }
    }

    /**
     * Action effectuée lors du clic sur Annuler.
     * @param event événement à l'origine
     */
    private void actionButtonCancel(ActionEvent event) {
        thisStage.close();
    }

    /**
     * Affiche la confirmation de création, ainsi que le login créé
     * et modifie le texte du bouton "Annuler" en "Fermer".
     * @param login le login du nouvel utilisateur
     */
    private void showConfirmation(String login) {
        textErrorOrConfirm.setText("Compte créé. Identifiant du compte : "+login);
        textErrorOrConfirm.setFill(Color.DARKGREEN);
        newAccountCancelButton.setText("Fermer");
    }

    /**
     * Affiche l'erreur dans le champ d'erreur.
     * @param error Le texte de l'erreur à afficher.
     */
    private void showError(String error) {
        textErrorOrConfirm.setText(error);
        textErrorOrConfirm.setFill(Color.DARKRED);
    }

    /**
     * Extrait la liste des équipes qui ont été cochées.
     * @param checkboxList la liste des CheckBox d'équipes
     * @return la liste d'id des équipes cochées
     */
    private ArrayList<Integer> extractTeamChecked(ArrayList<CheckBox> checkboxList) {
        ArrayList<Integer> result = new ArrayList<>();
        for (CheckBox check : checkboxList) {
            if (check.isSelected()) {
                result.add( ((Team)check.getUserData()).getId() );
            }
        }
        return result;
    }

    /**
     * Permet de préciser si la création du compte concerne un administrateur ou un utilisateur.
     * Cette méthode permet d'adapter l'interface graphique selon le cas. Un administrateur
     * ne pouvant pas rejoindre d'équipes, les CheckBox des équipes seront désactivées.
     * @param precise booléen vrai si administrateur ; false sinon
     */
    public void preciseIfCreationOfAdmin(boolean precise) {
        this.creationOfAdminAccount = precise;
        if (creationOfAdminAccount) {
            thisStage.setTitle("Ajouter un nouvel administrateur");
            titlePage.setText("Créer un nouveau compte administrateur");
            vboxTeamsList.setDisable(true);
            Tooltip tooltip = new Tooltip("Les équipes ne peuvent pas être choisies " +
                    "pour la création d'un compte administrateur.");
            Tooltip.install(scrollPaneTeams, tooltip);

        } else {
            infoNoTeamsForAdmin.setVisible(false);
        }
    }

    /**
     * Actualise la liste des CheckBox des équipes.
     */
    public void actualizeTeamsCheckBoxList() {
        try {
            ArrayList<Integer> teamsList = TeamRequests.teamsList();

            for (int id : teamsList) {
                Team team = new Team(id);
                CheckBox c = new CheckBox(team.getName());
                c.setUserData(team);
                this.checkBoxTeamsList.add(c);

            }
            vboxTeamsList.getChildren().addAll(this.checkBoxTeamsList);

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

    }

    /**
     * Actualise la liste des utilisateurs dans la fenêtre administrateur.
     */
    public void actualizeUserListAdminView() {
        if (this.adminView != null) {
            adminView.actualizeUsersLists();
        }
    }



    /**
     * Donne l'instance de AdminWindowView à cette instance.
     * @param adminWindowView instance de AdminWindowView
     */
    public void giveAdminView(AdminWindowView adminWindowView) {
        this.adminView = adminWindowView;
    }
}
