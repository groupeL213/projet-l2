package view;

import controller.Main;
import controller.Team;
import controller.User;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import model.TeamRequests;
import model.UserRequests;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Contrôleur de vue de la fenêtre d'administrateur.
 */
public class AdminWindowView {

    private final Stage thisStage;
    private User connectedUser;
    private ConnectionView connectionView;

    @FXML private Hyperlink createNewUser, createNewAdmin, createNewTeam;

    @FXML private Text textConnectedAs;

    @FXML private TableView<User> tableAdmin, tableUser;
    @FXML private TableColumn<User, String> nameAdmin, firstnameAdmin, loginAdmin, nameUser, firstnameUser, loginUser;

    @FXML private TableView<Team> tableTeam;
    @FXML private TableColumn<Team, String> nameTeam, creationDateTeam;
    @FXML private MenuItem menuAbout, optionCreateAlgo, disconnectOption, exitOption;

    /**
     * Constructeur de la fenêtre.
     */
    public AdminWindowView() {

        thisStage = new Stage();

        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("fxml/AdminWindow.fxml"));

            loader.setController(this);

            thisStage.setScene(new Scene(loader.load()));

            thisStage.setTitle("Espace administrateur");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Affiche le Stage créé dans le constructeur
     */
    public void showStage() {
        thisStage.showAndWait();
    }

    /**
     * Initialise la fenêtre et ses objets graphiques, met en place les écouteurs.
     */
    @FXML
    private void initialize() {
        createNewAdmin.setOnAction(event -> actionCreateNewAccount(true));
        createNewUser.setOnAction(event -> actionCreateNewAccount(false));
        createNewTeam.setOnAction(event -> actionCreateNewTeam());

        optionCreateAlgo.setOnAction(event -> actionCreateAlgo());
        disconnectOption.setOnAction(event -> actionDisconnect());
        exitOption.setOnAction(event -> {
            thisStage.close();
            if (Main.automaticReconnection) connectionView.reconnexion.stop();
            Platform.exit();
        });

        menuAbout.setOnAction(event -> actionAboutView());

        actualizeUsersLists();
        actualizeTeamsList();


        tableTeam.setRowFactory(tv -> {
            TableRow<Team> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
                    Team rowData = row.getItem();
                    VisualizationTeamView vizu = new VisualizationTeamView();
                    vizu.giveTeam(rowData);
                    vizu.giveConnectedUser(connectedUser);
                    vizu.giveAdminView(this);
                    vizu.showStage();
                }
            });
            return row ;
        });


        tableUser.setRowFactory(tv -> actionClicOnUser());
        tableAdmin.setRowFactory(tv -> actionClicOnUser());


    }

    /**
     * Ouvre la fenetre de création d'un algorithme
     */
    private void actionCreateAlgo() {
        CreateNewAlgorithmView newAlgoView = new CreateNewAlgorithmView();
        newAlgoView.showStage();
    }

    /**
     * Permet de donner l'utilisateur connecté à cette instance.
     * @param user utilisateur connecté
     */
    public void giveConnectedUser(User user) {
        this.connectedUser = user;
        textConnectedAs.setText(textConnectedAs.getText() + " " + connectedUser.getName() + " "
                + connectedUser.getFirstname() + " (" + connectedUser.getLogin() + ")");
    }

    /**
     * Permet de donner l'instance de ConnectionView à l'origine de cette fenêtre. Ferme la fenêtre
     * de ConnectionView.
     * @param connect instance de ConnectionView
     */
    public void giveConnectionView(ConnectionView connect) {
        this.connectionView = connect;
        connectionView.hideStage();
    }

    /**
     * Actualise la liste des utilisateurs et des administrateurs.
     */
    public void actualizeUsersLists() {
        ObservableList<User> dataUser = FXCollections.observableArrayList();
        ObservableList<User> dataAdmin = FXCollections.observableArrayList();

        try {
            ArrayList<Integer> userList = UserRequests.usersList();

            for(int id : userList) {
                User u = new User(id);
                if (u.isAdmin())
                    dataAdmin.add(u);
                else
                    dataUser.add(u);
            }

            nameAdmin.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getName()));
            firstnameAdmin.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getFirstname()));
            loginAdmin.setCellValueFactory(cellData -> new SimpleStringProperty(String.valueOf(cellData.getValue().getLogin())));
            nameUser.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getName()));
            firstnameUser.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getFirstname()));
            loginUser.setCellValueFactory(cellData -> new SimpleStringProperty(String.valueOf(cellData.getValue().getLogin())));


            tableAdmin.setItems(dataAdmin);
            tableUser.setItems(dataUser);

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    /**
     * Actualise la liste des équipes.
     */
    public void actualizeTeamsList() {
        ObservableList<Team> data = FXCollections.observableArrayList();

        try {
            ArrayList<Integer> teamList = TeamRequests.teamsList();

            for(int id : teamList) {
                Team t = new Team(id);
                data.add(t);
            }
            nameTeam.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getName()));
            creationDateTeam.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getCreationDate().toSimpleForm()));

            tableTeam.setItems(data);

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    /**
     * Action effectuée lors du clic sur Nouvel administrateur ou Nouvel utilisateur.
     * @param admin vrai si le compte sera administrateur ; faux sinon
     */
    private void actionCreateNewAccount(boolean admin) {
        CreateNewAccountView create = new CreateNewAccountView();
        create.preciseIfCreationOfAdmin(admin);
        create.giveAdminView(this);
        create.showStage();
    }

    /**
     * Action effectuée lorsqu'on accès au menu "A propos".
     */
    private void actionAboutView() {
        AboutView about = new AboutView();
        about.showStage();
    }

    /**
     * Action effectuée lors de la création d'une équipe.
     */
    private void actionCreateNewTeam() {
        CreateNewTeamView create = new CreateNewTeamView();
        create.giveAdminView(this);
        create.showStage();
    }

    /**
     * Action effectuée lors du double-clic sur une entrée d'un tableau d'utilisateur.
     * @return la ligne sur laquelle on a double-cliqué
     */
    private TableRow<User> actionClicOnUser() {
        TableRow<User> row = new TableRow<>();
        row.setOnMouseClicked(event -> {
            if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
                User rowData = row.getItem();
                VisualizationUserView vizu = new VisualizationUserView();
                vizu.giveUsers(rowData, connectedUser);
                vizu.giveAdminView(this);
                vizu.showStage();
            }
        });
        return row ;
    }

    /**
     * Action effectuée quand on veut se déconnecter.
     */
    private void actionDisconnect() {
        connectionView.giveHomeView(thisStage);
        connectionView.reset();
        connectionView.showStage();
    }
}
