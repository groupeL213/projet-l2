package view;

import controller.User;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Ouvre une fenêtre qui demande de confirmer le mot de passe de l'utilisateur (userToShow).
 * Pour ouvrir cette fenêtre, il faut appeler son constructeur et appeler giveUsers(User, User) pour donner
 * l'utilisateur connecté puis l'utilisateur dont on doit vérifier le mot de passe.
 * Pour obtenir le résultat (si le mot de passe est correct, il faut appeler getCorrectPassword().
 */
public class ConfirmPasswordView {

    private final Stage thisStage;

    private User userToShow, connectedUser;

    private boolean passwordCorrect;

    @FXML private Button buttonConfirm, buttonCancel;
    @FXML private PasswordField fieldPassword;


    /**
     * Constructeur de la fenêtre.
     */
    public ConfirmPasswordView() {

        thisStage = new Stage();

        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("fxml/ConfirmPassword.fxml"));

            loader.setController(this);

            thisStage.setScene(new Scene(loader.load()));

            thisStage.setTitle("Visualiser un compte utilisateur");

        } catch (IOException e) {
            e.printStackTrace();
        }

        passwordCorrect = false;

    }

    /**
     * Affiche le Stage créé dans le constructeur
     */
    public void showStage() {
        thisStage.showAndWait();
    }

    /**
     * Initialise la fenêtre et ses objets graphiques, ainsi que les écouteurs.
     */
    @FXML
    private void initialize() {
        buttonCancel.setOnAction(event -> actionOnCancel());
        buttonConfirm.setOnAction(event -> actionOnConfirm());
    }

    /**
     * Action effectuée au clic sur Confirmer.
     */
    private void actionOnConfirm() {
        passwordCorrect = userToShow != null && User.verifyPassword(userToShow.getLogin(), fieldPassword.getText());
        thisStage.close();
    }

    /**
     * Action effectuée au clic sur Annuler.
     */
    private void actionOnCancel() {
        passwordCorrect = false;
        thisStage.close();
    }

    /**
     * Donne les utilisateurs connecté et à montrer à cette instance.
     * @param connectedUser utilisateur connecté
     * @param userToShow utilisateur à montrer
     */
    public void giveUsers(User connectedUser, User userToShow) {
        this.userToShow = userToShow;
        this.connectedUser = connectedUser;
    }

    /**
     * Permet d'obtenir le booléen qui dit si le mot de passe tapé est correct.
     * @return true si mot de passe correct ; false sinon
     */
    public boolean getCorrectPassword() {
        return this.passwordCorrect;
    }



}
