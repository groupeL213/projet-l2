package tests;

import static org.junit.Assert.*;

import java.sql.SQLException;
import java.util.ArrayList;

import controller.Campaign;

import controller.Team;
import controller.User;
import org.junit.Test;

/**
 * Cette classe teste quelques méthodes de la classe Campaign.
 */
public class CampaignTest {

    /**
     * Ce test est pour vérifier quel est l'id de l'équipe (id = 6) liée à la campagne (id = 19)
     * @throws SQLException Erreur d'accès à la base de données
     */
    @Test
    public void testGetTeam() throws SQLException {
        Team t = new Team(6);
        int expected = t.getId();

        Campaign c = new Campaign(19);
        Team resultT = c.getTeam();

        int result = resultT.getId();

        assertEquals(expected, result);

    }

    /**
     * Ce test est pour la méthode qui renvoit le nombre d'essai dans une campagne (ici la campagne avec l'id = 19).
     * Le résultat attendu est 2.
     * @throws SQLException Erreur d'accès à la base de données
     */
    @Test
    public void testGetNumberOfAssays() throws SQLException {
        int expected = 2;

        Campaign c = new Campaign(19);
        int result = c.getNumberOfAssays();

        assertEquals(expected, result);

    }

    /**
     * Ce test vérifie le nombre d'images d'une campagne (ici la campagne avec l'id = 19).
     * Le résultat attendu ici est 4.
     * @throws SQLException Erreur d'accès à la base de données
     */
    @Test
    public void testGetNumberOfImages() throws SQLException {
        int expected = 4;

        Campaign c = new Campaign(19);
        int result = c.getNumberOfImages();

        assertEquals(expected, result);

    }

    /**
     * Ce test vérifie le nombre de cellules de la campagne avec l'id = 19.
     * Le résultat attendu est 564.
     * @throws SQLException Erreur d'accès à la base de données
     */
    @Test
    public void testGetNumberOfCells() throws SQLException {
        int expected = 564;

        Campaign c = new Campaign(19);
        int result = c.getNumberOfCells();

        assertEquals(expected, result);

    }

    /**
     * Ce test vérifie la moyenne du nombre de cellules de la campagne avec l'id = 19.
     * Le résultat attendu est 141.0.
     * @throws SQLException Erreur d'accès à la base de données
     */
    @Test
    public void testGetAverageNumberOfCellsPerImage() throws SQLException {
        float expected = (float) 141.0;
        float delta = 2;

        Campaign c = new Campaign(19);
        float result = c.getAverageNumberOfCellsPerImage();

        assertEquals(expected, result, delta);

    }

    /**
     * Ce test est pour vérifier la méthode CampaignsSeenByUSer qui permet de savoir quelle campagne peut voir
     * l'utilisateur (ici l'utilisateur est "UtilisateurJUnit" et la campagne qu'il peut voir est la campagne avec
     * l'id = 19)
     * @throws SQLException Erreur d'accès à la base de données
     */
    @Test
    public void testGetCampaignsSeenByUser() throws SQLException {
        Campaign c = new Campaign(19);

        User u = new User("UtilisateurJUnit");

        ArrayList<Campaign> expected = new ArrayList<Campaign>();
        expected.add(c);

        ArrayList<Campaign> result = c.getCampaignsSeenByUser(u);

        int i = 0;

        while(i < result.size()) {
            assertEquals(result.get(i).getId(), expected.get(i).getId());
            i++;
        }

    }

    /**
     * Ce test est pour vérifier la méthode CampaignsForATeam qui permet de savoir quelle campagne sont visibles
     * par l'équipe (ici l'id de l'équipe = 6, et l'id de la campagne = 19)
     * @throws SQLException Erreur d'accès à la base de données
     */
    @Test
    public void testGetCampaignsForATeam() throws SQLException {
        Campaign c = new Campaign(19);

        Team t = new Team(6);

        ArrayList<Campaign> expected = new ArrayList<Campaign>();
        expected.add(c);

        ArrayList<Campaign> result = c.getCampaignsForATeam(t);

        int i = 0;

        while(i < result.size()) {
            assertEquals(result.get(i).getId(), expected.get(i).getId());
            i++;
        }

    }

}
