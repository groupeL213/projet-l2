package tests;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.*;

import org.junit.Test;

import controller.User;

/**
 * Classe de test de la classe User.
 */
public class UserTest {

    /**
     * Ce test vérifie l'identité de l'utilisateur, ici Utilisateur JUnit
     */
    @Test
    public void testGetIdentite() {
        String expected = "Utilisateur JUnit";

        User u = new User("UtilisateurJUnit");
        String result = u.getIdentite();

        assertEquals(expected, result);
    }

    /**
     * Ce test vérifie l'id de l'utilisateur (ici UtilisateurJUnit)
     */
    @Test
    public void testGetId() {
        int expected = 12;

        User u = new User("UtilisateurJUnit");
        int result = u.getId();

        assertEquals(expected, result);
    }

    /**
     * Ce test vérifié le login (pour la connexion) d'un utilisateur (ici celui de UtilisateurJUnit)
     */
    @Test
    public void testGetLogin() {
        String expected = "UtilisateurJUnit";

        User u = new User("UtilisateurJUnit");
        String result = u.getLogin();

        assertEquals(expected, result);
    }

    /**
     * Ce test vérifie le nom de l'utilisateur (ici Utilisateur).
     */
    @Test
    public void testGetName() {
        String expected = "JUnit";

        User u = new User("UtilisateurJUnit");
        String result = u.getName();

        assertEquals(expected, result);
    }

    /**
     * Ce test vérifie le prénom de l'utilisateur (ici JUnit).
     */
    @Test
    public void testGetFirstname() {
        String expected = "Utilisateur";

        User u = new User("UtilisateurJUnit");
        String result = u.getFirstname();

        assertEquals(expected, result);
    }

    /**
     * Ce test vérifie si l'utilisateur est un administrateur (ici de l'utilisateur nommé 'AdminJUnit').
     */
    @Test
    public void testIsAdmin() {

        User u = new User("AdminJUnit");
        boolean result = u.isAdmin();

        assertEquals(true, result);
    }

    /**
     * Ce test vérifie le nombre d'images que peut voit l'utilisateur (login = 'UtilisateurJUnit').
     * Le résultat attendu est de 4.
     */
    @Test
    public void testGetNumberOfImportedImages() {
        int expected = 4;

        User u = new User("UtilisateurJUnit");
        int result = u.getNumberOfImportedImages();

        assertEquals(expected, result);
    }

    /**
     * Ce test vérifie le nombre de cellules dans les images que peut voit l'utilisateur,
     * (login = 'UtilisateurJUnit').
     * Le résultat attendu est de 564.
     */
    @Test
    public void testGetNumberOfCells() {

        int expected = 564;

        User u = new User("UtilisateurJUnit");
        int result = u.getNumberOfCells();

        assertEquals(expected, result);
    }

    /**
     * Ce test vérifie la moyenne du nombre de cellules dans les images que peut voit l'utilisateur,
     * (login = 'UtilisateurJUnit').
     * Le résultat attendu est de 141.0.
     */
    @Test
    public void testGetAverageCells() {
        float expected = (float) 141.0;
        float delta = 2;

        User u = new User("UtilisateurJUnit");
        float result = u.getAverageCells();

        assertEquals(expected, result, delta);
    }

    /**
     * Ce test vérifie que les accents sont bien remplacées dans les chaînes de caractères.
     */
    @Test
    public void testRemoveAccents() {

        String expected = "Cedric";

        String firstname = "Cédric";

        String result = "";
        User u = new User("UtilisateurJUnit");
        result = u.removeAccents(firstname);

        assertThat(result, is(expected));
    }

}