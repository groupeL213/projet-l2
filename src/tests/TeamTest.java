package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import controller.Date;
import controller.Team;

import java.sql.SQLException;

/**
 * Cette classe teste quelques méthodes de la classe Team.
 */
public class TeamTest {

    /**
     * Ce test vérifie l'id de l'équipe (ici on vérifie si l'id de l'équipe 6 est bien égal à 6).
     */
    @Test
    public void testGetId() {

        int expected = 6;

        Team t = new Team(6);
        int result = t.getId();

        assertEquals(expected, result);

    }

    /**
     * Ce test vérifie le nom de l'équipe (ici pour l'idEquipe = 6).
     * Le nom attendu est 'Equipe pour JUnit'.
     */
    @Test
    public void testGetName() {

        String expected = "Equipe pour JUnit";

        Team t = new Team(6);
        String result = t.getName();

        assertEquals(expected, result);

    }

    /**
     * Ce test vérifie la date de création de l'équipe (ici pour l'idEquipe = 6).
     * La Date attendue est '2021-01-17 20:36:00'.
     */
    @Test
    public void testGetCreationDate() {

        Date expected = new Date("2021-01-17 20:36:00");


        Team t = new Team(6);
        Date result = t.getCreationDate();
        expected.compareTo(result);

    }

}