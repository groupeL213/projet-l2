package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Cette classe donne les requêtes par rapport aux équipes. Elle permet d'ajouter, de modifier et de supprimer
 * une équipe. On peut également obtenir les informations d'une équipe selon l'id donné et
 * d'obtenir la liste des équipes.
 */

public class TeamRequests {

    /* Requête SQL pour l'insertion d'une nouvelle ligne dans la table équipe */
    private static final String sql_InsertTeam = "INSERT INTO equipe"
            + "(nomEquipe, dateCreation)"
            + "VALUES (?, ?)";
    /* ----- */

    /**
     * Méthode d'ajout d'une nouvelle equipe et sa requête.
     * @param teamName le nom de la nouvelle équipe créée
     * @param creationDate la date de création de l'équipe
     * @throws SQLException Erreur d'accès à la base de données
     */
    public static void requestForNewTeam(String teamName, String creationDate)
            throws SQLException {

        Connection connection = Connection_BD.connect();

        PreparedStatement newTeam = connection.prepareStatement(sql_InsertTeam);
        newTeam.setString(1, teamName);
        newTeam.setString(2, creationDate);
        newTeam.executeUpdate();


    }

    /**
     * Méthode de modification d'une équipe (son nom) et sa requête.
     * @param idTeam l'id de l'équipe que l'on souhaite modifier
     * @param newName le nouveau nom de l'équipe
     * @throws SQLException Erreur d'accès à la base de données
     */
    public static void requestForUpdatingTeam(int idTeam, String newName)
            throws SQLException {

        Connection connection = Connection_BD.connect();

        PreparedStatement updateNameTeam = connection.prepareStatement
                ("UPDATE equipe SET nomEquipe = ? WHERE idEquipe = ?");
        updateNameTeam.setString(1, newName);
        updateNameTeam.setInt(2, idTeam);
        updateNameTeam.executeUpdate();


    }

    /**
     * Méthode de suppression d'une équipe et sa requête.
     * @param idTeam l'id de l'équipe à supprimer
     * @throws SQLException Erreur d'accès à la base de données
     */
    public static void requestForDeletingTeam(int idTeam) throws SQLException {

        Connection connection = Connection_BD.connect();

        PreparedStatement deleteTeam = connection.prepareStatement
                ("DELETE FROM equipe WHERE idEquipe = ?");
        deleteTeam.setInt(1, idTeam);
        deleteTeam.executeUpdate();


    }

    /**
     * Méthode pour obtenir les informations d'une équipe selon l'id fournit et sa requête.
     * @param idOfTeam l'id de l'équipe qu'on souhaite obtenir els information
     * @return les informations de l'équipe voulue
     * @throws SQLException Erreur d'accès à la base de données
     */
    public static HashMap<String, String> requestForInfosOfTeam(int idOfTeam)
            throws SQLException {
        Connection connection = Connection_BD.connect();

        HashMap<String, String> resultInfos = new HashMap<>();
        String idTeam, nameTeam, dateTeam;

        PreparedStatement infosTeam = connection.prepareStatement
                ("SELECT idEquipe, nomEquipe, dateCreation FROM equipe WHERE idEquipe = ?");
        infosTeam.setInt(1, idOfTeam);

        ResultSet infosTeamResult = infosTeam.executeQuery();
        while(infosTeamResult.next()) {
            idTeam = infosTeamResult.getString("idEquipe");
            nameTeam = infosTeamResult.getString("nomEquipe");
            dateTeam = infosTeamResult.getString("dateCreation");
            resultInfos.put("ID", idTeam);
            resultInfos.put("Name", nameTeam);
            resultInfos.put("Date of creation", dateTeam);
        }

        return resultInfos;

    }

    /**
     * Méthode pour obtenir la liste des équipes (leur id) et sa requête.
     * @return la liste des id des équipes
     * @throws SQLException Erreur d'accès à la base de données
     */
    public static ArrayList<Integer> teamsList() throws SQLException {
        Connection connection = Connection_BD.connect();
        ArrayList<Integer> teamsAllList = new ArrayList<>();

        PreparedStatement listOfTeams = connection.prepareStatement
                ("SELECT EP.idEquipe FROM equipe EP");
        ResultSet listOfTeamsResult = listOfTeams.executeQuery();
        while(listOfTeamsResult.next()) {
            teamsAllList.add(listOfTeamsResult.getInt("EP.idEquipe"));
        }

        return teamsAllList;

    }

    /**
     * Méthode pour savoir si l'équipe est encore reliée à une campagne.
     * @return true si oui ; false sinon
     */
    public static boolean isTeamConnectedToACampaign(int id) throws SQLException {

        String sql = "SELECT 1 FROM campagne WHERE idEquipe = ?";
        PreparedStatement statement = Connection_BD.connect().prepareStatement(sql);
        statement.setInt(1, id);
        ResultSet result = statement.executeQuery();
        return result.next();

    }

    public static ArrayList<HashMap<String, String>> requestForCampaignsListForATeam(int idTeam) throws SQLException {
        String sql = "SELECT idCampagne, nom, description, dateCreation, derniereModif, idUtilisateur " +
                "FROM campagne WHERE idEquipe = ?";
        PreparedStatement statement = Connection_BD.connect().prepareStatement(sql);
        statement.setInt(1, idTeam);
        ResultSet result = statement.executeQuery();

        ArrayList<HashMap<String, String>> listOfInfos = new ArrayList<>();
        HashMap<String, String> line;
        while(result.next()) {
            line = new HashMap<>();
            line.put("idCampagne", result.getString("idCampagne"));
            line.put("nom", result.getString("nom"));
            line.put("description", result.getString("description"));
            line.put("dateCreation", result.getString("dateCreation"));
            line.put("derniereModif", result.getString("derniereModif"));
            line.put("idUtilisateur", result.getString("idUtilisateur"));
            listOfInfos.add(line);
        }
        return listOfInfos;
    }
}