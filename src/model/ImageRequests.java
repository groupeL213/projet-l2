package model;

import controller.ImageFromDB;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Cette classe donne les requêtes par rapport aux images dans la base de donnée. Elle permet d'ajouter, de modifier
 * et de supprimer une image. De récupérer les id des images que peut voir l'utilisateur,
 * les id des images selon l'id d'un essai, les informations d'une image, savoir quelles images ont été analysées.
 * D'obtenir aussi les informations d'une image selon l'essai auquel elle appartient.
 */

public class ImageRequests {

    /* Requête SQL pour l'insertion d'une nouvelle ligne dans la table image et pour dupliquer une image */
    private static final String sql_InsertImage = "INSERT INTO image"
            + "(nom, image, dateAjout, fondNoir, idEssai, idUtilisateur)"
            + "VALUES (?, ?, ?, ?, ?, ?)";

    private static final String sql_InsertDuplicateImage = "INSERT INTO image "
            + "(nom, image, dateAjout, fondNoir, idEssai, idUtilisateur) "
            + "SELECT nom, image, ?, fondNoir, ? , ? "
            + "FROM image "
            + "WHERE idEssai = ? AND idImage = ?";
    /* ----- */

    /**
     * Méthode d'ajout d'une nouvelle image avec sa requête pour. Et une requête qui renvoit son id.
     * @param imageName le nom de la nouvelle image créée
     * @param fis C'est l'objet qui permet de d'ouvrir un fichier en fournissant le chemin de l'image.
     * Ça récupère le chemin avec le FileDialog, pour ouvrir l'image avec fileInputStream
     * @param addDate la date d'ajout de l'image
     * @param BlackBackground si le fond de l'image est en noir ou pas
     * @param idEssai l'id de l'essai auquel elle appartient
     * @return l'id de l'image créée
     * @throws SQLException Erreur d'accès à la base de données
     */
    public static int requestForNewImage(String imageName, FileInputStream fis, String addDate, int BlackBackground,
              int idEssai, int idUser) throws SQLException {

        Connection connection = Connection_BD.connect();

        PreparedStatement insertImage = connection.prepareStatement(sql_InsertImage);
        insertImage.setString(1, imageName);
        insertImage.setBinaryStream(2, fis);
        insertImage.setString(3, addDate);
        insertImage.setInt(4, BlackBackground);
        insertImage.setInt(5, idEssai);
        insertImage.setInt(6, idUser);
        insertImage.executeUpdate();

        PreparedStatement selectIPS = connection.prepareStatement(
                "Select idImage from image order by idImage desc limit 1");
        ResultSet idImageResult = selectIPS.executeQuery();
        if(idImageResult.next()) {
            return idImageResult.getInt("idImage");
        }

        return -1;

    }

    /**
     * Méthode pour dupliquer une image d'un essai et modifier l'idEssai de l'image dupliquée
     * pour qu'elle appartient à l'essai dupliqué.
     * @param idAssay l'id de l'essai de l'image à dupliquer
     * @param idImage l'id de l'image à duplique
     * @param idNewAssay l'id essai dupliqué qui sera donné à l'image dupliquée
     * @param newDateImport nouvelle date d'import de l'image
     * @return l'id de la nouvelle image dupliquée
     * @throws SQLException Erreur d'accès à la base de données
     */
    public static int requestToDuplicateAnImage(int idAssay, int idImage, int idNewAssay,
                                                 String newDateImport, int idUser) throws SQLException {

        Connection connection = Connection_BD.connect();

        PreparedStatement imageDuplicate = connection.prepareStatement(sql_InsertDuplicateImage);
        imageDuplicate.setString(1, newDateImport);
        imageDuplicate.setInt(2, idNewAssay);
        imageDuplicate.setInt(3, idUser);
        imageDuplicate.setInt(4, idAssay);
        imageDuplicate.setInt(5, idImage);

        imageDuplicate.executeUpdate();

        int result = 0;
        PreparedStatement idImageDuplicate = connection.prepareStatement
                ("SELECT MAX(idImage) AS 'Dernier id de la table' FROM image");

        ResultSet idImageResult = idImageDuplicate.executeQuery();
        while(idImageResult.next()) {
            result = idImageResult.getInt("Dernier id de la table");
        }

        return result;
    }

    /**
     * Méthode pour modifier le nombre de cellules dans une image et sa requête.
     * @param nbrCells le nouveau nombre de cellules à mettre pour l'image
     * @param idImage l'id de l'image où l'on souhaite modifier le nombre de cellules présents.
     * @throws SQLException Erreur d'accès à la base de données
     */
    public static void updateCellsNumber(int nbrCells, int idImage) throws SQLException {

        Connection connection = Connection_BD.connect();

        PreparedStatement updateCells = connection.prepareStatement(
                "UPDATE image SET nbrCellules = ? WHERE idImage = ?");
        updateCells.setInt(1, nbrCells);
        updateCells.setInt(2, idImage);
        updateCells.executeUpdate();

    }

    /**
     * Méthode de modification du nom d'une image et sa requête.
     * @param newImageName Le nouvel nom pour l'image
     * @param idImage l'id de l'image que l'on souhaite modifier le nom
     * @throws SQLException Erreur d'accès à la base de données
     */
    public static void requestForUpdatingNameImage(String newImageName, int idImage) throws SQLException {

        Connection connection = Connection_BD.connect();

        PreparedStatement updateNameImage = connection.prepareStatement
                ("UPDATE image SET nom = ? WHERE idImage = ?");
        updateNameImage.setString(1, newImageName);
        updateNameImage.setInt(2, idImage);
        updateNameImage.executeUpdate();


    }

    /**
     * Méthode de suppression d'une image et sa requête.
     * @param idImage l'id de l'image à supprimer
     * @throws SQLException Erreur d'accès à la base de données
     */
    public static void requestForDeletingImage(int idImage) throws SQLException {

        Connection connection = Connection_BD.connect();

        PreparedStatement deleteImage = connection.prepareStatement
                ("DELETE FROM image WHERE idImage = ?");
        deleteImage.setInt(1, idImage);
        deleteImage.executeUpdate();

    }

    /**
     * Méthode pour récupérer les id des images que peut voir l'utilisateur et sa requête.
     * @param idUser l'id de l'utilisateur (pour savoir quelles images il peut voir)
     * @return la liste des id des images auquelles l'utilisateur a accès
     * @throws SQLException Erreur d'accès à la base de données
     */
    public static ArrayList<Integer> requestForImagesSeenByUser(String idUser) throws SQLException {

        Connection connection = Connection_BD.connect();

        ArrayList<Integer> result = new ArrayList<>();

        PreparedStatement imagesSeenByUser = connection.prepareStatement
                ("SELECT I.idImage FROM image I "
                + "INNER JOIN essai E ON I.idEssai = E.idEssai "
                + "INNER JOIN campagne C ON E.idCampagne = C.idCampagne "
                + "INNER JOIN equipe EP ON C.idEquipe = EP.idEquipe "
                + "INNER JOIN appartient A ON EP.idEquipe = A.idEquipe "
                + "WHERE A.idUtilisateur = ?");
        imagesSeenByUser.setString(1, idUser);
        ResultSet imagesResult = imagesSeenByUser.executeQuery();
        while(imagesResult.next()) {
            result.add(imagesResult.getInt("idImage"));
        }

        return result;

    }

    /**
     * Méthode pour récupérer les id des images selon l'id d'un essai et sa requête.
     * @param idAssay l'id de l'essai pour récupérer les id de images de cet essai
     * @return la liste des id des images de l'essai en question
     * @throws SQLException Erreur d'accès à la base de données
     */
    public static ArrayList<Integer> requestForImagesInTheAssay(int idAssay) throws SQLException {
        Connection connection = Connection_BD.connect();

        ArrayList<Integer> result = new ArrayList<>();

        PreparedStatement imagesInAssay = connection.prepareStatement
                ("SELECT idImage FROM image WHERE idEssai = ?");
        imagesInAssay.setInt(1, idAssay);
        ResultSet imagesResult = imagesInAssay.executeQuery();
        while(imagesResult.next()) {
            result.add(imagesResult.getInt("idImage"));
        }

        return result;

    }

    /**
     * Méthode pour récupérer les informations de l'image et sa requête.
     * @param idImage l'id de l'image pour lequel on veut avoir ses informations
     * @return les informations de l'image en question
     * @throws SQLException Erreur d'accès à la base de données
     */
    public static ImageFromDB requestInfosImage(int idImage) throws SQLException {
        Connection connection = Connection_BD.connect();

        PreparedStatement infosImage = connection.prepareStatement("SELECT idImage, nom, image, "
                + " dateAjout, fondNoir, nbrCellules"
                + " FROM image "
                + " WHERE idImage = ?");
        infosImage.setInt(1, idImage);
        ResultSet infosImageResult = infosImage.executeQuery();
        int idI = -1, numberOfCells = 0;
        String name = null, addedDate = null;
        byte[] imgBytes = new byte[0];
        boolean blackBackground = false;
        while(infosImageResult.next()) {
            idI = infosImageResult.getInt("idImage");
            name = infosImageResult.getString("nom");
            imgBytes = infosImageResult.getBytes("image");
            addedDate = infosImageResult.getString("dateAjout");
            numberOfCells = infosImageResult.getInt("nbrCellules");
            blackBackground = infosImageResult.getBoolean("fondNoir");
        }
        InputStream is = new ByteArrayInputStream(imgBytes);

        return new ImageFromDB(idI, name, is, addedDate, numberOfCells, blackBackground);

    }

    /**
     * Méthode pour savoir quelles images ont été analysées et sa requête.
     * @return la liste des id des images non analysées
     * @throws SQLException Erreur d'accès à la base de données
     */
    public static ArrayList<Integer> requestToKnowIfImageHasBeenAnalysed() throws SQLException {
        Connection connection = Connection_BD.connect();

        ArrayList<Integer> result = new ArrayList<>();

        PreparedStatement nbCellsInImageFromAssay = connection.prepareStatement("SELECT I.idImage "
                + " FROM image I "
                + " LEFT OUTER JOIN amas A ON I.idImage = A.idImage "
                + " WHERE I.nbrCellules IS NULL");
        ResultSet imageResult = nbCellsInImageFromAssay.executeQuery();
        while(imageResult.next()) {
            result.add(imageResult.getInt("idImage"));
        }

        return result;

    }

    public static ArrayList<HashMap<String,String>> requestForExportInfos(int idAssay) throws SQLException {
        Connection connection = Connection_BD.connect();

        ArrayList<HashMap<String, String>> result = new ArrayList<>();

        PreparedStatement imagesInfosExport = connection.prepareStatement
                ("SELECT nom, nbrCellules "
                        + "FROM image "
                        + "WHERE idEssai = ?");
        imagesInfosExport.setInt(1, idAssay);

        ResultSet imgs = imagesInfosExport.executeQuery();
        while(imgs.next()) {
            HashMap<String, String> img = new HashMap<>();
            img.put("name", imgs.getString("nom"));
            img.put("number", imgs.getString("nbrCellules"));
            result.add(img);
        }

        return result;
    }

}
