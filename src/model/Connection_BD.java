package model;

import controller.Date;
import controller.Main;
import view.ConnectionView;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Connexion à la base de données en ligne ou locale.
 */
public class Connection_BD {


	/* Paramétrez ces informations pour vous connecter à votre BDD locale */

	private static final String LOCAL_login = "imagej_java";			// identifiant d'accès à la BDD
	private static final String LOCAL_password = "1m@g3J.JAVAg13";		// mot de passe d'accès à la BDD
	private static final String LOCAL_ip_adress = "192.168.102.153";	// adresse IP ou URL du SGBD
	private static final String LOCAL_port = "3306";					// port d'accès à la BDD
	private static final String LOCAL_db_name = "imagej_java";			// nom de la base de données
/*
	private static final String LOCAL_login = "aurelie";			// identifiant d'accès à la BDD
	private static final String LOCAL_password = "@urelie";		// mot de passe d'accès à la BDD
	private static final String LOCAL_ip_adress = "192.168.56.101";	// adresse IP ou URL du SGBD
	private static final String LOCAL_port = "3306";					// port d'accès à la BDD
	private static final String LOCAL_db_name = "imagej_bd";			// nom de la base de données
*/


	/**
	 * True si l'accès à la base de données se fait par internet. False sinon.
	 * Si vous souhaitez vous connecter à votre base de données locale,
	 * choisissez false et modifiez les valeurs précédentes.
	 */
	private static final boolean CONNECTIONINTERNET = true;


	/* Fin des informations à changer manuellement */


	private static final int timeToWaitDuringConnecting = (CONNECTIONINTERNET) ? 500 : 100;
	private static Connection connection;



	private static final String url = (CONNECTIONINTERNET) ?
			/* CONNEXION INTERNET */
			"jdbc:mysql://calibou.homedns.org:13306/imagej_java"
			+"?useSSL=false&useUnicode=true"
			+"&useJDBCCompliantTimezoneShift=true"
			+"&useLegacyDatetimeCode=false"
			+"&serverTimezone=UTC"
			+"&allowPublicKeyRetrieval=true"

			/* CONNEXION LOCALE */
			/* CONNECTIONINTERNET doit valoir false */
			: "jdbc:mysql://"+ LOCAL_ip_adress +":"+ LOCAL_port +"/" + LOCAL_db_name
			+"?useSSL=false&useUnicode=true"
			+"&useJDBCCompliantTimezoneShift=true"
			+"&useLegacyDatetimeCode=false"
			+"&serverTimezone=UTC"
			+"&allowPublicKeyRetrieval=true";
	private static final String login = (CONNECTIONINTERNET) ? /* NE PAS CHANGER */       "imagej_java"
															   /* si BD locale */       : LOCAL_login;
	private static final String password = (CONNECTIONINTERNET) ? /* NE PAS CHANGER */    "1m@g3J.JAVAg13"
																  /* si BD locale */    : LOCAL_password;
	/**
	 * Booléen qui indique si l'établissement de la connexion à la BDD est en cours.
	 */
	public static boolean isConnecting;
	public static boolean isConnected=false;
	public static boolean isAThreadWaitingForConnection=false;

	public static long delayToConnectToDB;

	/**
	 * Méthode pour obtenir la connexion à la base de données.
 	 * @return la connexion à la base de données
	 */
	public static Connection connect() throws SQLException {
		waitingForConnection();
		if (connection == null) {
			createConnexion();
		}
		if (!connection.isValid(0)) {
			System.out.println("Connexion non valide, reconnexion en cours...");
			createConnexion();
		}
		return connection;
	}

	/**
	 * Etablit la connexion à la base de données. Affiche la durée qu'a nécessité la connexion.
	 */
	public static void createConnexion() {
		System.out.println("Etablissement de la connexion à la base de données...");
		long startDate = System.currentTimeMillis();
		if (!isConnecting) {
			isConnecting=true;
			try {
				Class.forName("com.mysql.cj.jdbc.Driver");
				connection = DriverManager.getConnection(url, login, password);
			}
			catch(Exception e){
				e.printStackTrace();
			}
			long endDate = System.currentTimeMillis();
			long duration = endDate-startDate;
			delayToConnectToDB = duration;
			System.out.println("\nConnexion à la base de données établie (durée : "+duration+" ms)");
			isConnecting=false;
			isConnected=true;
			if (isAThreadWaitingForConnection) {
				synchronized (Main.objectToWait) {
					Main.objectToWait.notify();
				}
			}
		}

	}

	/**
	 * Méthode qui bloque le programme en attendant que la connexion à la BDD soit établie.
	 * Affiche également "Attention de la connexion à la base de données" toutes les 1 seconde,
	 * jusqu'à ce qu'elle soit établie.
	 */
	private static void waitingForConnection() {
		if (isConnecting) {
			System.out.print("Attente de la connexion à la base de données");
			while(isConnecting) {
				System.out.print(".");
				try {
					Thread.sleep(timeToWaitDuringConnecting);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static void verifyConnection() throws SQLException {
		if (!connection.isValid(0)) {
			System.out.println("Connexion non valide, reconnexion en cours...");
			createConnexion();
		}
	}
	
}
