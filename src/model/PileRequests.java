package model;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;

/**
 * Cette classe rassemble les requêtes concernant la table amas dans la base de données.
 * Elle permet d'ajouter ou de supprimer une ligne de la table amas.
 */

public class PileRequests {

    /* Requête sql pour l'insertion d'une nouvelle ligne dans la table amas */
    private static final String sql_InsertPile =
            "INSERT INTO amas (positionX, positionY, nbrCellules, idImage) VALUES (?, ?, ?, ?)";

    /**
     * Cette requête permet d'ajouter une nouvelle ligne dans la table amas,
     * autrement dit, la position d'une cellule.
     * @param positionX position de la cellule horizontalement.
     * @param positionY position de la cellule verticalement.
     * @param nbCells le nombre de cellule
     * @param idImage l'identifiant de l'image à laquelle elle appartient
     * @throws SQLException Erreur d'accès à la base de données
     */
    public static void requestForNewPile(double positionX, double positionY, int nbCells, int idImage)
    throws SQLException {

        Connection connection = Connection_BD.connect();

        PreparedStatement newPile = connection.prepareStatement(sql_InsertPile);

        newPile.setDouble(1, positionX);
        newPile.setDouble(2, positionY);
        newPile.setInt(3, nbCells);
        newPile.setInt(4, idImage);

        newPile.executeUpdate();
    }

    /**
     * Cette méthode permet l'ajout de plusieurs amas en même temps.
     * @param positionX la liste des positions en X
     * @param positionY la liste des position en Y
     * @param nbCells la liste des nombres de cellules par amas
     * @param idImage id de l'image associée
     */
    public static void requestForNewPileList(double[] positionX, double[] positionY, int[] nbCells, int idImage)
            throws SQLException {
        if (positionX.length > 0) {
            String sql = "INSERT INTO amas(positionX, positionY, nbrCellules, idImage) VALUES (?,?,?,?)";
            if (positionX.length > 1) {
                for (int i=1 ; i<positionX.length ; i++)
                    sql += ", (?,?,?,?)";
            }

            Connection connection = Connection_BD.connect();
            PreparedStatement statement = connection.prepareStatement(sql);
            for (int i=0 ; i<positionX.length ; i++) {
                statement.setDouble(4*i+1, positionX[i]);
                statement.setDouble(4*i+2, positionY[i]);
                statement.setInt(4*i+3, nbCells[i]);
                statement.setInt(4*i+4, idImage);
            }
            statement.executeUpdate();

        }
    }

    /**
     * Méthode pour supprimer une ligne de la table amas.
     * @param idPile identifiant de l'amas.
     * @throws SQLException Erreur d'accès à la base de données
     */
    public static void requestForDeletingPile(int idPile) throws SQLException {

        Connection connection = Connection_BD.connect();

        PreparedStatement deletePile = connection.prepareStatement
                ("DELETE FROM amas WHERE idAmas = ?");
        deletePile.setInt(1, idPile);

        deletePile.executeUpdate();

    }

    /**
     * Méthode pour supprimer les amas d'une image
     * @param idImage l'identifiant de l'image
     * @throws SQLException Erreur d'accès à la base de données
     */
    public static void requestForDeletingImagePile(int idImage) throws SQLException {

        Connection connection = Connection_BD.connect();

        PreparedStatement deletePile = connection.prepareStatement
                ("DELETE FROM amas WHERE idImage = ?");
        deletePile.setInt(1, idImage);

        deletePile.executeUpdate();

    }

}
