package model;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;

/**
 * Cette classe regroupe les requêtes sql pour la table appartient de la base de données.
 * Elle permet d'ajouter, de modifier ou de supprimer une ligne de la table appartient
 * et a en attribut la requête d'insertion d'une nouvelle ligne.
 */

public class BelongRequests {

    /* Requête sql pour l'insertion d'une nouvelle ligne dans la table appartient */
    private static final String sql_InsertBelong = "INSERT INTO appartient (idUtilisateur, idEquipe) VALUES (?, ?)";

    /**
     * Cette méthode permet d'ajouter une nouvelle ligne dans la table appartient et donc d'indiquer
     * à quelle équipe appartient un utilisateur.
     * @param idUser ID de l'utilisateur
     * @param idTeam identifiant de l'équipe à laquelle appartient l'utilisateur
     * @throws SQLException Erreur d'accès à la base de données
     */
    public static void requestToAddCorrelationBetweenUserAndTeam(int idUser, int idTeam) throws SQLException {

        Connection connection = Connection_BD.connect();

        PreparedStatement insertBelong = connection.prepareStatement(sql_InsertBelong);
        insertBelong.setInt(1, idUser);
        insertBelong.setInt(2, idTeam);
        insertBelong.executeUpdate();


    }

    /**
     * Cette méthode permet de supprimer une ligne de la table appartient,
     * pour que l'utilisateur ne fasse plus partie de l'équipe correspondante.
     * @param idUser ID de l'utilisateur
     * @param idTeam identifiant de l'équipe à laquelle appartient l'utilisateur avant la suppression
     * @throws SQLException Erreur d'accès à la base de données
     */
    public static void requestToDeleteCorrelation(int idUser, int idTeam) throws SQLException {

        Connection connection = Connection_BD.connect();

        PreparedStatement deleteBelong = connection.prepareStatement
                ("DELETE FROM appartient WHERE idUtilisateur = ? AND idEquipe = ? ");
        deleteBelong.setInt(1, idUser);
        deleteBelong.setInt(2, idTeam);
        deleteBelong.executeUpdate();

    }

    /**
     * Méthode qui supprime tous les liens entre un utilisateur précis et les équipes.
     * @param idUser id de l'utilisateur concerné
     * @throws SQLException Erreur d'accès à la base de données
     */
    public static void requestToDeleteAllCorrelationsForAUser(int idUser) throws SQLException {
        Connection connection = Connection_BD.connect();

        PreparedStatement deleteBelong = connection.prepareStatement
                ("DELETE FROM appartient WHERE idUtilisateur = ?");
        deleteBelong.setInt(1, idUser);
        deleteBelong.executeUpdate();
    }


}
