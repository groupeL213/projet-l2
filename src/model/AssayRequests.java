package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Donne les requêtes par rapports aux essais. Donc en ajouter, modifier, supprimer.
 * De récupérer l'id des essais appartenant à une campagne, les informations d'un essai,
 * les id des essais vus par l'utilisateur.
 * Avoir le nom de la campagne d'un essai voulu, le nombre total d'images dans un essai.
 * Et pouvoir dupliquer un essai.
 */

public class AssayRequests {

    /* Requête SQL pour l'insertion d'une nouvelle ligne dans la table essai et pour dupliquer un essai */
    private static final String sql_InsertAssay = "INSERT INTO essai"
            + "(nom, dateEssai, description, derniereModif, idCampagne, idAlgorithme, idUtilisateur)"
            + "VALUES (?, ?, ?, ?, ?, ?, ?)";

    private static final String sql_InsertDuplicateAssay = "INSERT INTO essai "
            + "(nom, dateEssai, description, derniereModif, idCampagne, idAlgorithme, idUtilisateur) "
            + "SELECT ?, ?, description, ?, idCampagne, ?, ? "
            + "FROM essai "
            + "WHERE idEssai = ?";
    /* ----- */

    /**
     * Méthode pour ajouter un nouvel essai avec sa requête pour l'ajouter et une requête pour renvoyer l'id créer.
     * @param assayName le nom de l'essai
     * @param creationDate la date de création de l'essai
     * @param assayDescription la description de l'essai
     * @param lastUpdateDate la dernière date de modification de l'essai
     * @param idCampaign l'id de la campagne auquel il appartient
     * @param idAlgorithm l'id de l'algorithme utilisé
     * @param idUser l'id de l'utilisateur qui créé l'essai
     * @return id du nouvel essai créé
     * @throws SQLException Erreur d'accès à la base de données
     */
    public static int requestForNewAssay
    (String assayName, String creationDate, String assayDescription, String lastUpdateDate, int idCampaign,
     int idAlgorithm, int idUser) throws SQLException {

        Connection connection = Connection_BD.connect();

        PreparedStatement newAssay = connection.prepareStatement(sql_InsertAssay);
        newAssay.setString(1, assayName);
        newAssay.setString(2, creationDate);
        newAssay.setString(3, assayDescription);
        newAssay.setString(4, lastUpdateDate);
        newAssay.setInt(5, idCampaign);
        newAssay.setInt(6, idAlgorithm);
        newAssay.setInt(7, idUser);
        newAssay.executeUpdate();

        int idNewAssay = 0;
        PreparedStatement idAssay = connection.prepareStatement("SELECT idEssai FROM essai "
                + "WHERE nom = ? AND dateEssai = ? AND description = ? AND derniereModif = ? "
                + "AND idCampagne = ? AND idAlgorithme = ? AND idUtilisateur = ?");
        idAssay.setString(1, assayName);
        idAssay.setString(2, creationDate);
        idAssay.setString(3, assayDescription);
        idAssay.setString(4, lastUpdateDate);
        idAssay.setInt(5, idCampaign);
        idAssay.setInt(6, idAlgorithm);
        idAssay.setInt(7, idUser);
        ResultSet idAssayResult = idAssay.executeQuery();
        while(idAssayResult.next()) {
            idNewAssay = idAssayResult.getInt("idEssai");
        }
        return idNewAssay;

    }

    /**
     * Méthode pour dupliquer un essai avec sa requête et la requête pour renvoyer son id.
     * @param idAssay l'id de l'essai à dupliquer
     * @param idAlgo l'id de l'algorithme choisi pour le nouvel essai
     * @param newName nouveau nom
     * @param newDate nouvelle date de création
     * @param idCreator id de l'utilisateur qui a dupliqué l'essai
     * @return l'id de l'essai qui vient d'être dupliqué
     * @throws SQLException Erreur d'accès à la base de données
     */
    public static int requestToDuplicateAnAssay(int idAssay, int idAlgo, String newName, String newDate, int idCreator) throws SQLException {

        Connection connection = Connection_BD.connect();

        PreparedStatement assayDuplicate = connection.prepareStatement(sql_InsertDuplicateAssay);
        assayDuplicate.setString(1, newName);
        assayDuplicate.setString(2, newDate);
        assayDuplicate.setString(3, newDate);
        assayDuplicate.setInt(4, idAlgo);
        assayDuplicate.setInt(5, idCreator);
        assayDuplicate.setInt(6, idAssay);
        assayDuplicate.executeUpdate();

        int result = 0;
        PreparedStatement idAssayDuplicate = connection.prepareStatement
                ("SELECT MAX(idEssai) AS 'Dernier id de la table' FROM essai");

        ResultSet idAssayResult = idAssayDuplicate.executeQuery();
        while(idAssayResult.next()) {
            result = idAssayResult.getInt("Dernier id de la table");
        }

        return result;

    }

    /**
     * Méthode pour la modification des informations d'un essai et sa requête.
     * @param idAssay l'id de l'essai à modifier
     * @param newAssayName le nouveau nom de l'essai
     * @param newAssayDescription sa nouvelle description
     * @param newAssayLastDate la date où l'essai a été modifié
     * @throws SQLException Erreur d'accès à la base de données
     */
    public static void requestForUpdatingNameAssay(int idAssay, String newAssayName,String newAssayDescription,
           String newAssayLastDate) throws SQLException {

        Connection connection = Connection_BD.connect();

        PreparedStatement updateNameAssay = connection.prepareStatement("UPDATE essai "
                + " SET nom = ?"
                + ", description = ?"
                + ", derniereModif = ?"
                + " WHERE idEssai = ?");
        updateNameAssay.setString(1, newAssayName);
        updateNameAssay.setString(2, newAssayDescription);
        updateNameAssay.setString(3, newAssayLastDate);
        updateNameAssay.setInt(4, idAssay);
        updateNameAssay.executeUpdate();


    }

    /**
     * Méthode pour la suppression d'un essai et sa requête.
     * @param idAssay l'id de l'essai à supprimer
     * @throws SQLException Erreur d'accès à la base de données
     */
    public static void requestForDeletingAssay(int idAssay) throws SQLException {

        Connection connection = Connection_BD.connect();

        PreparedStatement deleteAssay = connection.prepareStatement("DELETE FROM essai "
                + " WHERE idEssai = ?");
        deleteAssay.setInt(1, idAssay);
        deleteAssay.executeUpdate();

    }

    /**
     * Méthode pour récupérer l'id des essais appartenant à une campagne et sa requête.
     * @param idCampaign l'id de la campagne pour avoir l'id des essais provenant de cette campagne
     * @return la liste des idEssai d'une campagne
     * @throws SQLException Erreur d'accès à la base de données
     */
    public static ArrayList<Integer> requestForIdAssaysInCampaign(int idCampaign)
            throws SQLException {
        ArrayList<Integer> idOfAllAssaysInCampaign = new ArrayList<>();

        Connection connection = Connection_BD.connect();

        PreparedStatement idAssays = connection.prepareStatement("SELECT idEssai FROM essai E "
                +"WHERE idCampagne = ?");
        idAssays.setInt(1, idCampaign);
        ResultSet idAssaysResults = idAssays.executeQuery();
        while(idAssaysResults.next()) {
            idOfAllAssaysInCampaign.add(idAssaysResults.getInt("idEssai"));
        }

        return idOfAllAssaysInCampaign;

    }

    /**
     * Méthode pour récupérer les informations d'un essai (son id, son nom, sa date de création, sa description,
     * sa dernière date de modification, l'id de la campagne auquel il appartient, l'id de l'algorithme utilisé
     * et l'id de l'utilisateur qui a créé l'essai) et sa requête.
     * @param idAssay l'id de l'essai pour savoir ses informations
     * @return Les informations de l'essai
     * @throws SQLException Erreur d'accès à la base de données
     */
    public static HashMap<String, String> requestForInfosOfAssay(int idAssay) throws SQLException {
        HashMap<String, String> result = new HashMap<>();

        Connection connection = Connection_BD.connect();

        PreparedStatement assayInfo = connection.prepareStatement("SELECT E.idEssai, E.nom, " +
                "E.dateEssai, E.description, E.derniereModif,  E.idCampagne, E.idAlgorithme, E.idUtilisateur"
                +" FROM essai E WHERE E.idEssai = ?");
        assayInfo.setInt(1, idAssay);
        ResultSet assayResult = assayInfo.executeQuery();
        while(assayResult.next()) {
            result.put("idAssay", assayResult.getString("idEssai"));
            result.put("nameAssay", assayResult.getString("nom"));
            result.put("dateAssay", assayResult.getString("dateEssai"));
            result.put("description", assayResult.getString("description"));
            result.put("lastUpdate", assayResult.getString("derniereModif"));
            result.put("idCampaign", assayResult.getString("idCampagne"));
            result.put("idAlgorithm", assayResult.getString("idAlgorithme"));
            result.put("idCreator", assayResult.getString("idUtilisateur"));
        }

        return result;

    }

    /**
     * Méthode pour avoir le nom de la campagne d'un essai voulu et sa requête.
     * @param idAssay l'id de l'essai où l'on veut retrouver le nom de la campagne auquel il appartient
     * @return le nom de la campagne auquel appartient l'essai
     * @throws SQLException Erreur d'accès à la base de données
     */
    public static String requestAssayFromASpecificCampaign(int idAssay) throws SQLException {
        String result = null;

        Connection connection = Connection_BD.connect();

        PreparedStatement nameCampaignOfTheAssay = connection.prepareStatement("SELECT C.nom FROM " +
                "campagne C INNER JOIN essai E ON E.idCampagne"
                + " = C.idCampagne WHERE E.idEssai = ?");
        nameCampaignOfTheAssay.setInt(1, idAssay);
        ResultSet nameCampaignOfAssayResult = nameCampaignOfTheAssay.executeQuery();
        while(nameCampaignOfAssayResult.next()) {
            result = nameCampaignOfAssayResult.getString(idAssay);
        }

        return result;

    }

    /**
     * Méthode indiquant le nombre total d'images dans un essai et sa requête.
     * @param idAssay l'id de l'essai pour savoir son total d'images
     * @return le nombre total d'images dans l'essai
     * @throws SQLException Erreur d'accès à la base de données
     */
    public static int requestNbImagesInAssay(int idAssay) throws SQLException {
        int result = 0;

        Connection connection = Connection_BD.connect();

        PreparedStatement nbImagesOfTheAssay = connection.prepareStatement("SELECT "
                + " COUNT(I.idImage) AS 'Nombre d''images' FROM image I "
                + "INNER JOIN  essai E ON E.idEssai = I.idEssai "
                + " WHERE E.idEssai = ?");
        nbImagesOfTheAssay.setInt(1, idAssay);
        ResultSet nbImagesInAssayResult = nbImagesOfTheAssay.executeQuery();
        while(nbImagesInAssayResult.next()) {
            result = nbImagesInAssayResult.getInt("Nombre d'images");
        }

        return result;

    }

    /**
     * Méthode pour récupérer les id des essais vus par l'utilisateur et sa requête.
     * @param idUser l'id de l'utilisateur (pour savoir quels essais il peut voir)
     * @return la liste des id essai que peuvent voir l'utilisateur en question
     * @throws SQLException Erreur d'accès à la base de données
     */
    public static ArrayList<HashMap<String, String>> requestForAssaysSeenByUser (int idUser) throws SQLException {
        ArrayList<HashMap<String, String>> result = new ArrayList<>();

        Connection connection = Connection_BD.connect();

        PreparedStatement assaysSeenByUser = connection.prepareStatement("SELECT DISTINCT E.idEssai," +
                "E.nom, E. dateEssai, E.description, E.derniereModif, E.idCampagne, E.idAlgorithme, E.idUtilisateur "
                + "FROM essai E "
                + "INNER JOIN campagne C ON C.idCampagne = E.idCampagne "
                + "INNER JOIN appartient A ON A.idEquipe = C.idEquipe "
                + "WHERE A.idUtilisateur = ?");
        assaysSeenByUser.setInt(1, idUser);
        ResultSet assaysSeenByUserResults = assaysSeenByUser.executeQuery();
        while(assaysSeenByUserResults.next()){
            HashMap<String, String> assay = new HashMap<>();
            assay.put("idEssai", assaysSeenByUserResults.getString("E.idEssai"));
            assay.put("nom", assaysSeenByUserResults.getString("E.nom"));
            assay.put("dateCreation", assaysSeenByUserResults.getString("E.dateEssai"));
            assay.put("description", assaysSeenByUserResults.getString("E.description"));
            assay.put("derniereModif", assaysSeenByUserResults.getString("E.derniereModif"));
            assay.put("idCampagne", assaysSeenByUserResults.getString("E.idCampagne"));
            assay.put("idAlgorithme", assaysSeenByUserResults.getString("E.idAlgorithme"));
            assay.put("idUtilisateur", assaysSeenByUserResults.getString("E.idUtilisateur"));

            result.add(assay);
        }

        return result;

    }

}
