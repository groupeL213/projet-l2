package model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Cette classe contient les requêtes concernant la table campagne de la base de données.
 * Elle permet de créer une nouvelle campagne, d'en modifier ou d'en supprimer une,
 * de récupérer la liste des identifiants des campagnes que peut voir l'utilisateur,
 * d'obtenir les informations d'une campagne en particulier (telles que son nom, sa description, etc.),
 * de connaître le nombre d'essais dans une campagne, ainsi que le nombre d'images dans une campagne.
 */

public class CampaignRequests {

    /* Requête sql pour ajouter une nouvelle ligne dans la table campagne */
    private static final String sql_InsertInto = "INSERT INTO campagne "
            +"(nom, description, dateCreation, derniereModif, idEquipe, idUtilisateur) VALUES (?, ?, ?, ?, ?, ?)";

    /**
     * Cette méthode permet d'ajouter une nouvelle ligne dans la table campagne,
     * tout en renvoyant l'identifiant de celle-ci à la fin.
     * @param campaignName nom de la campagne
     * @param campaignDescription description de la campagne
     * @param creationDate date de création de la campagne
     * @param lastUpdateDate date de dernière modification de la campagne
     * @param idTeam identifiant de l'équipe à laquelle appartient la campagne
     * @param idUser identifiant de l'utilisateur qui a créé la campagne
     * @return l'identifiant de la campagne tout juste créée.
     * @throws SQLException Erreur d'accès à la base de données
     */
    public static int requestForNewCampaign
    (String campaignName, String campaignDescription, String creationDate,
     String lastUpdateDate, int idTeam, int idUser) throws SQLException {

        Connection connection = Connection_BD.connect();

        PreparedStatement newCampaign = connection.prepareStatement(sql_InsertInto);

        newCampaign.setString(1, campaignName);
        newCampaign.setString(2, campaignDescription);
        newCampaign.setString(3, creationDate);
        newCampaign.setString(4, lastUpdateDate);
        newCampaign.setInt(5, idTeam);
        newCampaign.setInt(6, idUser);

        newCampaign.executeUpdate();

        int idNewCampaign = 0;
        PreparedStatement idCampaign = connection.prepareStatement
                ("SELECT idCampagne FROM campagne "
                + "WHERE nom = ? AND description = ? "
                + "AND dateCreation = ? AND derniereModif = ? "
                + "AND idEquipe = ? AND idUtilisateur = ?");

        idCampaign.setString(1, campaignName);
        idCampaign.setString(2, campaignDescription);
        idCampaign.setString(3, creationDate);
        idCampaign.setString(4, lastUpdateDate);
        idCampaign.setInt(5, idTeam);
        idCampaign.setInt(6, idUser);

        ResultSet idCampaignResult = idCampaign.executeQuery();
        while(idCampaignResult.next()) {
            idNewCampaign = idCampaignResult.getInt("idCampagne");
        }


        return idNewCampaign;

    }

    /**
     * Méthode permettant de modifier les informations (le nom, la description, la date de dernière modification),
     * d'une campagne.
     * @param idCampaign identifiant de la campagne
     * @param newName nouveau nom de la campagne
     * @param newDescription nouvelle description de la campagne
     * @param newLastUpdate nouvelle date de dernière modification de la campagne
     * @throws SQLException Erreur d'accès à la base de données
     */
    public static void requestForUpdatingCampaign
    (int idCampaign, String newName, String newDescription, String newLastUpdate) throws SQLException {

        Connection connection = Connection_BD.connect();

        PreparedStatement updateCampaign = connection.prepareStatement
                ("UPDATE campagne SET nom = ?, description = ?, derniereModif = ? WHERE idCampagne = ?");

        updateCampaign.setString(1, newName);
        updateCampaign.setString(2, newDescription);
        updateCampaign.setString(3, newLastUpdate);
        updateCampaign.setInt(4, idCampaign);

        updateCampaign.executeUpdate();


    }

    /**
     * Cette méthode permet de supprimer une campagne de la base de données.
     * @param idCampaign identifiant de la campagne
     * @throws SQLException Erreur d'accès à la base de données
     */
    public static void requestForDeletingCampaign(int idCampaign) throws SQLException {

        Connection connection = Connection_BD.connect();

        PreparedStatement deleteCampaign = connection.prepareStatement
                ("DELETE FROM campagne WHERE idCampagne = ?");
        deleteCampaign.setInt(1, idCampaign);
        deleteCampaign.executeUpdate();

    }

    /**
     * Méthode pour récupérer la liste des identifiants des campagnes que peut voir un utilisateur.
     * @param idUser ID de l'utilisateur
     * @return la liste des identifiants des campagnes visibles pour l'utilisateur.
     * @throws SQLException Erreur d'accès à la base de données
     */
    public static ArrayList<HashMap<String, String>> requestForCampaignsSeenByUser(int idUser) throws SQLException {

        Connection connection = Connection_BD.connect();

        ArrayList<HashMap<String, String>> result = new ArrayList<>();

        PreparedStatement campaignsSeenByUser = connection.prepareStatement
                ("SELECT C.idCampagne, C.nom, C.description, C.dateCreation, " +
                        "C.derniereModif, C.idEquipe, C.idUtilisateur "
                + "FROM campagne C "
                + "INNER JOIN equipe E ON C.idEquipe = E.idEquipe "
                + "INNER JOIN appartient A ON E.idEquipe = A.idEquipe "
                + "INNER JOIN utilisateur U ON A.idUtilisateur = U.idUtilisateur "
                + "WHERE U.idUtilisateur = ?");
        campaignsSeenByUser.setInt(1, idUser);

        ResultSet campaigns = campaignsSeenByUser.executeQuery();
        while(campaigns.next()) {
            HashMap<String, String> campaign = new HashMap<>();
            campaign.put("idCampagne", String.valueOf(campaigns.getInt("C.idCampagne")));
            campaign.put("nom", campaigns.getString("C.nom"));
            campaign.put("description", campaigns.getString("C.description"));
            campaign.put("dateCreation", campaigns.getString("C.dateCreation"));
            campaign.put("derniereModif", campaigns.getString("C.derniereModif"));
            campaign.put("idEquipe", String.valueOf(campaigns.getInt("C.idEquipe")));
            campaign.put("idUtilisateur", String.valueOf(campaigns.getInt("C.idUtilisateur")));
            result.add(campaign);
        }

        return result;

    }

    /**
     * Cette méthode permet de récupérer toutes les informations spécifiques à une campagne (telles que l'identifiant,
     * le nom, la description, etc.)
     * @param idCampaign identifiant de la campagne
     * @return toutes les informations concernant la campagne
     * @throws SQLException Erreur d'accès à la base de données
     */
    public static HashMap<String, String> requestForInfosOfCampaign(int idCampaign) throws SQLException {

        Connection connection = Connection_BD.connect();

        HashMap<String, String> result = new HashMap<>();

        PreparedStatement campaignsSeenByUser = connection.prepareStatement
                ("SELECT idCampagne, C.nom, C.description, C.dateCreation, "
                + "C.derniereModif, C.idEquipe, C.idUtilisateur FROM campagne C "
                + "WHERE C.idCampagne = ?");
        campaignsSeenByUser.setInt(1, idCampaign);

        ResultSet campaigns = campaignsSeenByUser.executeQuery();
        while(campaigns.next()) {
            result.put("ID of the campaign", campaigns.getString("idCampagne"));
            result.put("Name", campaigns.getString("nom"));
            result.put("Description", campaigns.getString("description"));
            result.put("Date of creation", campaigns.getString("dateCreation"));
            result.put("Date of the last update", campaigns.getString("derniereModif"));
            result.put("ID of the team", campaigns.getString("idEquipe"));
            result.put("ID of the creator", campaigns.getString("idUtilisateur"));
        }

        return result;

    }

    /**
     * Cette méthode permet de compter combien d'essais il y a dans une campagne.
     * @param idCampaign identifiant de la campagne
     * @return le nombre total d'essais dans cette campagne
     * @throws SQLException Erreur d'accès à la base de données
     */
    public static int requestNbAssayInCampaign(int idCampaign) throws SQLException {

        Connection connection = Connection_BD.connect();
        int result = 0;

        PreparedStatement nbAssayOfTheCampaign = connection.prepareStatement
                ("SELECT COUNT(idEssai) AS 'Nombre d''essais' FROM essai E WHERE E.idCampagne = ?");
        nbAssayOfTheCampaign.setInt(1, idCampaign);

        ResultSet nbAssays = nbAssayOfTheCampaign.executeQuery();
        while(nbAssays.next()) {
            result = nbAssays.getInt("Nombre d'essais");
        }

        return result;

    }

    /**
     * Méthode pour compter le nombre d'images dans une campagne
     * @param idCampaign identifiant de la campagne
     * @return le nombre total d'images dans cette campagne
     * @throws SQLException Erreur d'accès à la base de données
     */
    public static int requestNbImageInCampaign(int idCampaign) throws SQLException {

        Connection connection = Connection_BD.connect();
        int result = 0;

        PreparedStatement nbImagesOfTheCampaign = connection.prepareStatement
                ("SELECT COUNT(I.idImage) AS 'Nombre d''images' FROM image I "
                + "INNER JOIN essai E ON E.idEssai = I.idEssai "
                + "INNER JOIN campagne C ON E.idCampagne = C.idCampagne "
                + "WHERE C.idCampagne = ?");
        nbImagesOfTheCampaign.setInt(1, idCampaign);

        ResultSet nbImages = nbImagesOfTheCampaign.executeQuery();
        while(nbImages.next()) {
            result = nbImages.getInt("Nombre d'images");
        }
        return result;

    }
}