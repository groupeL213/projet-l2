package model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Cette classe regroupe les requêtes pour obtenir les statistiques ;
 * celles d'une campagne, celles d'un essai, celles d'une image et celles de la page d'accueil.
 */

public class Statistics {

	/**
	 * Méthode qui calcule les statistiques d'une campagne,
	 * dont le nombre de cellules, le nombre d'images et le nombre moyen de cellules qu'elle contient.
	 * @param idCampaign identifiant de la campagne
	 * @return le nombre de cellules, le nombre d'images et le nombre moyen de cellules de la campagne.
	 * @throws SQLException Erreur d'accès à la base de données
	 */
	public static HashMap<String, Float> requestForCampaignStatistics(int idCampaign) throws SQLException {

		HashMap<String, Float> result = new HashMap<>();

		PreparedStatement campaignStatistics = Connection_BD.connect().prepareStatement
				("SELECT SUM(I.nbrCellules) AS 'Nombre total de cellules dans la campagne', "
						+ " COUNT(DISTINCT I.idImage) AS 'Nombre total d''images dans la campagne', "
						+ " AVG(I.nbrCellules) AS 'Nombre moyen de cellules par image dans la campagne' FROM image I "
						+ " LEFT OUTER JOIN essai E ON I.idEssai=E.idEssai "
						+ " LEFT OUTER JOIN campagne C ON E.idCampagne=C.idCampagne "
						+ " WHERE C.idCampagne = ? ");
		campaignStatistics.setInt(1, idCampaign);

		ResultSet campaignResults = campaignStatistics.executeQuery();

		while(campaignResults.next()){
			result.put("Nombre total de cellules dans la campagne",campaignResults.getFloat
					("Nombre total de cellules dans la campagne"));
			result.put("Nombre total d'images dans la campagne", campaignResults.getFloat
					("Nombre total d'images dans la campagne"));
			result.put("Nombre moyen de cellules par image dans la campagne", campaignResults.getFloat
					("Nombre moyen de cellules par image dans la campagne"));
		}

		return result;

	}

	/**
	 * Cette méthode calcule les statistiques d'un essai,
	 * dont le nombre de cellules, le nombre d'images et le nombre moyen de cellules qu'il contient.
	 * @param idAssay identifiant de l'essai
	 * @return le nombre de cellules, le nombre d'images et le nombre moyen de cellules de l'essai.
	 * @throws SQLException Erreur d'accès à la base de données
	 */
	public static HashMap<String, Float> requestForAssayStatistics(int idAssay) throws SQLException {

		HashMap<String, Float> result = new HashMap<>();

		PreparedStatement assayStatistics = Connection_BD.connect().prepareStatement("SELECT SUM(I.nbrCellules) "
				+"AS 'Nombre total de cellules dans l''essai', "
				+"COUNT(DISTINCT I.idImage) AS 'Nombre total d''images dans l''essai', "
				+"AVG(I.nbrCellules) AS 'Nombre moyen de cellules par image dans l''essai', E.nom "
				+"FROM image I LEFT OUTER JOIN essai E ON I.idEssai = E.idEssai "
				+"WHERE I.idEssai = ? ");
		assayStatistics.setInt(1, idAssay);

		ResultSet assayResults = assayStatistics.executeQuery();

		while(assayResults.next()){
			result.put("Nombre total de cellules dans l'essai", assayResults.getFloat
					("Nombre total de cellules dans l'essai"));
			result.put("Nombre total d'images dans l'essai", assayResults.getFloat
					("Nombre total d'images dans l'essai"));
			result.put("Nombre moyen de cellules par image dans l'essai", assayResults.getFloat
					("Nombre moyen de cellules par image dans l'essai"));
		}

		return result;

	}

	/**
	 * Cette méthode calcule le nombre de cellules contenu dans une image.
	 * @param idImage identifiant de l'image
	 * @return le nombre de cellules dans une image.
	 * @throws SQLException Erreur d'accès à la base de données
	 */
	public static HashMap<String, Integer> requestForImageStatistics(int idImage) throws SQLException {

		HashMap<String, Integer> result = new HashMap<>();

		PreparedStatement nbCellsInImage = Connection_BD.connect().prepareStatement("SELECT nbrCellules "
				+"AS 'Nombre de cellules dans l''image', nom FROM image "
				+"WHERE idImage = ?");
		nbCellsInImage.setInt(1, idImage);

		ResultSet imageResults = nbCellsInImage.executeQuery();

		while(imageResults.next()){
			result.put("Nombre de cellules dans l'image", imageResults.getInt
					("Nombre de cellules dans l'image"));
		}

		return result;

	}

	/**
	 * Cette méthode calcule les statistiques nécessaires à la page d'accueil de l'application,
	 * c'est-à-dire le nombre d'images visibles pour l'utilisateur, le nombre de cellules contenues dans celles-ci,
	 * ainsi que la moyenne de ce nombre de cellules.
	 * @param loginUser ID de connexion de l'utilisateur
	 * @return le nombre d'images que peut voir l'utilisateur, le nombre de cellules dans ces images
	 * et le nombre moyen de cellules dans ces images
	 * @throws SQLException Erreur d'accès à la base de données
	 */
	public static HashMap<String, Float> requestHomepageStatistics(String loginUser) throws SQLException {

		HashMap<String, Float> result = new HashMap<>();

		PreparedStatement homepageStat = Connection_BD.connect().prepareStatement
				("SELECT COUNT(DISTINCT I.idImage) AS 'Images vues par l''utilisateur', "
						+ "SUM(I.nbrCellules) AS 'Cellules dans les images vues par l''utilisateur', "
						+ "AVG(I.nbrCellules) AS 'Nombre moyen de celulles dans les images vues par l''utilisateur' "
						+ "FROM image I "
						+ "RIGHT OUTER JOIN essai E ON I.idEssai=E.idEssai "
						+ "RIGHT OUTER JOIN campagne C ON E.idCampagne = C.idCampagne "
						+ "RIGHT OUTER JOIN equipe EP ON C.idEquipe=EP.idEquipe "
						+ "RIGHT OUTER JOIN appartient PT ON EP.idEquipe = PT.idEquipe "
						+ "RIGHT OUTER JOIN utilisateur U ON PT.idUtilisateur = U.idUtilisateur "
						+ "WHERE U.login = ?");
		homepageStat.setString(1, loginUser);

		ResultSet homepageStatResults = homepageStat.executeQuery();

		while(homepageStatResults.next()) {
			result.put("Images vues par l'utilisateur", homepageStatResults.getFloat
					("Images vues par l'utilisateur"));
			result.put("Cellules dans les images vues par l'utilisateur",
					homepageStatResults.getFloat("Cellules dans les images vues par l'utilisateur"));
			result.put("Nombre moyen de celulles dans les images vues par l'utilisateur", homepageStatResults.getFloat
					("Nombre moyen de celulles dans les images vues par l'utilisateur"));
		}

		return result;

	}

	/**
	 * Donne la liste des id des utilisateurs ayant créé plus de x essais associé au nombre d'essais qu'ils ont créé.
	 * @param numberOfAssays nombre d'essais
	 * @return la liste des id utilisateur avec le nombre d'essais créés triée par ordre décroissant du nombre d'essais
	 * @throws SQLException Erreur d'accès à la base de données.
	 */
	public static ArrayList<HashMap<String, String>> requestForUsersHavingCreatedMoreThanXAssays(int numberOfAssays)
			throws SQLException {
		PreparedStatement statement = Connection_BD.connect().prepareStatement(
				"SELECT U.idUtilisateur, COUNT(E.idEssai) AS nbEssais " +
						"FROM essai E " +
						"INNER JOIN utilisateur U ON U.idUtilisateur = E.idUtilisateur " +
						"GROUP BY E.idUtilisateur " +
						"HAVING nbEssais > ? " +
						"ORDER BY nbEssais DESC "
		);
		statement.setInt(1, numberOfAssays);

		ResultSet result = statement.executeQuery();

		ArrayList<HashMap<String, String>> listOfUsers = new ArrayList<>();
		while(result.next()) {
			HashMap<String, String> line = new HashMap<>();
			line.put("idUtilisateur", result.getString("U.idUtilisateur"));
			line.put("nbEssais", Integer.toString(result.getInt("nbEssais")));
			listOfUsers.add(line);
		}

		return listOfUsers;

	}

}