package model;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Requêtes pour la table algorithme de la base de données.
 */

public class AlgorithmRequests {

    /* Requête SQL pour l'insertion d'une nouvelle ligne dans la table algorithme */
    private static final String sql_InsertAlgo = "INSERT INTO algorithme"
            + "(nom, description, methodeThreshold, Tmin, Tmax, Cmin, Cmax)"
            + "VALUES (?, ?, ?, ?, ?, ?, ?)";

    /**
     * Méthode pour pouvoir rajouter un nouvel algorithme
     * @param algoName le nom de l'algorithme
     * @param methodeThreshold le nom de méthode Threshold
     * @param Tmin Aire minimal d'une cellule en pixel
     * @param Tmax Aire maximale d'une cellule en pixel
     * @param Cmin Circularité minimale d'une cellule
     * @param Cmax Circularité maximale d'une cellule
     * @throws SQLException Erreur d'accès à la base de données
     */
    public static void requestForNewAlgo
            (String algoName, String methodeThreshold, double Tmin,
             double Tmax, double Cmin, double Cmax) throws SQLException {

        Connection connection = Connection_BD.connect();

        PreparedStatement newAlgo = connection.prepareStatement(sql_InsertAlgo);
        newAlgo.setString(1, algoName);
        newAlgo.setString(2, "Vide");
        newAlgo.setString(3, methodeThreshold);
        newAlgo.setDouble(4, Tmin);
        newAlgo.setDouble(5, Tmax);
        newAlgo.setDouble(6, Cmin);
        newAlgo.setDouble(7, Cmax);
        newAlgo.executeUpdate();

    }

    /**
     * Méthode pour récupérer les informations d'un algorithme.
     * @param idAlgo l'id de l'algorithme dont on veut les informations
     * @return les informations de l'algorithme
     * @throws SQLException Erreur d'accès à la base de données
     */
    public static HashMap<String, String> requestForAlgorithm(int idAlgo) throws SQLException {

        Connection connection = Connection_BD.connect();

        HashMap<String, String> result = new HashMap<String, String>();
        PreparedStatement algoInfo = connection.prepareStatement(
                "SELECT * FROM algorithme WHERE idAlgorithme = ?");
        algoInfo.setInt(1, idAlgo);
        ResultSet algoResult = algoInfo.executeQuery();
        while(algoResult.next()) {
            result.put("idAlgorithm", algoResult.getString("idAlgorithme"));
            result.put("name", algoResult.getString("nom"));
            result.put("description", algoResult.getString("description"));
            result.put("methodThreshold", algoResult.getString("methodeThreshold"));
            result.put("Tmin", algoResult.getString("Tmin"));
            result.put("Tmax", algoResult.getString("Tmax"));
            result.put("Cmin", algoResult.getString("Cmin"));
            result.put("Cmax", algoResult.getString("Cmax"));
        }

        return result;

    }

    /**
     * Méthode pour avoir la liste des identifiants des algorithmes
     * @return la liste des identifiants des algorithmes
     * @throws SQLException Erreur d'accès à la base de données
     */
    public static ArrayList<Integer> requestForListIdAlgo() throws SQLException {

        Connection connection = Connection_BD.connect();

        ArrayList<Integer> result = new ArrayList<>();

        PreparedStatement idsAlgo = connection.prepareStatement
                ("SELECT idAlgorithme FROM algorithme");

        ResultSet idsAlgorithmResult = idsAlgo.executeQuery();
        while(idsAlgorithmResult.next()) {
            result.add(idsAlgorithmResult.getInt("idAlgorithme"));
        }

        return result;

    }

    public static ArrayList<HashMap<String, String>> requestForListAlgoInfos() throws SQLException {
        Connection connection = Connection_BD.connect();

        ArrayList<HashMap<String, String>> result = new ArrayList<>();

        PreparedStatement idsAlgo = connection.prepareStatement
                ("SELECT idAlgorithme, nom, description, methodeThreshold, Tmin, Tmax, Cmin, Cmax FROM algorithme");

        ResultSet idsAlgorithmResult = idsAlgo.executeQuery();

        HashMap<String, String> line;
        while(idsAlgorithmResult.next()) {
            line = new HashMap<>();
            line.put("idAlgorithme", idsAlgorithmResult.getString("idAlgorithme"));
            line.put("nom", idsAlgorithmResult.getString("nom"));
            line.put("description", idsAlgorithmResult.getString("description"));
            line.put("methodeThreshold", idsAlgorithmResult.getString("methodeThreshold"));
            line.put("Tmin", idsAlgorithmResult.getString("Tmin"));
            line.put("Tmax", idsAlgorithmResult.getString("Tmax"));
            line.put("Cmin", idsAlgorithmResult.getString("Cmin"));
            line.put("Cmax", idsAlgorithmResult.getString("Cmax"));

            result.add(line);
        }

        return result;
    }

}
