package model;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Cette classe donne les requêtes par rapport aux utilisateurs. Elle permet d'ajouter, de modifier
 * et de supprimer un utilisateur. Et d'ajouter un admin.
 * Cette classe permet également d'obtenir la liste des utilisateurs et aussi selon leur équipe,
 * la liste des équipes auxquelles appartient un utilisateur
 * De savoir si un utilisateur est administrateur ou pas.
 * De vérifier le couple identifiant/mot de passe permettant la connexion.
 * De récupérer les informations d'un utilisateur.
 * De compter le nombre de fois qu'un utilisateur est présent.
 */

public class UserRequests {

    /* Statut des utilisateurs */
    private static final int statutUser = 0;
    private static final int statutAdmin = 1;
    /* ----- */

    /* Requête SQL pour l'insertion d'un nouvel utilisateur et d'un nouvel admin */
    private static final String sql_InsertUser = "INSERT INTO utilisateur"
            + "(login, nom, prenom, motDePasse, admin)"
            + "VALUES (?, ?, ?, ?, ?)";
    private static final String sql_InsertAdmin = "INSERT INTO utilisateur"
            + "(login, nom, prenom, motDePasse, admin)"
            + "VALUES (?, ?, ?, ?, ?)";
    /* ----- */

    /**
     * Méthode d'ajout d'un nouveau utilisateur avec sa requête. Et la requête pour avoir son id.
     * @param userlogin l'id de l'utilisateur (qui se forme avec le nom + prénom, donc : nomprenom)
     * @param userSurname le nom de l'utilisateur
     * @param userFirstName le prénom de l'utilisateur
     * @param userPassword le mot de passe de l'utilisateur
     * @return l'id du nouvel utilisateur
     * @throws SQLException Erreur d'accès à la base de données
     */
    public static int requestForNewUser
    (String userlogin, String userSurname, String userFirstName, String userPassword) throws SQLException {

        Connection connection = Connection_BD.connect();

        PreparedStatement newUser = connection.prepareStatement(sql_InsertUser);
        newUser.setString(1, userlogin);
        newUser.setString(2, userSurname);
        newUser.setString(3, userFirstName);
        newUser.setString(4, userPassword);
        newUser.setInt(5, statutUser);
        newUser.executeUpdate();

        int idNewUser = 0;
        PreparedStatement idUser = connection.prepareStatement("SELECT idUtilisateur FROM utilisateur "
                + "WHERE login = ? AND nom = ? AND prenom = ? AND motDePasse = ? AND admin = ? ");
        idUser.setString(1, userlogin);
        idUser.setString(2, userSurname);
        idUser.setString(3, userFirstName);
        idUser.setString(4, userPassword);
        idUser.setInt(5, statutUser);
        ResultSet idUserResult = idUser.executeQuery();
        while(idUserResult.next()) {
            idNewUser = idUserResult.getInt("idUtilisateur");
        }
        return idNewUser;

    }

    /**
     * Méthode d'ajout d'un nouveau administrateur avec sa requête. Et la requête pour avoir son id.
     * @param userlogin l'id de l'utilisateur(admin) qui se forme avec le nom + prénom (donc : nomprenom)
     * @param adminSurname le nom de l'administrateur
     * @param adminFirstName le prénom de l'administrateur
     * @param adminPassword le mot de passe de l'administrateur
     * @return l'id de l'admin créé
     * @throws SQLException Erreur d'accès à la base de données
     */
    public static int requestForNewAdmin
    (String userlogin, String adminSurname, String adminFirstName, String adminPassword) throws SQLException {

        Connection connection = Connection_BD.connect();

        PreparedStatement newAdmin = connection.prepareStatement(sql_InsertAdmin);
        newAdmin.setString(1, userlogin);
        newAdmin.setString(2, adminSurname);
        newAdmin.setString(3, adminFirstName);
        newAdmin.setString(4, adminPassword);
        newAdmin.setInt(5, statutAdmin);
        newAdmin.executeUpdate();

        int idNewAdmin = 0;
        PreparedStatement idAdmin = connection.prepareStatement
                ("SELECT idUtilisateur FROM utilisateur "
                + "WHERE login = ? AND nom = ? AND prenom = ? AND motDePasse = ? AND admin = ? ");
        idAdmin.setString(1, userlogin);
        idAdmin.setString(2, adminSurname);
        idAdmin.setString(3, adminFirstName);
        idAdmin.setString(4, adminPassword);
        idAdmin.setInt(5, statutAdmin);
        ResultSet idAdminResult = idAdmin.executeQuery();
        while(idAdminResult.next()) {
            idNewAdmin = idAdminResult.getInt("idUtilisateur");
        }
        return idNewAdmin;

    }

    /**
     * Méthode de modification des informations d'un utilisateur ou d'un administrateur et la requête.
     * @param newSurname le nouveau nom à mettre, NULL si inchangé
     * @param newFirstName le nouveau prénom, NULL si inchangé
     * @param newPassword le nouveau mot de passe, NULL si inchangé
     * @param newLogin le nouveau login, NULL si inchangé
     * @param idUser l'id de l'utilisateur que l'on souhaite modifier
     * @throws SQLException Erreur d'accès à la base de données
     */
    public static void requestForUpdateInfosUsers
    (String newSurname, String newFirstName, String newPassword, String newLogin, int idUser) throws SQLException {

        boolean changeSurname = newSurname != null;
        boolean changeFirstname = newFirstName != null;
        boolean changePassword = newPassword != null;
        boolean changeLogin = newLogin != null;

        if ((changeSurname || changeFirstname || changePassword || changeLogin) && idUser > 0) {
            Connection connection = Connection_BD.connect();

            String sql = "UPDATE utilisateur SET ";

            if (changeSurname) {
                sql += " nom = ?";
            }
            if (changeFirstname) {
                if (changeSurname) sql += ",";
                sql += " prenom = ?";
            }
            if (changePassword) {
                if (changeSurname || changeFirstname) sql += ",";
                sql += " motDePasse = ?";
            }
            if (changeLogin) {
                if (changeSurname || changeFirstname || changePassword) sql += ",";
                sql += " login = ?";
            }

            sql += " WHERE idUtilisateur = ?";

            System.out.println(sql);

            PreparedStatement newInfos = connection.prepareStatement(sql);
            int index = 0;
            if (changeSurname) {
                index++;
                newInfos.setString(index, newSurname);
            }
            if (changeFirstname) {
                index++;
                newInfos.setString(index, newFirstName);
            }
            if (changePassword) {
                index++;
                newInfos.setString(index, newPassword);
            }
            if (changeLogin) {
                index++;
                newInfos.setString(index, newLogin);
            }

            index++;
            newInfos.setInt(index, idUser);

            newInfos.executeUpdate();
        }

    }

    /**
     * Méthode de suppression d'un compte utilisateur et sa requête.
     * @param idUser l'id de l'utilisateur à supprimer
     * @throws SQLException Erreur d'accès à la base de données
     */
    public static void deleteUser(String idUser) throws SQLException {

        Connection connection = Connection_BD.connect();

        PreparedStatement deleteUser = connection.prepareStatement
                ("DELETE FROM utilisateur WHERE idUtilisateur = ?");
        deleteUser.setString(1, idUser);
        deleteUser.executeUpdate();
    }

    /**
     * Méthode pour obtenir la liste des utilisateurs et sa requête.
     * @return la liste des id des utilisateurs
     * @throws SQLException Erreur d'accès à la base de données
     */
    public static ArrayList<Integer> usersList() throws SQLException {
        Connection connection = Connection_BD.connect();

        ArrayList<Integer> usersAllList = new ArrayList<>();

        PreparedStatement listOfUsers = connection.prepareStatement("SELECT idUtilisateur " +
                "FROM utilisateur WHERE motDePasse <> ''");
        ResultSet listOfUsersResult = listOfUsers.executeQuery();
        while(listOfUsersResult.next()) {
            usersAllList.add(listOfUsersResult.getInt("idUtilisateur"));
        }
        return usersAllList;

    }

    /**
     * Méthode pour obtenir la liste des utilisateurs et sa requête.
     * @return la liste des login des utilisateurs
     * @throws SQLException Erreur d'accès à la base de données
     */
    public static ArrayList<String> usersLoginList() throws SQLException {
        Connection connection = Connection_BD.connect();

        ArrayList<String> usersAllList = new ArrayList<>();

        PreparedStatement listOfUsers = connection.prepareStatement("SELECT login FROM utilisateur");
        ResultSet listOfUsersResult = listOfUsers.executeQuery();
        while(listOfUsersResult.next()) {
            usersAllList.add(listOfUsersResult.getString("login"));
        }
        return usersAllList;

    }


    /**
     * Méthode pour obtenir la liste des utilisateurs selon leur équipe et sa requête.
     * @param idTeam l'id de l'équipe pour avoir la liste de ses utilisateurs
     * @return la liste des identifiants des utilisateurs
     * @throws SQLException Erreur d'accès à la base de données
     */
    public static ArrayList<Integer> teamUsersList(int idTeam)
            throws SQLException {

        Connection connection = Connection_BD.connect();
        ArrayList<Integer> teamsAllList = new ArrayList<>();

        PreparedStatement listOfTeams = connection.prepareStatement("SELECT U.idUtilisateur"
                + " FROM utilisateur U"
                + " INNER JOIN appartient A ON U.idUtilisateur = A.idUtilisateur"
                + " WHERE A.idEquipe = ?");
        listOfTeams.setInt(1, idTeam);
        ResultSet listOfTeamsResult = listOfTeams.executeQuery();
        while(listOfTeamsResult.next()) {
            teamsAllList.add(listOfTeamsResult.getInt("U.idUtilisateur"));
        }
        return teamsAllList;

    }

    /**
     * Méthode pour savoir si un utilisateur est administrateur ou non et sa requête.
     * @param userid l'id de l'utilisateur a testé pour savoir s'il est ou pas admin
     * @return le statut de l'utilisateur
     * @throws SQLException Erreur d'accès à la base de données
     */
    public static boolean statusUser(int userid) throws SQLException {

        Connection connection = Connection_BD.connect();
        PreparedStatement idUser = connection.prepareStatement("SELECT U.admin FROM utilisateur U "
                +"WHERE idUtilisateur = ?");
        idUser.setInt(1, userid);
        ResultSet idUserResult = idUser.executeQuery();

        int status;

        if(idUserResult.next()) {
            status = idUserResult.getInt("U.admin");
            return status == 1;
        }
        return false;

    }

    /**
     * Méthode de vérification du couple identifiant/mot de passe et sa requête. Elle permet la connexion.
     * @param login c'est ce que l'utilisateur écrit pour se connecter (donc son id)
     * @return renvoie son mot de passe
     * @throws SQLException Erreur d'accès à la base de données
     */
    public static String connectionUser(String login) throws SQLException {

        Connection connection = Connection_BD.connect();
        PreparedStatement idUser = connection.prepareStatement("SELECT motDePasse FROM utilisateur "
                +"WHERE login = ?");
        idUser.setString(1, login);
        ResultSet idUserResult = idUser.executeQuery();

        String passwordBD = null;

        while(idUserResult.next()) {
            passwordBD = idUserResult.getString("motDePasse");
        }
        return passwordBD;

    }

    /**
     * Méthode pour récupérer les informations d'un utilisateur d'après son login et sa requête.
     * @param login le login de l'utilisateur dont on souhaite avoir les informations
     * @return les informations de l'utilisateur
     * @throws SQLException Erreur d'accès à la base de données
     */
    public static HashMap<String, String> requestForUserInfos(String login) throws SQLException {

        Connection connection = Connection_BD.connect();
        HashMap<String, String> result = new HashMap<>();

        PreparedStatement idUser = connection.prepareStatement("SELECT idUtilisateur, nom," +
                "prenom, admin FROM utilisateur " +
                "WHERE login = ? ");

        idUser.setString(1,login);
        ResultSet idUserResult = idUser.executeQuery();

        while(idUserResult.next()){
            result.put("userId", idUserResult.getString("idUtilisateur"));
            result.put("name", idUserResult.getString("nom"));
            result.put("firstname", idUserResult.getString("prenom"));
            result.put("admin", idUserResult.getString("admin"));
        }
        return result;


    }

    /**
     * Méthode pour récupérer les informations d'un utilisateur d'après son id et sa requête.
     * @param id l'id de l'utilisateur dont on souhaite avoir les informations
     * @return les informations de l'utilisateur
     * @throws SQLException Erreur d'accès à la base de données
     */
    public static HashMap<String, String> requestForUserInfos(int id) throws SQLException {

        Connection connection = Connection_BD.connect();
        HashMap<String, String> result = new HashMap<>();


        PreparedStatement idUser = connection.prepareStatement("SELECT login, nom," +
                "prenom, admin FROM utilisateur " +
                "WHERE idUtilisateur = ? ");

        idUser.setInt(1,id);
        ResultSet idUserResult = idUser.executeQuery();

        while(idUserResult.next()){
            result.put("login", idUserResult.getString("login"));
            result.put("name", idUserResult.getString("nom"));
            result.put("firstname", idUserResult.getString("prenom"));
            result.put("admin", idUserResult.getString("admin"));
        }
        return result;


    }

    /**
     * Méthode pour obtenir la liste des équipes auxquelles appartient un utilisateur et sa requête.
     * @param id l'id de l'utilisateur pour savoir dans quelles équipes il peut appartenir
     * @return la liste des id des équipes auxquelles appartient un utilisateur
     * @throws SQLException Erreur d'accès à la base de données
     */
    public static ArrayList<Integer> requestForTeamsOfUser(int id) throws SQLException {

        Connection connection = Connection_BD.connect();
        ArrayList<Integer> result = new ArrayList<>();

        PreparedStatement idUser = connection.prepareStatement("SELECT E.idEquipe FROM " +
                "equipe E INNER JOIN appartient AT ON E.idEquipe = AT.idEquipe WHERE AT.idUtilisateur = ?");

        idUser.setInt(1, id);
        ResultSet idUserResult = idUser.executeQuery();

        while(idUserResult.next()){
            result.add(idUserResult.getInt("idEquipe"));
        }
        return result;

    }

    /**
     * Méthode pour compter le nombre de fois qu'un utilisateur est présent dans la base de donnée et sa requête.
     * @param login l'id de l'utilisateur pour savoir combien de fois il est présent
     * @return le nombre de fois qu'un utilisateur est présent dans la base de donnée
     * @throws SQLException Erreur d'accès à la base de données
     */
    public static int requestForNumberAccountUser(String login)throws SQLException{

        Connection connection = Connection_BD.connect();
        int result = 0;
        String idPercent = login + "%";

        PreparedStatement idUser = connection.prepareStatement("SELECT count(idUtilisateur) "
                + "AS 'Nombre d''identifiant pour l''utilisateur' FROM utilisateur WHERE login LIKE ? ");
        idUser.setString(1, idPercent);
        ResultSet idUserResult = idUser.executeQuery();

        while(idUserResult.next()) {
            result = idUserResult.getInt("Nombre d'identifiant pour l'utilisateur");
        }
        return result;

    }

}
