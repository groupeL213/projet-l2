module Projet.L2 {
    requires javafx.fxml;
    requires javafx.controls;
    requires java.sql;
    requires ij;
    requires java.desktop;
    requires mysql.connector.java;
    requires javafx.swing;
    requires junit;
    requires hamcrest.core;

    opens view;
}