package controller;


import ij.plugin.filter.ParticleAnalyzer;
import model.AlgorithmRequests;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Représente un algorithme de comptage de cellule.
 */
public class Algorithm {

    private final int id;
    private final String name, description;

    private final String method;
    private final String methodColor = "b&w";
    private final int OPTIONS = ParticleAnalyzer.RECORD_STARTS + ParticleAnalyzer.SHOW_OVERLAY_MASKS + ParticleAnalyzer.IN_SITU_SHOW;
    private final double minT;
    private final double maxT;
    private final double minC;
    private final double maxC;      //minC,maxC -> [0.0-1.0]

    /**
     * Constructeur de Algorithm
     * @param idAlgo l'ID de l'Algorithm dans la BD
     * @throws SQLException Erreur d'accès à la base de données
     */
    public Algorithm(int idAlgo) throws SQLException {

        HashMap<String,String> info = AlgorithmRequests.requestForAlgorithm(idAlgo);

        this.id = idAlgo;
        this.name = info.get("name");
        this.description = info.get("description");

        this.method = info.get("methodThreshold")+" "+methodColor;
        this.minT = Double.parseDouble(info.get("Tmin"));
        this.maxT = Double.parseDouble(info.get("Tmax"));
        this.minC = Double.parseDouble(info.get("Cmin"));
        this.maxC = Double.parseDouble(info.get("Cmax"));
    }

    private Algorithm(int idAlgo, String name, String description, String method, double minT, double maxT,
                      double minC, double maxC) {
        this.id = idAlgo;
        this.name = name;
        this.description = description;
        this.method = method;
        this.minT = minT;
        this.maxT = maxT;
        this.minC = minC;
        this.maxC = maxC;
    }

    /**
     * Méthode pour écrire un nouvel algorithme dans la base de données.
     * @param name le nom de l'algorithme
     * @param method sa méthode de Threshold (voir javadoc ImageJ)
     * @param minT l'aire minimal en pixel d'une cellule
     * @param maxT l'aire maximal en pixel d'une cellule
     * @param minC la circularité minimum d'une cellule
     * @param maxC la circularité maximum d'une cellule (1 = cercle parfait)
     */
    public static boolean createNewAlgo(String name, String method, double minT, double maxT,
                              double minC, double maxC){
        try {
            AlgorithmRequests.requestForNewAlgo(name, method, minT, maxT, minC, maxC);
        } catch (SQLException throwables) {
            return false;
        }
        return true;
    }

    /**
     * Renvoie la méthode de Threshold de l'algorithme
     * @return la méthode de l'algorithme
     */
    public String getMethod() {
        return method;
    }

    /**
     * Renvoie les options de l'algorithme
     * @return les options sous la forme d'un int
     */
    public int getOptions() {
        return OPTIONS;
    }

    /**
     * Renvoie l'aire minimale des cellules comptées
     * @return l'aire minimale
     */
    public double getMinT() {
        return minT;
    }

    /**
     * Renvoie l'aire maximale des cellules comptées
     * @return l'aire maximale
     */
    public double getMaxT() {
        return maxT;
    }

    /**
     * Renvoie le taux de circularité minimal des cellules comptées
     * @return le taux minimal
     */
    public double getMinC() {
        return minC;
    }

    /**
     * Renvoie le taux de circularité maximal des cellules comptées
     * @return le taux maximal
     */
    public double getMaxC() {
        return maxC;
    }

    /**
     * Renvoie la description de l'algorithme, qui donne des informations sur son utilisation
     * @return la description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Renvoie le nom de l'algorithme
     * @return le nom de l'algorithme
     */
    public String getName() {
        return name;
    }

    /**
     * Renvoie l'id de l'algorithme
     * @return id de l'algorithme
     */
    public int getId() {
        return id;
    }

    /**
     * Donne la valeur de l'algorithme au format String
     * @return nom de l'algorithme
     */
    public String toString() {
        return name;
    }

    /**
     * Permet d'obtenir la liste des Algorithmes contenus dans la base de données.
     * @return la liste des algorithmes de la base de données
     */
    public static ArrayList<Algorithm> getAlgorithmList() {
        ArrayList<Algorithm> list = new ArrayList<>();

        ArrayList<HashMap<String, String>> algoListInfo ;
        try {
            algoListInfo = AlgorithmRequests.requestForListAlgoInfos();
            for (HashMap<String,String> algoInfo : algoListInfo) {
                list.add(new Algorithm(Integer.parseInt(algoInfo.get("idAlgorithme")), algoInfo.get("nom"),
                        algoInfo.get("description"), algoInfo.get("methodeThreshold"),
                        Double.parseDouble(algoInfo.get("Tmin")), Double.parseDouble(algoInfo.get("Tmax")),
                        Double.parseDouble(algoInfo.get("Cmin")), Double.parseDouble(algoInfo.get("Cmax"))));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return list;
    }

}
