package controller;

import java.awt.*;
import java.sql.SQLException;

import ij.ImagePlus;
import ij.measure.ResultsTable;
import ij.plugin.filter.EDM;
import ij.plugin.filter.ParticleAnalyzer;
import ij.process.ImageProcessor;
import ij.process.LUT;
import ij.process.TypeConverter;
import model.ImageRequests;
import model.PileRequests;

import javax.swing.*;

/**
 * Résume les principales fonctionnalités de ImageJ et simplifie les appels
 */
public class ImageJ2 {

	Algorithm algo;

	ImagePlus imp;
	ImageProcessor ipnb;

	ResultsTable rt;

	Image img;
	String title;

	boolean fondNoir;

	AnalyzeResults results;

    /**
	 * @param img l'objet java contenant l'Image
     * @param title le titre de l'image
	 * @param algo l'algorithme à utiliser
	 * @param fondNoir true si l'image a un fond noir (en général sur les images en couleur)
	 */
	public ImageJ2(Image img, String title, Algorithm algo, boolean fondNoir){
		this.img = img;
		this.title = title;
		this.imp = new ImagePlus(title, img);
		this.algo = algo;
		this.fondNoir = fondNoir;
	}

	/**
	 * affiche une image traitée par ImageJ
	 */
	public void showConvertedImage(){
		this.imp = new ImagePlus(title, img);

		this.toBinary();
		this.threshold();
		this.watershed();
		this.analyzer();
		imp.show();
		imp.getWindow().setAlwaysOnTop(true);
		imp.getWindow().setLocation(10,10);
	}

	public void closeImage(){
		this.imp.close();
	}

	/**
	 * Appelle les fonctions pour une analyse basique
	 * @return le nombre de cellules trouvées
	 */
	public int runAnalyze(){
		this.toBinary();
		this.threshold();
		this.watershed();
		return this.analyzer();
	}

	/**
	 * Convertie l'image en nuances de gris
	 */
	public void toBinary(){
		ImageProcessor ip = imp.getProcessor();
		TypeConverter convert = new TypeConverter(ip, false);
		ipnb = convert.convertToByte();
		imp.setProcessor(ipnb);
	}

    /**
     * Convertie l'image en nuance binaire (noir et blanc) selon la méthode de l'objet Algorithme
     */
	public void threshold(){

		if(this.fondNoir) {
			ipnb.setAutoThreshold(algo.getMethod()+" dark");
		}
		else
			ipnb.setAutoThreshold(algo.getMethod());

		ipnb.autoThreshold();

		imp.setProcessor(ipnb);

		if(this.fondNoir) {
			LUT lut = ipnb.getLut();
			ipnb.setLut(lut.createInvertedLut());
		}

	}

	/**
	 * Sépare les cellules jointes
	 */
	public void watershed(){
		EDM watershed = new EDM();
		watershed.setup("watershed", imp);
		watershed.run(ipnb);
	}

    /**
     * Compte les cellules d'une image en noir et blanc sans nuances, selon les variables numériques de l'algorithme.
	 * Créer un objet AnalyzeResults pour stocker les résultats.
     * @return le nombre de cellules trouvées
     */
	public int analyzer(){

		rt = new ResultsTable();
		int options;
		options = algo.getOptions();
		double minS = algo.getMinT();
		double maxS = algo.getMaxT();
		double minC = algo.getMinC();
		double maxC = algo.getMaxC();

		ParticleAnalyzer pa = new ParticleAnalyzer(options,0,rt,minS,maxS,minC,maxC);

		if(!pa.analyze(imp))
			System.out.println("Erreur lors de l'analyse");

		int colXStart = rt.getColumnIndex("XStart");
		int colYStart = rt.getColumnIndex("YStart");

		this.results = new AnalyzeResults(rt.getColumnAsDoubles(colXStart),rt.getColumnAsDoubles(colYStart));

		return results.getPosX().length;
	}

	/**
	 *
	 * @param id id de l'image
	 * @throws SQLException Erreur d'accès à la base de données
	 */
	public void writePiles(int id) throws SQLException {
		if(this.results == null){
			JOptionPane.showConfirmDialog(null,
					"L'analyse n'a pas été faite pour cette image, veuillez ré-essayer."
					, "Erreur", JOptionPane.INFORMATION_MESSAGE);
			return;
		}

		int[] nbCellsPerPile = new int[results.getPosX().length];
		for (int i=0; i<nbCellsPerPile.length; i++) nbCellsPerPile[i] = 1;
		PileRequests.requestForNewPileList(results.getPosX(), results.getPosY(), nbCellsPerPile, id);

		ImageRequests.updateCellsNumber(results.getPosX().length, id);
	}
	 
}
