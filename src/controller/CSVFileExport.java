package controller;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;

public class CSVFileExport {

    OutputStreamWriter csvWriter;

    /**
     * Constructeur pour l'export d'un fichier
     * @param nom le nom du fichier à créer
     * @throws IOException
     */
    public CSVFileExport(String nom) throws IOException {

        csvWriter = new OutputStreamWriter(new FileOutputStream("export/"+nom+".csv"), StandardCharsets.UTF_8);

    }

    /**
     * méthode simple pour écrire dans le fichier, le changement de colonne est automatique.
     * @param text le texte à écrire
     * @throws IOException
     */
    public void write(String text) throws IOException {
        csvWriter.write(text+",");
    }

    /**
     * méthode surchargée pour écrire dans le fichier
     * @param text le texte à écrire
     * @param last permet la surcharge, dans ce cas elle permet de passer à la ligne suivante.
     * @throws IOException
     */
    public void write(String text,boolean last) throws IOException {
        csvWriter.write(text+"\n");

    }

    /**
     * Permet d'écrire un ensemble de colonnes à partir d'un tableau
     * @param list la liste String à écrire
     * @throws IOException
     */
    public void write(String[] list) throws IOException {
        for(int i=0;i<list.length-1;i++){
            write(list[i]);
        }
        write(list[list.length-1],true);
    }

    /**
     * Passe à la ligne suivante
     * @throws IOException
     */
    public void nextLine() throws IOException {
        csvWriter.write("\n");
    }

    /**
     * Ferme le fichier
     * @throws IOException
     */
    public void close() throws IOException {
        csvWriter.close();
    }
}
