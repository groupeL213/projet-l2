package controller;

import javafx.application.Application;
import model.Connection_BD;
import view.Launcher;

import java.sql.SQLException;

/**
 * Classe principale du programme. C'est elle qui exécute la première interface graphique.
 */
public class Main  {

	public static final String objectToWait = "";
	/**
	 * Paramètre la reconnexion automatique à la base de données. True si on souhaite une reconnexion automatique.
	 * False sinon.
	 */
	public static final boolean automaticReconnection = true;
	/**
	 * Délai entre chaque test de validité de la connexion à la base de données, en secondes.
	 */
	public static final int delayToCheckConnectionToDB = 3;


	/**
	 * Méthode main. Exécute l'application et ouvre la première fenêtre.
	 * @param args arguments
	 */
	public static void main(String[] args) {
		Thread t = new Thread() {
			public void run() {
				Connection_BD.isAThreadWaitingForConnection=true;
				Connection_BD.createConnexion();
			}
		};
		t.start();



		Application.launch(Launcher.class, args);
	}

}
