package controller;

/**
 * Permet de stocker les résultats d'une analyze sous forme d'un objet.
 * Evite de devoir créer une matrice pour le renvoie, et laisse la possibilité d'adapter
 * facilement le nombre de champs à renvoyer (si l'on veut renvoyer aussi l'aire par exemple)
 */
public class AnalyzeResults {

    private double[] posX;
    private double[] posY;

    /**
     * Constructeur de AnalyzeResults
     * @param posX un tableau contenant les positions X des cellules
     * @param posY un tableau contenant les positions Y des cellules
     */
    public AnalyzeResults(double[] posX, double[] posY){
        this.posX=posX;
        this.posY=posY;
    }

    /**
     * Renvoie le tableau des positions en X
     * @return le tableau des X
     */
    public double[] getPosX() {
        return posX;
    }

    /**
     * Renvoie le tableau des positions en Y
     * @return le tableau des Y
     */
    public double[] getPosY() {
        return posY;
    }
}
