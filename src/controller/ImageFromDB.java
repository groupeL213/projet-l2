package controller;


import javafx.scene.image.Image;
import model.ImageRequests;

import java.io.InputStream;
import java.sql.SQLException;

/**
 * Modélise une image telle qu'elle est représentée dans la base de données.
 */
public class ImageFromDB {

    private final int id;
    private final String name;
    private final Image image;
    private final Date addDate;
    private final boolean blackBackground;
    private final int numberOfCells;

    public final static int width = 362;
    public final static int height = 319;

    /**
     * Permet de créer une nouvelle image
     * @param id id de l'image
     * @param name nom de l'image
     * @param img fichier de l'image
     * @param date date d'importation de l'image
     * @param nb nombre de cellules de l'image
     * @param bb fond noir de l'image (true si fond noir ; false sinon)
     */
    public ImageFromDB(int id, String name, InputStream img, String date, int nb, boolean bb) {
        this.id = id;
        this.name = name;
        this.image = new Image(img,width,height,true,false);
        this.addDate = new Date(date);
        this.numberOfCells = nb;
        this.blackBackground = bb;
    }

    /**
     * Donne l'ID unique de l'image.
     * @return l'id de l'image
     */
    public int getId() {
        return id;
    }

    /**
     * Donne le nom de l'image.
     * @return nom de l'image
     */
    public String getName() {
        return name;
    }

    /**
     * Donne la date d'ajout de l'image.
     * @return date d'ajout de l'image au format Date
     */
    public Date getAddDate() {
        return addDate;
    }

    /**
     * Précise si le fond de cette image est noir.
     * @return Vrai si le fond est noir ; faux sinon
     */
    public boolean isBlackBackground() {
        return blackBackground;
    }

    /**
     * Donne le nombre de cellules de cette image.
     * @return nombre de cellules
     */
    public int getNumberOfCells() {
        return numberOfCells;
    }

    /**
     * Donne l'image de l'instance
     * @return image
     */
    public Image getImage() {
        return image;
    }

    /**
     * Modifie le nom de l'image. Met à jour la base de données.
     * @param newName nouveau nom
     * @return true si réussite ; false sinon
     */
    public boolean modifyName(String newName) {
        try {
            ImageRequests.requestForUpdatingNameImage(newName, this.id);
            return true;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return false;
        }
    }
}
