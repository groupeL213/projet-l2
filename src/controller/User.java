package controller;


import javafx.scene.control.CheckBox;
import model.BelongRequests;
import model.Statistics;
import model.TeamRequests;
import model.UserRequests;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Représente un utilisateur administrateur ou normal. Caractérisé par son nom, prénom, ID de connexion,
 * le caractère administrateur ou non. Le mot de passe de l'utilisateur n'est jamais stocké dans cette classe.
 * Permet aussi d'accéder à diverses statistiques liées à l'utilisateur, telles que le nombre d'images importées
 * qu'il peut voir, le nombre total de cellules de ces images, le nombre moyen de cellules par image.
 */
public class User {

    public static final int TYPE_MODIF_NAME = 0;
    public static final int TYPE_MODIF_FIRSTNAME = 1;

    private int id;
    private String name, firstname, login;
    private boolean admin;

    /* STATS */
    private int numberOfImportedImages, numberOfCells;
    private float averageNumberOfCells;
    /* ----- */

    /**
     * Constructeur d'un Utilisateur, les paramètres sont récupérés dans la BD
     * @param name son nom
     * @param firstname son prénom
     * @param password son mot de passe
     * @param admin true si c'est un admin
     * @throws SQLException Une erreur est survenue lors de l'accès à la base de données.
     */
    private User(String name, String firstname, String password, boolean admin)
            throws SQLException {

        this.login = removeAccents(firstname) + removeAccents(name);
        int numberOfUsersAlreadyCreated = UserRequests.requestForNumberAccountUser(this.login);
        if (numberOfUsersAlreadyCreated > 0) {
            int integerToAdd = numberOfUsersAlreadyCreated+1;
            ArrayList<String> userList = UserRequests.usersLoginList();
            while (userList.contains(this.login + integerToAdd)) {
                integerToAdd++;
            }
            this.login += integerToAdd;
        }

        this.name = name;
        this.firstname = firstname;
        this.admin = admin;
        String passwordHashed = BCrypt.hashpw(password, BCrypt.gensalt());

        if (this.admin)
            this.id = UserRequests.requestForNewAdmin(this.login, this.name, this.firstname, passwordHashed);
        else
            this.id = UserRequests.requestForNewUser(this.login, this.name, this.firstname, passwordHashed);

        this.numberOfCells = -1;
        this.numberOfImportedImages = -1;
        this.averageNumberOfCells = -1;

    }

    /**
     * Construit un nouvel objet User à partir d'un login. Cet utilisateur ne sera pas ajouté à la BDD
     * mais ses informations seront récupérées dans la BDD grâce à son login. Si le login n'est pas
     * trouvé, alors ses attributs ne sont pas initialisés.
     * @param login Le login de l'utilisateur à retrouver.
     */
    public User(String login) {
        HashMap<String, String> userInfos;
        try {
            userInfos = UserRequests.requestForUserInfos(login);
            this.login = login;
            this.id = Integer.parseInt(userInfos.get("userId"));
            this.name = userInfos.get("name");
            this.firstname = userInfos.get("firstname");
            this.admin = userInfos.get("admin").equals("1");
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        this.numberOfCells = -1;
        this.numberOfImportedImages = -1;
        this.averageNumberOfCells = -1;

    }

    /**
     * Construit un nouvel objet User à partir d'un id. Cet utilisateur ne sera pas ajouté à la BDD
     * mais ses informations seront récupérées dans la BDD grâce à son id. Si l'id n'est pas
     * trouvé, alors ses attributs ne sont pas initialisés.
     * @param id L'id de l'utilisateur à retrouver.
     */
    public User(int id) {
        HashMap<String, String> userInfos;
        try {
            userInfos = UserRequests.requestForUserInfos(id);
            this.login = userInfos.get("login");
            this.id = id;
            this.name = userInfos.get("name");
            this.firstname = userInfos.get("firstname");
            this.admin = userInfos.get("admin").equals("1");
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        this.numberOfCells = -1;
        this.numberOfImportedImages = -1;
        this.averageNumberOfCells = -1;

    }

    /**
     * Donne l'identité de l'utilisateur sous la forme Prénom Nom. Par exemple, Aurélie Abraham.
     * @return La chaîne de caractères qui décrit l'utilisateur.
     */
    public String getIdentite(){
        return this.firstname +" "+this.name;
    }

    /**
     * Donne  l'identifiant de l'utilisateur.
     * @return L'identifiant de l'utilisateur.
     */
    public int getId() {
        return id;
    }

    /**
     * Donne  le login de l'utilisateur.
     * @return Le login de l'utilisateur.
     */
    public String getLogin() {
        return login;
    }

    /**
     * Donne le nom de famille de l'utilisateur.
     * @return Le nom de l'utilisateur.
     */
    public String getName(){
        return this.name;
    }

    /**
     * Donne le prénom de l'utilisateur.
     * @return Prénom de l'utilisateur.
     */
    public String getFirstname(){
        return this.firstname;
    }

    /**
     * Précise si l'utilisateur est administrateur.
     * @return True si l'utilisateur est administrateur ; false sinon.
     */
    public boolean isAdmin() {
        return admin;
    }

    /**
     * Retourne une copie de cet utilisateur.
     * @return La copie de l'utilisateur.
     */
    public User copy() {
        return new User(this.login);
    }

    /**
     * Donne le nombre d'images importées que peut voir l'utilisateur. Les images comptabilisées sont celles
     * qui sont comprises dans les campagnes visibles par l'utilisateur (dont les équipes sont aussi celles
     * de l'utilisateur).
     * Cette méthode comporte une optimisation : au premier appel, la BDD est interrogée, mais les valeurs
     * issues de la BDD sont ensuite stockées dans l'objet User. Pour les appels suivants, la valeur stockée
     * est directement retournée.
     * @return Le nombre d'images importées visibles par l'utilisateur.
     */
    public int getNumberOfImportedImages() {
        if (numberOfImportedImages < 0) {
            loadStatistics();
        }
        return numberOfImportedImages;
    }

    /**
     * Donne le nombre de cellules sur toutes les images que peut voir l'utilisateur. Ces images sont celles qui sont
     * comprises dans les campagnes visibles par l'utilisateur. Pour plus de précisions, voir
     * getNumberOfImportedImages().
     * @return Le nombre de cellules visibles sur toutes les images que peut voir l'utilisateur.
     */
    public int getNumberOfCells() {
        if (numberOfCells < 0) {
            loadStatistics();
        }
        return numberOfCells;
    }

    /**
     * Donne le nombre moyen de cellules par image. Les images concernées sont uniquement celles que peut voir
     * l'utilisateur. Pour plus d'informations, voir getNumberOfImportedImages().
     * @return Le nombre moyen de cellules par image.
     */
    public float getAverageCells() {
        if (averageNumberOfCells < 0) {
            loadStatistics();
        }
        return averageNumberOfCells;
    }

    /**
     * Charge les statistiques dans l'objet User. Usage réservé à cette classe.
     * Statistiques concernées : nombre d'images importées, nombre total de cellules, nombre moyen de cellules.
     */
    private void loadStatistics() {
        try {
            HashMap<String, Float> stats = Statistics.requestHomepageStatistics(this.login);
            numberOfImportedImages = Math.round(stats.get("Images vues par l'utilisateur"));
            numberOfCells = Math.round(stats.get("Cellules dans les images vues par l'utilisateur"));
            averageNumberOfCells = stats.get("Nombre moyen de celulles dans les images vues par l'utilisateur");
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    /**
     * Permet d'ajouter l'utilisateur dans une équipe existante et dont il ne fait pas déjà partie.
     * Dans les cas contraires, aucune opération n'est effectuée.
     * @param idTeam l'id de l'équipe dans laquelle on ajoute l'utilisateur
     * @param teamsOfUser la liste des ID des équipes dont l'utilisateur fait déjà partie
     * @param globalListTeam la liste des ID de toutes les équipes déjà existantes
     */
    private void addToTeam(int idTeam, ArrayList<Integer> teamsOfUser, ArrayList<Integer> globalListTeam) {
        try {
            if (!teamsOfUser.contains(idTeam) && globalListTeam.contains(idTeam)) {
                BelongRequests.requestToAddCorrelationBetweenUserAndTeam(this.id, idTeam);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

    }

    /**
     * Ajoute l'utilisateur dans les équiqes dont les ID ont été transmis dans le paramètre.
     * Si l'utilisateur est déjà présent dans l'une d'elles ou que l'équipe est inexistante,
     * aucune opération n'est effectuée.
     * @param idTeamList liste des id des équipes dans lesquelles il faut ajouter l'utilisateur
     */
    public void addToTeamList(ArrayList<Integer> idTeamList) {
        try {
            ArrayList<Integer> teamsIDList = UserRequests.requestForTeamsOfUser(this.id);
            ArrayList<Integer> globalTeamsList = TeamRequests.teamsList();
            for (int i : idTeamList) {
                addToTeam(i, teamsIDList, globalTeamsList);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    /**
     * Supprime l'utilisateur. Ecrase ses données personnelles (login, nom, prénom) et vide son mot de passe.
     * @return true si réussite ; false si échec
     */
    public boolean deleteThisUser() {
        try {
            UserRequests.requestForUpdateInfosUsers("Utilisateur supprimé", "Utilisateur supprimé",
                    "", "UtilisateurSupprime", this.id);
            BelongRequests.requestToDeleteAllCorrelationsForAUser(this.id);

            return true;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return false;
        }
    }

    /**
     * Enregistre les changements effectués sur l'écran de visualisation d'un compte utilisateur dans la BDD.
     * @param password nouveau mot de passe (en clair)
     * @param teams liste des checkbox des équipes
     * @return true si les changements ont réussi ; false sinon
     */
    public boolean saveChanges(String password, ArrayList<CheckBox> teams) {


        String hashed = (password.equals("")) ? "" : BCrypt.hashpw(password, BCrypt.gensalt());

        ArrayList<Integer> listTeamsOfUser;

        try {
            if (!hashed.equals(""))
                UserRequests.requestForUpdateInfosUsers(null, null, hashed, null, this.id);
            listTeamsOfUser = UserRequests.requestForTeamsOfUser(this.id);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return false;
        }
        ArrayList<Integer> listIDTeamsSelected = new ArrayList<>();

        for (CheckBox check : teams) {
            if (check.isSelected()) {
                listIDTeamsSelected.add(((Team)check.getUserData()).getId());
            }
        }
        try {
            for (int teamID : listIDTeamsSelected) {
                if (!listTeamsOfUser.contains(teamID)) {
                    BelongRequests.requestToAddCorrelationBetweenUserAndTeam(this.id, teamID);
                }
            }
            for (int teamID : listTeamsOfUser) {
                if (!listIDTeamsSelected.contains(teamID)) {
                    BelongRequests.requestToDeleteCorrelation(this.id, teamID);
                }
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return false;
        }

        return true;

    }

    /**
     * Permet de modifier le nom ou le prénom de l'utilisateur.
     * @param name nouveau nom ou nouveau prénom
     * @param type type de modification : utiliser les constantes TYPE_MODIF_NAME ou TYPE_MODIF_FIRSTNAME
     * @return true si réussite ; false sinon
     */
    public boolean modifyName(String name, int type) {
        switch(type) {
            case TYPE_MODIF_NAME:
                // modifier nom
                this.name = name;
                break;
            case TYPE_MODIF_FIRSTNAME:
                // modifier firstname
                this.firstname = name;
                break;
            default: return false;
        }
        try {
            UserRequests.requestForUpdateInfosUsers(this.name, this.firstname, null, null, this.id);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return false;
        }
        return true;
    }



    /* ------------------- STATIC METHODS --------------------- */


    /**
     * Vérifie les informations de connexion.
     * Charge une nouvelle fenêtre selon les informations.
     * @param login Le login entré par l'utilisateur.
     * @param password Le mot de passe entré par l'utilisateur.
     * @return L'utilisateur qui s'est identifié.
     */
    public static User verifyConnection(String login, String password) {
        User u = null;

        if (verifyPassword(login, password)) {
            u = new User(login);
        }

        return u;
    }

    /**
     * Vérifie que le couple (identifiant, mot de passe) est correct selon les données de la base de données.
     * @param login login de connexion
     * @param password mot de passe
     * @return true si le couple est correct ; false sinon
     */
    public static boolean verifyPassword(String login, String password) {
        boolean loginExists;
        boolean loginPassCorrects;

        String passwordDB=null;
        try {
            passwordDB = UserRequests.connectionUser(login);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        loginExists = passwordDB != null;
        loginPassCorrects = loginExists && BCrypt.checkpw(password, passwordDB);

        return loginPassCorrects;
    }

    /**
     * Crée un nouvel utilisateur à partir des paramètres.
     * @param name Le nom de famille
     * @param firstname Le prénom
     * @param password Le mot de passe
     * @param admin Si l'utilisateur est administrateur
     * @return L'utilisateur créé. Vaut null si une erreur est survenue lors de la créatio.
     */
    public static User createNewUser(String name, String firstname,
                                     String password, boolean admin) {
        User u = null;

        try {
            u = new User(name, firstname, password, admin);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return u;
    }

    /**
     * Classe qui supprime les accents d'une chaîne de caractères.
     * @param s La chaîne dans laquelle on doit supprimer les accents.
     * @return La copie de la chaîne mais sans les accents.
     */
    public static String removeAccents(String s) {
        HashMap<Character, Character> table = new HashMap<>();
        table.put('é', 'e');
        table.put('è', 'e');
        table.put('ê', 'e');
        table.put('ë', 'e');
        table.put('à', 'a');
        table.put('ä', 'a');
        table.put('â', 'a');
        table.put('ï', 'i');
        table.put('î', 'i');
        table.put('ô', 'o');
        table.put('ö', 'o');
        table.put('ç', 'c');

        StringBuilder result= new StringBuilder();

        for (int i=0 ; i<s.length() ; i++) {
            if (table.containsKey(s.charAt(i))) {
                result.append(table.get(s.charAt(i)));
            } else {
                result.append(s.charAt(i));
            }
        }

        return result.toString();

    }

}
