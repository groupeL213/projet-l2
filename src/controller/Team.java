package controller;

import model.TeamRequests;

import java.sql.SQLException;
import java.util.HashMap;

/**
 * Modélise une équipe, caractérisée par son ID, son nom, sa date de création.
 */
public class Team {
    private int id;
    private String name;
    private Date creationDate;


    /**
     * Constructeur utilisé pour récupérer un objet de type Team pour représenter une équipe déjà existante.
     * Recupère les informations dans la base de données à partir de l'ID.
     * @param id L'ID de l'équipe à retrouver.
     */
    public Team(int id) {
        try {
            HashMap<String, String> infos = TeamRequests.requestForInfosOfTeam(id);
            this.name = infos.get("Name");
            this.creationDate = new Date(infos.get("Date of creation"));


        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        this.id = id;

    }

    /**
     * Constructeur utilisé pour créer une nouvelle équipe à partir des paramètres. Ajoute l'équipe dans la base
     * de données. Usage strictement réservé à cette méthode.
     * @param name Le nom de l'équipe.
     * @throws SQLException Erreur d'accès à la base de données
     */
    private Team(String name) throws SQLException {
        this.name = name;
        this.creationDate = new Date();

        TeamRequests.requestForNewTeam(this.name, this.creationDate.toString());
    }


    /**
     * Renvoie l'ID de l'équipe
     * @return l'ID
     */
    public int getId() {
        return id;
    }

    /**
     * Renvoie le nom de l'équipe
     * @return le nom
     */
    public String getName() {
        return name;
    }

    /**
     * Modifie le nom de l'équipe
     * @param name le nouveau nom de l'équipe
     */
    public void setName(String name) {
        this.name = name;
    }


    /**
     * Donne la chaîne de caractères correspondant à cette équipe. Permet d'afficher le nom de l'équipe dans les
     * ComboBox.
     * @return la chaîne de caractères
     */
    public String toString() {
        return this.name;
    }

    /**
     * Donne la date de création de l'équipe au format Date.
     * @return date de création de l'équipe
     */
    public Date getCreationDate() {
        return this.creationDate;
    }

    /**
     * Permet de créer une nouvelle équipe. L'insère dans la base de données.
     * @param name nom de la nouvelle équipe
     * @return nouvelle équipe créée
     */
    public static Team createNewTeam(String name) {
        Team t = null;
        try {
            t = new Team(name);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return t;
    }

    /**
     * Supprime l'équipe courante seulement si elle n'est reliée à aucune campagne.
     * @return true si la suppression est une réussite ; false sinon
     */
    public boolean deleteThisTeam() {
        try {
            if (TeamRequests.isTeamConnectedToACampaign(this.id)) {
                return false;
            } else {
                TeamRequests.requestForDeletingTeam(this.id);
                return true;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return false;
        }

    }


}
