package controller;


import ij.ImagePlus;
import model.AssayRequests;
import model.CampaignRequests;
import model.ImageRequests;
import model.Statistics;
import view.InformationWindow;

import javax.swing.*;
import java.awt.*;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Modélise un essai. Un essai regroupe des images, et appartient à une campagne de mesure.
 */
public class Assay extends CommonData {

    private final int idCampaign, idAlgorithm;

    private int numberOfCells;
    private int numberOfImages;
    private float averageNumberOfCells;

    private Algorithm algo;

    /**
     * Constructeur privé de Assay utilisé pour créer un nouvel essai. Il sera ajouté dans la base de données.
     * Ce constructeur ne peut être utilisé que par les méthodes de cette classe, notamment createNewAssay().
     * @param name Le nom de l'Essai
     * @param creationDate La date de création. Sera aussi la date de dernière modification.
     * @param description Sa description
     * @param idCampagne L'ID de la campagne associée.
     * @param idAlgorithme L'ID de l'algorithme associé.
     * @param creator Utilisateur ayant créé cet essai.
     * @throws SQLException Si une erreur survient lors de de la modification de la base de données.
     */
    private Assay(String name, Date creationDate, String description, int idCampagne, int idAlgorithme, User creator)
            throws SQLException {
        super(name, description, creationDate, creationDate);
        this.idAlgorithm = idAlgorithme;
        this.idCampaign = idCampagne;
        this.id = AssayRequests.requestForNewAssay(name, creationDate.toString(), description,
                creationDate.toString(), idCampaign, idAlgorithm, creator.getId());
        this.algo = new Algorithm(idAlgorithme);
        this.numberOfCells = -1;
        this.numberOfImages = -1;
        this.averageNumberOfCells = -1;
    }

    /**
     * Constructeur privé de Assay utilisé pour créer une instance de Assay à partir de tous ses attributs. Il ne
     * sera pas ajouté à la base de données. Réservé à getAssaysSeenByUser() de cette classe.
     * @param id id de l'essai
     * @param name nom de l'essai
     * @param creationDate date de création de l'essai
     * @param modificationDate date de dernière modification de l'essai
     * @param description description de l'essai
     * @param idCampagne id de la campagne associée à l'essai
     * @param idAlgorithm id de l'algorithme de cet essai
     * @param creator créateur de l'essai
     * @throws SQLException Erreur d'accès à la base de données
     */
    private Assay(int id, String name, Date creationDate, Date modificationDate, String description, int idCampagne,
                  int idAlgorithm, User creator) throws SQLException {
        super(name, description, creationDate, modificationDate);
        this.idCampaign = idCampagne;
        this.idAlgorithm = idAlgorithm;
        this.algo = new Algorithm(idAlgorithm);
        this.id = id;
        this.numberOfCells = -1;
        this.numberOfImages = -1;
        this.averageNumberOfCells = -1;
    }

    /**
     * Constructeur utilisé pour créer un objet Assay pour un essai déjà existant à partir de son ID à l'aide
     * de la base de données.
     * @param id L'ID de l'essai à retrouver.
     * @throws SQLException Erreur d'accès à la base de données
     */
    public Assay(int id) throws SQLException {
        super(id);
        HashMap<String, String> assay = AssayRequests.requestForInfosOfAssay(id);
        this.setName(assay.get("nameAssay"));
        this.setDescription(assay.get("description"));
        this.setCreationDate(new Date(assay.get("dateAssay")));
        this.setLastModificationDate(new Date(assay.get("lastUpdate")));
        this.idCampaign = Integer.parseInt(assay.get("idCampaign"));
        this.idAlgorithm = Integer.parseInt(assay.get("idAlgorithm"));
        this.algo = new Algorithm(idAlgorithm);
        this.numberOfCells = -1;
        this.numberOfImages = -1;
        this.averageNumberOfCells = -1;
    }


    /**
     * Modifie l'algorithme.
     * @param algo L'objet algorithme.
     */
    public void setAlgo(Algorithm algo){
        this.algo = algo;
        // code pour maj dans la BD
    }

    /**
     * Renvoie l'Algorithm associé à l'Assay
     * @return l'objet Algorithm
     */
    public Algorithm getAlgo(){
        return this.algo;
    }

    /**
     * Renvoie l'id de la campagne auquel l'essai est associé.
     * @return id de la campagne
     */
    public int getIdCampaign() {
        return this.idCampaign;
    }

    /**
     * Renvoie le nombre de cellules visibles sur toutes les images de l'essai.
     * @return nombre de cellules
     */
    public int getNumberOfCells() {
        if (numberOfCells < 0) {
            loadStatistics();
        }
        return this.numberOfCells;
    }

    /**
     * Renvoie le nombre total d'images de l'essai.
     * @return nombre d'images de l'essai
     */
    public int getNumberOfImages() {
        if (numberOfImages < 0) {
            loadStatistics();
        }
        return this.numberOfImages;
    }

    /**
     * Renvoie le nombre moyen de cellules visibles sur l'ensemble des images de l'essai.
     * @return nombre moyen de cellules
     */
    public float getAverageNumberOfCells() {
        if (averageNumberOfCells < 0) {
            loadStatistics();
        }
        return this.averageNumberOfCells;
    }

    /**
     * Modifie l'essai. Met à jour la base de données. La dernière date de modification est actualisée.
     * @param name Nouveau nom.
     * @param description Nouvelle description.
     * @return true si réussite ; false si échec
     */
    public boolean modify(String name, String description) {
        super.setName(name);
        super.setDescription(description);
        super.setLastModificationDate(new Date());
        try {
            AssayRequests.requestForUpdatingNameAssay(this.id, this.getName(), this.getDescription(),
                    this.getLastModificationDate().toString());
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return false;
        }
        return true;

    }

    /**
     * Modifie la date de dernière modification. Met à jour la base de données.
     * @param lastModification La nouvelle date.
     */
    public void modifyLastModification(Date lastModification) {
        super.setLastModificationDate(lastModification);
        loadStatistics();
    }

    /**
     * Donne une chaîne de caractères qui présente l'essai et ses attributs.
     * @return La chaîne de caractères qui présente l'essai.
     */
    @Override
    public String toString() {
        return "[ESSAI] "+super.toString()+" Campagne : "+this.idCampaign;
    }


    /**
     * Méthode qui permet de créer un nouvel essai. L'ajoute également dans la base de données. Utilise le constructeur privé.
     * @param name Le nom de l'essai.
     * @param description La description de l'essai.
     * @param campaign L'objet Campaign associé.
     * @param idAlgorithm L'ID de l'algorithme associé.
     * @param creator L'utilisateur qui crée cet essai.
     * @return L'essai créé. Vaut null si une erreur est survenue lors de la création.
     */
    public static Assay createNewAssay(String name, String description,
                                       Campaign campaign, int idAlgorithm, User creator) {

        Assay a = null;
        try {
            a = new Assay(name, new Date(), description, campaign.getId(), idAlgorithm, creator);

            // modifier également la date de dernière modification de la campagne
            campaign.modify(campaign.getName(), campaign.getDescription());

        } catch (SQLException throwables) {
            throwables.printStackTrace();
            // gérer le cas
        }
        return a;
    }

    /**
     * Initialise les statistiques de l'essai.
     */
    private void loadStatistics() {
        try {
            HashMap<String, Float> stats = Statistics.requestForAssayStatistics(this.id);
            this.averageNumberOfCells = stats.get("Nombre moyen de cellules par image dans l'essai");
            this.numberOfCells = Math.round(stats.get("Nombre total de cellules dans l'essai"));
            this.numberOfImages = Math.round(stats.get("Nombre total d'images dans l'essai"));
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    /**
     * Duplique l'essai et toutes ses images.
     * @param actor utilisateur qui duplique l'essai
     * @param newAlgo nouvel algorithme choisi pour le nouvel essai
     * @return le nouvel essai dupliqué
     */
    public Assay duplicateAssay(User actor, Algorithm newAlgo) {
        Assay newAssay = null;
        String newName = this.getName() + " - dupliqué";
        String newDate = (new Date()).toString();
        try {

            int id = AssayRequests.requestToDuplicateAnAssay(this.id, newAlgo.getId(),
                    newName, newDate, actor.getId());
            ArrayList<Integer> listOfImagesToDuplicate = ImageRequests.requestForImagesInTheAssay(this.id);
            newAssay = new Assay(id);

            for(int imageID : listOfImagesToDuplicate) {

                ImageFromDB imageData = ImageRequests.requestInfosImage(imageID);
                Image img = javafx.embed.swing.SwingFXUtils.fromFXImage(imageData.getImage(),null);

                ImageJ2 ij = new ImageJ2(img, "Image convertie", newAssay.getAlgo(), imageData.isBlackBackground());
                int nbrCells = ij.runAnalyze();

                ImagePlus imageBase = new ImagePlus("Image de base", img);
                imageBase.show();

                ij.showConvertedImage();

                boolean confirm = InformationWindow.create("Confirmer le résultat et l'écriture dans la base de données ?" +
                        "\n "+nbrCells+" cellules ont été trouvées", InformationWindow.CONFIRMATION);

                ij.closeImage();
                imageBase.close();

                if(confirm) {
                    int newId = ImageRequests.requestToDuplicateAnImage(this.id, imageID, newAssay.id, newDate, actor.getId());
                    ij.writePiles(newId);
                }

            }//for
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return newAssay;
    }

    /**
     * Supprime l'essai.
     * @return true si la suppression est une réussite ; false sinon
     */
    public boolean deleteThisAssay() {
        try {
            ArrayList<Integer> idList = ImageRequests.requestForImagesInTheAssay(this.id);
            for (int id : idList) {
                ImageRequests.requestForDeletingImage(id);
            }
            AssayRequests.requestForDeletingAssay(this.id);
            return true;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return false;
        }
    }

    /**
     * Donne la liste des essais visibles pour un utilisateur donné.
     * @param user utilisateur concerné
     * @return liste des essais visibles par l'utilisateur
     */
    public static ArrayList<Assay> getAssaysSeenByUser(User user) {
        ArrayList<Assay> result = new ArrayList<>();

        try {
            ArrayList<HashMap<String,String>> listOfAssaysInfo = AssayRequests.requestForAssaysSeenByUser(user.getId());
            for(HashMap<String, String> line : listOfAssaysInfo) {
                Assay assay = new Assay(Integer.parseInt(line.get("idEssai")), line.get("nom"),
                                new Date(line.get("dateCreation")),
                                new Date(line.get("derniereModif")), line.get("description"),
                                Integer.parseInt(line.get("idCampagne")),
                                Integer.parseInt(line.get("idAlgorithme")),
                                new User(Integer.parseInt(line.get("idUtilisateur"))));

                result.add(assay);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return result;
    }




}
