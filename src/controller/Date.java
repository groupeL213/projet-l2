package controller;

import java.text.DateFormatSymbols;
import java.time.Duration;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Locale;

/**
 * Modélise une date et une heure. Utilise la classe ZonedDateTime. Les nanosecondes sont ignorées.
 */
public class Date {
    private final ZonedDateTime date ;

    /* CONSTANTS */
    private static final String ZONE = "Europe/Paris";
    private static final DateFormatSymbols french_dfs = new DateFormatSymbols(Locale.FRENCH);

    private static final int DURATION_ONE_MINUTE = 60;
    private static final int DURATION_TEN_MINUTES = 60*10;
    private static final int DURATION_ONE_HOUR = 60*60;
    private static final int DURATION_ONE_DAY = 60*60*24;
    private static final int DURATION_ONE_MONTH = 60*60*24*31;
    private static final int DURATION_ONE_YEAR = 60*60*24*31*12;


    /**
     * Créer un objet date correspondant à la date et l'heure de sa création.
     */
    public Date() {
        date = ZonedDateTime.now(ZoneId.of(ZONE));
    }

    /**
     * Créer un objet Date à partir de l'année, mois, jour, heure, minute, seconde.
     * @param year L'année.
     * @param month Le mois (entre 1 et 12).
     * @param dayOfMonth Jour du mois (entre 1 et 31).
     * @param hourOfDay L'heure dans la journée (entre 0 et 23).
     * @param minute Les minutes dans l'heure (entre 0 et 59).
     * @param seconds Les secondes dans la minutes (entre 0 et 59).
     * @throws IllegalArgumentException Au moins un des paramètres n'est pas valide.
     */
    public Date(int year, int month, int dayOfMonth, int hourOfDay, int minute, int seconds) {
        if (verifyValues(year, month, dayOfMonth, hourOfDay, minute, seconds)) {
            this.date = ZonedDateTime.of(year, month, dayOfMonth, hourOfDay, minute, seconds,
                    0, ZoneId.of(ZONE));
        } else {
            throw new IllegalArgumentException("Les paramètres ne sont pas valides : l'année doit être" +
                    "positive, le mois compris entre 1 et 12, le jour compris entre 1 et 31, " +
                    "les heures comprises entre 0 et 24, et les minutes/secondes entre 0 et 59");
        }
    }

    /**
     * Créer une date à partir d'une chaîne de caractères ayant ce format : aaaa-mm-jj hh:mm:ss (format BD).
     * @param dateDB Chaine de caractères représentant la date et l'heure.
     */
    public Date(String dateDB) {
        String[] dateFromDB = dateDB.split(" ")[0].split("-");
        String[] timeFromDB = dateDB.split(" ")[1].split(":");
        this.date = ZonedDateTime.of(Integer.parseInt(dateFromDB[0]), Integer.parseInt(dateFromDB[1]),
                Integer.parseInt(dateFromDB[2]), Integer.parseInt(timeFromDB[0]),
                Integer.parseInt(timeFromDB[1]), Integer.parseInt(timeFromDB[2]),
                0, ZoneId.of(ZONE));

    }


    /**
     * Donne une chaîne de caractères donnant la valeur de l'objet avec ce format : aaaa-mm-jj hh:mm:ss.
     * Par exemple, 2020-12-19 12:25:53. Ce format permet d'insérer des datetime dans la base de données.
     * @return La chaîne de caractère donnant la date et l'heure.
     */
    public String toString() {
        return date.getYear()+"-"+date.getMonthValue()+"-"+date.getDayOfMonth()
                +" "+date.getHour()+":"+date.getMinute()+":"+date.getSecond();
    }

    /**
     * Donne une chaîne de caractères donnant la valeur de l'objet avec ce format : jj mois aaaa à hh:mm.
     * Par exemple, le 19 décembre 2020 à 12:25. Ce format sera utilisé pour afficher les dates à l'utilisateur.
     * Si l'heure ou les minutes sont inférieures à 10, elles sont complétées d'un zéro à gauche.
     * @return La chaîne de caractère donnant la date et l'heure au format français.
     */
    public String toFrenchForm() {
        return date.getDayOfMonth()+" "+ french_dfs.getMonths()[date.getMonthValue()-1] + " " + date.getYear()
                +" à " + formatWithZeros(date.getHour())+":"+formatWithZeros(date.getMinute());
    }


    /**
     * Vérifie que les valeurs données en paramètres sont toutes correctes.
     * @param year L'année : doit être positive.
     * @param month Le mois : doit être compris entre 1 et 12.
     * @param day Le jour : doit être compris entre 1 et 31.
     * @param hour L'heure : doit être comprise entre 0 et 23.
     * @param minute Les minutes : doivent être comprises entre 0 et 59.
     * @param seconds Les secondes : doivent être comprises entre 0 et 59.
     * @return True si toutes les valeurs sont correctes ; False sinon.
     */
    private boolean verifyValues(int year, int month, int day, int hour, int minute, int seconds) {
        return (year > 0
                && month > 0 && month < 13
                && day > 0 && day < 32
                && hour >= 0 && hour <= 23
                && minute >= 0 && minute <= 59
                && seconds >= 0 && seconds <= 59);
    }

    /**
     * Renvoie la date sous ce format : jj/mm/aaaa. Si le jour ou le mois est inférieur à 10, il est complété
     * d'un zéro à gauche. Ex : 1 devient 01.
     * @return la chaine de caractères qui décrit la date au format jj/mm/aaaa
     */
    public String toSimpleForm() {
        return formatWithZeros(date.getDayOfMonth()) + "/"
                + formatWithZeros(date.getMonthValue()) + "/"
                + date.getYear();
    }

    /**
     * Donne la différence entre deux Date.
     * @param date La date à comparer.
     * @return La Duration représentant la différence entre les deux dates.
     */
    private Duration difference(Date date) {
        return Duration.between(this.date, date.getDate());
    }

    /**
     * Donne l'objet ZonedDateTime de l'instance. Usage réservé à la classe.
     * @return La date au format ZonedDateTime.
     */
    private ZonedDateTime getDate() {
        return date;
    }

    /**
     * Renvoie une chaîne de caractères qui représente la durée qui s'est écoulée entre deux dates.
     * Peu importe l'ordre des Date, le résultat sera toujours positif.
     * Le format dépend de cette durée :
     * Si la durée est inférieure à une minute, alors le format est du type "35 secondes".
     * Si elle est inférieure à dix minutes, alors le format est du type "5 min 10 sec".
     * Si elle est inférieure à une heure, le format est du type "45 minutes".
     * Si elle est inférieure à un jour, le format est donné en heure, du type "6 heures".
     * Au-delà d'un jour, le format est donné en jours, par exemple "15 jours".
     * Au-delà d'un mois, le format est donné en mois, par exemple "2 mois".
     * Au-delà d'un an, le format est donné en année, par exemple "2 ans".
     * @param date La date à comparer.
     * @return La chaîne de caractères indiquant la durée qui s'est écoulée entre les deux dates.
     */
    public String howManyTimeAgo(Date date) {
        Duration d = this.difference(date);
        long seconds = d.getSeconds();
        seconds = (seconds < 0) ? seconds*(-1) : seconds;
        String result = "";

        int typeFormat;

        if (seconds < DURATION_ONE_MINUTE) typeFormat = 1;
        else
            if (seconds < DURATION_ONE_HOUR)
                if (seconds < DURATION_TEN_MINUTES) typeFormat = 2;
                else typeFormat = 3;
            else
                if (seconds < DURATION_ONE_DAY)  typeFormat = 4;
                else
                    if (seconds < DURATION_ONE_MONTH) typeFormat = 5;
                    else
                        if (seconds < DURATION_ONE_YEAR) typeFormat = 6;
                        else typeFormat = 7;


        switch(typeFormat) {
            case 1: result = seconds+" secondes"; break;
            case 2: result = (int)(seconds / DURATION_ONE_MINUTE) + " min"
                            + (int)(seconds % DURATION_ONE_MINUTE) + " sec"; break;
            case 3: result = (int)(seconds / DURATION_ONE_MINUTE) + " minutes"; break;
            case 4: result = (int)(seconds / DURATION_ONE_HOUR) > 1 ?
                            (int)(seconds/DURATION_ONE_HOUR)+" heures"
                            : (int)(seconds/DURATION_ONE_HOUR)+" heure"; break;
            case 5: result = (int)(seconds / DURATION_ONE_DAY) > 1 ?
                            (int)(seconds/DURATION_ONE_DAY)+" jours"
                            : (int)(seconds/DURATION_ONE_DAY)+" jour"; break;
            case 6: result = (int) (seconds / DURATION_ONE_MONTH) + " mois"; break;
            case 7: result = (int) (seconds / DURATION_ONE_YEAR) > 1 ?
                            (int) (seconds / DURATION_ONE_YEAR) + " ans"
                            : (int) (seconds / DURATION_ONE_YEAR) + " an"; break;
        }

        return result;
    }

    /**
     * Transforme un nombre en un String formatté comme suit : 1 devient "01".
     * Les zéros sont ajoutés à gauche pour remplir 2 caractères.
     * @param number Le nombre à formatter.
     * @return La chaîne de caractères formatée.
     */
    public String formatWithZeros(int number) {
        return String.format("%02d",number);
    }

    /**
     * Compare deux dates entre elles.
     * @param otherDate la date à comparer
     * @return nombre positif si date supérieure à otherDate ; négatif si date inférieure à otherDate ; nul sinon.
     */
    public int compareTo(Date otherDate) {
        if (this.date.isBefore(otherDate.getZonedDateTime())) {
            return -1;
        }
        if (this.date.isAfter(otherDate.getZonedDateTime())) {
            return 1;
        }
        return 0;
    }

    /**
     * Donne l'objet ZonedDateTime de l'instance.
     * @return objet ZonedDateTime
     */
    public ZonedDateTime getZonedDateTime() {
        return date;
    }

    /**
     * Donne une date à partir d'une chaine de caractères représentant une date au format jj/mm/aaaa.
     * Attention, ce format ne permet pas de donner l'heure exacte, les heures, minutes et secondes seront
     * initialisées à zéro.
     * @param dateWithSlashs la date avec des slashs
     * @return l'objet Date issu de la chaine de caractères
     */
    public static Date convertStringSlashsToDate(String dateWithSlashs) {
        String[] tabSplit = dateWithSlashs.split("/");
        return new Date(Integer.parseInt(tabSplit[2]), Integer.parseInt(tabSplit[1]),
                Integer.parseInt(tabSplit[0]), 0, 0, 0);
    }

}
