package controller;

/**
 * Classe abstraite qui représente les informations basiques des campagnes et des essais.
 */
public abstract class  CommonData {

    protected int id;
    private String name, description;
    private Date creationDate, lastModificationDate;
    private User creator;


    /**
     * Construit un nouvel objet qui n'initialise que l'ID. Le reste devra être initialisé par la classe fille.
     * @param id L'ID de l'objet.
     */
    public CommonData(int id) {
        this.id = id;
    }

    /**
     * Construit un nouvel objet à partir des paramètres.
     * @param name Le nom de l'objet.
     * @param description La description de l'objet.
     * @param creationDate La date de création de l'objet.
     * @param lastModificationDate La date de dernière modification de l'objet.
     */
    public CommonData(String name, String description, Date creationDate, Date lastModificationDate) {
        this.name = name;
        this.description = description;
        this.creationDate = creationDate;
        this.lastModificationDate = lastModificationDate;
    }


    /**
     * Donne l'ID de l'objet.
     * @return L'ID de l'objet.
     */
    public int getId() {
        return id;
    }

    /**
     * Donne le nom de l'objet.
     * @return Nom de l'objet.
     */
    public String getName() {
        return name;
    }

    /**
     * Donne la description de l'objet.
     * @return La description de l'objet.
     */
    public String getDescription() {
        return description;
    }

    /**
     * Donne la valeur de la date de création.
     * @return Date de création.
     */
    public Date getCreationDate() {
        return creationDate;
    }

    /**
     * Donne la valeur de la date de dernière modification.
     * @return Date de dernière modification.
     */
    public Date getLastModificationDate() {
        return lastModificationDate;
    }

    /**
     * Donne l'utilisateur qui a créé l'objet.
     * @return créateur de l'objet
     */
    public User getCreator() {
        return creator;
    }

    /**
     * Modifie le nom de l'objet à partir de celui donné en paramètre.
     * @param name Le nouveau nom.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Modifie la description de l'objet à partir de celui donné en paramètre.
     * @param description La nouvelle description.
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Modifie la date de création.
     * @param creationDate Nouvelle date de création.
     */
    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }


    /**
     * Modifie la date de dernière modification.
     * @param lastModificationDate Nouvelle date de dernière modification.
     */
    public void setLastModificationDate(Date lastModificationDate) {
        this.lastModificationDate = lastModificationDate;
    }

    public void setCreator(User user) {
        this.creator = user;
    }

    /**
     * Donne une chaîne de caractères qui présente l'objet et ses attributs.
     * @return La chaîne de caractères qui présente l'objet.
     */
    public String toString() {
        return this.getName() + " Description : "+this.getDescription()+ " Date Création : "+this.getCreationDate().toString()
                + " Date Dernière modification : "+this.getLastModificationDate().toString();
    }


}
