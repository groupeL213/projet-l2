package controller;

import javafx.collections.ObservableList;
import model.*;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * L'objet Campaign modélise une campagne. Une campagne est caractérisée par son unique ID, son nom, sa description,
 * sa date de création, sa date de dernière modification.
 * Cette classe permet aussi l'accès à quelques statistiques, telles que le nombre d'essais contenus dans cette
 * campagne, le nombre d'images, le nombre total de cellules, le nombre moyen de cellules.
 */
public class Campaign  extends CommonData {

    private final Team team;

    private int numberOfAssays, numberOfImages, numberOfCells;
    private float averageNumberOfCellsPerImage;


    /**
     * Constructeur utilisé pour créer un objet Campaign pour une campagne déjà existante à partir de son ID à l'aide
     * de la base de données.
     * @param id L'ID de la campagne à retrouver.
     * @throws SQLException Si une erreur survient lors de l'accès aux données de la base de données.
     */
    public Campaign(int id) throws SQLException {
        super(id);
        HashMap<String, String> infos = CampaignRequests.requestForInfosOfCampaign(id);

        super.setName(infos.get("Name"));
        super.setDescription(infos.get("Description"));
        super.setCreationDate(new Date(infos.get("Date of creation")));
        super.setLastModificationDate(new Date(infos.get("Date of the last update")));
        this.team = new Team(Integer.parseInt(infos.get("ID of the team")));

        this.numberOfAssays = -1;
        this.numberOfImages = -1;
        this.averageNumberOfCellsPerImage = -1;
        this.numberOfCells = -1;

    }

    /**
     * Constructeur utilisé pour créer une nouvelle campagne. Initialise les attributs à partir des valeurs données
     * en paramètres. Ajoute la campagne dans la base de données. Usage exclusivement réservé à la classe uniquement.
     * Utilisé notamment par la méthode createNewCampaign().
     * @param name Le nom de la campagne.
     * @param description La description de la campagne.
     * @param creationDate La date de création de la campagne. La date de dernière modification sera égale à cette valeur.
     * @param team L'équipe qui gère cette campagne.
     * @param creator Utilisateur qui a créé la campagne.
     * @throws SQLException Si une erreur survient lors de la modification de la base de données.
     */
    private Campaign(String name, String description, Date creationDate, Team team, User creator)
            throws SQLException {
        super(name, description, creationDate, creationDate);
        this.team = team;
        super.id = CampaignRequests.requestForNewCampaign(name, description, creationDate.toString(),
                creationDate.toString(), team.getId(), creator.getId());
        this.numberOfAssays = -1;
        this.numberOfImages = -1;
        this.averageNumberOfCellsPerImage = -1;
        this.numberOfCells = -1;
        setCreator(creator);
    }

    /**
     * Constructeur utilisé pour créer un objet Campaign à partir de tous ses attributs.
     * Cette campagne n'est pas ajoutée dans la base de données.
     * @param id id de la campagne
     * @param name nom
     * @param description description
     * @param creationDate date de création
     * @param modificationDate date de dernière modification
     * @param team équipe associée
     * @param creator utilisateur qui a créé la campagne
     */
    private Campaign(int id, String name, String description, Date creationDate, Date modificationDate,
                     Team team, User creator) {
        super(name, description, creationDate, modificationDate);
        this.team = team;
        this.id = id;
        setCreator(creator);
        this.numberOfAssays = -1;
        this.numberOfImages = -1;
        this.averageNumberOfCellsPerImage = -1;
        this.numberOfCells = -1;
    }

    /**
     * Supprime la campagne.
     * @return true si la suppression est une réussite ; false sinon
     */
    public boolean deleteThisCampaign() {
        try{
            CampaignRequests.requestForDeletingCampaign(this.id);
            return true;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return false;
        }
    }

    /**
     * Exporte la campagne et ses données associées dans un fichier CSV dans le dossier "export"
     * @param items la liste des essais (pour éviter de refaire une requete)
     */
    public void export(ObservableList<Assay> items) throws IOException, SQLException {

        String[] assayHeader = {"Nom de l'essai","Date de création","Nombre d'image",
        "Nombre de cellule", "Moyenne cellule/image", "Algorithme"};

        String[] imageHeader = {"Nom de l'image","Nombre de cellule"};

        String[] total = {"Nombre d'essai","Nombre d'image","Nombre de cellule","Moyenne"};

        String fileName = this.getName().replaceAll(" ","-");

        CSVFileExport file = new CSVFileExport("Export-Campagne-"+fileName);

        file.write("Nom de la campagne");
        file.write(this.getName(),true);

        file.nextLine();

        for(Assay assay : items){

            file.write(assayHeader);

            file.write(assay.getName());
            file.write(assay.getCreationDate().toFrenchForm());
            HashMap<String,Float> stats = Statistics.requestForAssayStatistics(assay.getId());
            file.write(stats.get("Nombre total d'images dans l'essai").toString());
            file.write(stats.get("Nombre total de cellules dans l'essai").toString());
            file.write(stats.get("Nombre moyen de cellules par image dans l'essai").toString());
            file.write(assay.getAlgo().getName(),true);

            file.nextLine();

            file.write(imageHeader);

            ArrayList<HashMap<String,String>> info = ImageRequests.requestForExportInfos(assay.getId());
            for (HashMap<String, String> img : info) {
                file.write(img.get("name"));
                file.write(img.get("number"), true);
            }
            if(info.size() == 0){
                file.write("Aucune image dans cet essai",true);
            }
            file.nextLine();

        }
        file.nextLine();
        file.write("TOTAL",true);
        file.write(total);
        file.write(""+items.size());

        HashMap<String,Float> statsCampaign = Statistics.requestForCampaignStatistics(this.getId());
        file.write(statsCampaign.get("Nombre total d'images dans la campagne").toString());
        file.write(statsCampaign.get("Nombre total de cellules dans la campagne").toString());
        file.write(statsCampaign.get("Nombre moyen de cellules par image dans la campagne").toString(),true);

        file.close();

    }


    /* ----------------------- GETTERS ----------------------- */


    /**
     * Donne l'équipe qui gère la campagne.
     * @return L'équipe de la campagne.
     */
    public Team getTeam() {
        return team;
    }



    /* ----------------------- SETTERS ----------------------- */


    /**
     * Modifie la campagne. Met à jour la base de données. La dernière date de modification est actualisée.
     * @param name Nouveau nom.
     * @param description Nouvelle description.
     * @return true si réussite ; false si échec
     */
    public boolean modify(String name, String description) {
        super.setName(name);
        super.setDescription(description);
        super.setLastModificationDate(new Date());
        try {
            CampaignRequests.requestForUpdatingCampaign(this.id, this.getName(), this.getDescription(),
                    this.getLastModificationDate().toString());
            loadStatistics();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return false;
        }
        return true;

    }


    /**
     * Donne une chaîne de caractères qui présente la campagne et ses attributs.
     * @return La chaîne de caractères qui présente la campagne.
     */
    @Override
    public String toString() {
        return this.getName();
    }



    /**
     * Renvoie le nombre d'essais de la campagne.
     * Lors du premier appel, cette méthode appelle la BD, puis la valeur est enregistrée.
     * Lors des appels suivants, la valeur est directement renvoyée sans appeler la BD.
     * @return Le nombre d'essais.
     */
    public int getNumberOfAssays() {
        if (numberOfAssays < 0) {
            try {
                numberOfAssays = CampaignRequests.requestNbAssayInCampaign(this.id);
            } catch (SQLException throwables) {
                throwables.printStackTrace();
                return 0;
            }
        }
        return numberOfAssays;

    }
    /**
     * Renvoie le nombre d'images de la campagne.
     * Lors du premier appel, cette méthode appelle la BD, puis la valeur est enregistrée.
     * Lors des appels suivants, la valeur est directement renvoyée sans appeler la BD.
     * @return Le nombre d'images.
     */
    public int getNumberOfImages() {
        if(numberOfImages < 0) {
            loadStatistics();
        }
        return this.numberOfImages;
    }

    /**
     * Renvoie le nombre total de cellules comptées sur les images de cette campagne.
     * Lors du premier appel, cette méthode appelle la BD, puis la valeur est enregistrée.
     * Lors des appels suivants, la valeur est directement renvoyée sans appeler la BD.
     * @return Le nombre total de cellules.
     */
    public int getNumberOfCells() {
        if(numberOfCells < 0) {
            loadStatistics();
        }
        return this.numberOfCells;
    }

    /**
     * Renvoie le nombre moyen de cellules par image.
     * Lors du premier appel, cette méthode appelle la BD, puis la valeur est enregistrée.
     * Lors des appels suivants, la valeur est directement renvoyée sans appeler la BD.
     * @return Le nombre moyen de cellules par image.
     */
    public float getAverageNumberOfCellsPerImage() {
        if(averageNumberOfCellsPerImage < 0) {
            loadStatistics();
        }
        return this.averageNumberOfCellsPerImage;
    }

    /**
     * Charge les statistiques dans l'objet User. Usage réservé à cette classe.
     * Statistiques concernées : nombre d'images, nombre de cellules, nombre moyen de cellules par image.
     */
    private void loadStatistics() {
        try {
            HashMap<String, Float> stats = Statistics.requestForCampaignStatistics(this.id);
            this.averageNumberOfCellsPerImage = stats.get("Nombre moyen de cellules par image dans la campagne");
            this.numberOfImages = Math.round(stats.get("Nombre total d'images dans la campagne"));
            this.numberOfCells = Math.round(stats.get("Nombre total de cellules dans la campagne"));
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }




    /* ----------------------- STATIC METHODS ----------------------- */

    /**
     * Crée une nouvelle campagne à partir des paramètres. L'ajoute dans la base de données.
     * @param name Nom de la nouvelle campagne.
     * @param description Description de la nouvelle campagne.
     * @param team Equipe qui gère la campagne.
     * @param creator Utilisateur qui a créé la campagne.
     * @return La campagne créée. Vaut null si une erreur est survenue lors de la création de la campagne.
     */
    public static Campaign createNewCampaign(String name, String description, Team team, User creator) {
        Campaign c = null;
        try {
            c = new Campaign(name, description, new Date(), team, creator);

        } catch (SQLException throwables) {
            throwables.printStackTrace();
            // Gérer le cas où une erreur est survenue avec la BD
        } // Gérer le cas où une erreur est survenue

        return c;
    }

    /**
     * Donne la liste des campagnes visibles pour un utilisateur donné.
     * @param user utilisateur concerné
     * @return liste des campagnes visibles par l'utilisateur
     */
    public static ArrayList<Campaign> getCampaignsSeenByUser(User user) {
        ArrayList<Campaign> result = new ArrayList<>();

        try {
            ArrayList<HashMap<String, String>> listOfCampagnsInfo =
                    CampaignRequests.requestForCampaignsSeenByUser(user.getId());
            for (HashMap<String, String> line : listOfCampagnsInfo) {
                Campaign c = new Campaign(Integer.parseInt(line.get("idCampagne")), line.get("nom"),
                        line.get("description"),
                        new Date(line.get("dateCreation")), new Date(line.get("derniereModif")),
                        new Team(Integer.parseInt(line.get("idEquipe"))),
                        new User(Integer.parseInt(line.get("idUtilisateur"))));
                result.add(c);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return result;
    }

    /**
     * Donne la liste des campagnes qui sont gérées par une équipe précise.
     * @param team équipe en question
     * @return la liste des campagnes gérées par l'équipe
     */
    public static ArrayList<Campaign> getCampaignsForATeam(Team team) {
        ArrayList<Campaign> result = new ArrayList<>();

        try {
            ArrayList<HashMap<String, String>> listOfCampagnsInfo =
                    TeamRequests.requestForCampaignsListForATeam(team.getId());
            for (HashMap<String, String> line : listOfCampagnsInfo) {
                Campaign c = new Campaign(Integer.parseInt(line.get("idCampagne")), line.get("nom"),
                        line.get("description"),
                        new Date(line.get("dateCreation")), new Date(line.get("derniereModif")),
                        team,
                        new User(Integer.parseInt(line.get("idUtilisateur"))));
                result.add(c);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return result;
    }
}
